<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {

	return \Redirect::to('/login');
});

Route::get('login', 'UserController@login');
Route::get('register', 'UserController@register');


Route::group(['middleware' => 'guest'], function () {
	Route::get('/login', 'UserController@login');
	Route::get('/create-account', 'UserController@create');
	Route::post('/auth', 'UserController@auth');
	Route::post('store-user', 'UserController@store');

});


/*
|FMB Admin pages
|
|
|
*/
Route::group(['middleware' => 'fmb-admin-access'], function () {
	
	Route::get('/admin/dashboard', 'FmbAdminController@dashboard');
	Route::get('/admin/accounts', 'FmbAdminController@accounts');
	Route::get('/admin/accounts/{id}', 'FmbAdminController@viewAccount');
	Route::get('/admin/users', 'FmbAdminController@users');
	Route::get('/admin/users/create', 'FmbAdminController@createUser');

	Route::get('/admin/users/pending', 'FmbAdminController@userModifications');
	Route::get('/admin/users/pending/{id}/authorize', 'FmbAdminController@authorizeUserModification');
	Route::get('/admin/users/pending/{id}', 'FmbAdminController@viewPendingUserModification');


	Route::get('/admin/users/modifications/{id}', 'FmbAdminController@viewUserModification');

	Route::get('/admin/users/{id}/edit', 'FmbAdminController@editUser');
	Route::get('/admin/users/{id}', 'FmbAdminController@viewUser');


	Route::get('/admin/activities', 'FmbAdminController@userActivities');

	Route::get("/admin/accounts-report", 'FmbAdminController@accountsReport');
	Route::get("/admin/accounts-report/download", 'FmbAdminController@downloadAccountsReport');
	
	Route::get("/admin/ajax-generate-files-report", 'FmbAdminController@exportDocumentFiles');

	Route::post('/admin/users/store', 'FmbAdminController@storeUser');
	Route::post('/admin/users/{id}/update', 'FmbAdminController@updateUser');

});



Route::group(['middleware' => 'fmb-staff-access'], function () {
	
	Route::get('/user/dashboard', 'FmbStaffController@dashboard');
	Route::get('/user/accounts', 'FmbStaffController@accounts');
	Route::get('/user/accounts/{id}', 'FmbStaffController@viewAccount');

	Route::get("/user/accounts-report", 'FmbStaffController@accountsReport');
	Route::get("/user/accounts-report/download", 'FmbStaffController@downloadAccountsReport');



});





Route::get("/my-account", 'FmbAdminController@myAccount');
Route::post('/update-my-account', 'FmbAdminController@updateOwnAccount');


/*
|FMB Admin pages
|
|
|
*/
Route::group(['middleware' => 'beehive-admin-access'], function () {
	
	Route::get('/beehive-admin/dashboard', 'BeehiveAdminController@dashboard');
	Route::get('/beehive-admin/accounts', 'BeehiveAdminController@accounts');

	Route::get('/beehive-admin/accounts/{id}', 'BeehiveAdminController@viewAccount');
	Route::get('/beehive-admin/pending-accounts', 'BeehiveAdminController@pendingAccounts');
	Route::get('/beehive-admin/users', 'BeehiveAdminController@users');

	Route::get('/beehive-admin/activities', 'BeehiveAdminController@userActivities');

	Route::get('/beehive-admin/users/create', 'BeehiveAdminController@createUser');
	Route::get('/beehive-admin/users/{id}/edit', 'BeehiveAdminController@editUser');

	Route::post('/beehive-admin/users/store', 'BeehiveAdminController@storeUser');
	Route::post('/beehive-admin/users/{id}/update', 'BeehiveAdminController@updateUser');

	Route::get('/beehive-admin/users/{id}', 'BeehiveAdminController@viewUser');



	Route::get("/beehive-admin/accounts-report", 'BeehiveAdminController@accountsReport');
	Route::get("/beehive-admin/accounts-report/download", 'BeehiveAdminController@downloadAccountsReport');




	Route::get("/beehive-admin/ajax-pending-accounts", 'BeehiveAdminController@ajaxPendingAccounts');

	
	Route::get("/beehive-admin/ajax-pending-accounts/{id}/authorize", 'BeehiveAdminController@ajaxAuthorizePendingAccount');

	Route::get("/beehive-admin/ajax-pending-accounts/{id}/unauthorize", 'BeehiveAdminController@ajaxUnauthorizePendingAccount');

	Route::get("/beehive-admin/ajax-pending-accounts/{id}", 'BeehiveAdminController@ajaxPendingAccount');



	

});







/*
|Clerk pages
|
|
|
*/
Route::group(['middleware' => 'clerk-access'], function () {
	Route::get('/clerks/dashboard', 'ClerkController@dashboard');
	Route::get('/clerks/entries', 'ClerkController@entries');
	Route::get('/clerks/customers/{customerTypeId}/accounts/create', 'ClerkController@newEntry');
});





Route::get('/logout', 'UserController@logout');



Route::group(['middleware' => 'auth'], function () {

	


	Route::get('/ajax-access-permissions', 'UserController@ajaxAccessPermissions');
	


	Route::post('/archiving-boxes/store', 'ArchivingBoxController@store');
	Route::post('/accounts/store', 'AccountController@store');
	Route::post("/accounts/files/store-tmp", 'FileController@storeTmpAccountFile');


	Route::get("/accounts/files/delete-tmp", 'FileController@deleteTmpAccountFile');
	Route::get('/account-files/{filename}/view', "FileController@view");
	Route::get('/account-files/{filename}/download', "FileController@download");

	Route::get('/pending-account-files/{filename}/view', "FileController@pendingAccountFileView");
	Route::get('/pending-account-files/{filename}/download', "FileController@pendingAccountFileDownload");
	
});
