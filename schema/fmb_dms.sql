-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 21, 2018 at 10:54 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.2.3-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fmb_dms`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(2) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `name`, `updated_at`, `active`, `created_at`) VALUES
(1, 'Superuser', '2018-01-24 09:10:56', 1, '2018-01-24 19:10:56'),
(2, 'FMB Admin', '2018-01-24 09:10:56', NULL, '2018-01-24 19:10:56'),
(3, 'FMB Staff', '2018-01-24 09:11:19', 1, '2018-01-24 19:11:19'),
(4, 'FMB Tech', '2018-01-24 09:11:19', NULL, '2018-01-24 19:11:19'),
(5, 'Beehive Admin', '2018-01-24 09:11:49', 1, '2018-01-24 19:11:49'),
(6, 'Beehive Team Leader', '2018-01-24 09:11:49', 1, '2018-01-24 19:11:49'),
(7, 'Beehive Clerk', '2018-01-24 09:12:13', 1, '2018-01-24 19:12:13');

-- --------------------------------------------------------

--
-- Table structure for table `access_level_access_permission`
--

CREATE TABLE `access_level_access_permission` (
  `id` int(11) NOT NULL,
  `access_level_id` int(11) NOT NULL,
  `access_permission_id` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_level_access_permission`
--

INSERT INTO `access_level_access_permission` (`id`, `access_level_id`, `access_permission_id`, `updated_at`, `created_at`) VALUES
(1, 2, 1, '2018-02-13 14:58:36', '2018-02-13 20:58:36'),
(2, 2, 2, '2018-02-13 15:13:35', '2018-02-13 20:58:36'),
(3, 2, 3, '2018-02-13 15:13:57', '2018-02-13 21:13:57'),
(4, 2, 4, '2018-02-13 15:14:07', '2018-02-13 21:13:57'),
(5, 2, 5, '2018-02-13 15:14:33', '2018-02-13 21:14:33'),
(6, 2, 6, '2018-02-13 15:14:33', '2018-02-13 21:14:33'),
(7, 2, 7, '2018-02-13 15:14:51', '2018-02-13 21:14:51'),
(8, 2, 8, '2018-02-13 15:14:51', '2018-02-13 21:14:51'),
(9, 2, 9, '2018-02-14 08:00:21', '2018-02-14 12:00:21'),
(10, 3, 6, '2018-02-14 09:32:24', '2018-02-14 13:32:24'),
(11, 3, 9, '2018-02-14 09:32:24', '2018-02-14 13:32:24'),
(12, 5, 3, '2018-03-09 20:46:31', '2018-03-09 22:46:31'),
(13, 5, 11, '2018-03-09 20:46:31', '2018-03-09 22:46:31'),
(14, 5, 12, '2018-03-09 20:46:31', '2018-03-09 22:46:31'),
(15, 5, 8, '2018-03-09 20:46:31', '2018-03-09 22:46:31'),
(16, 5, 9, '2018-03-09 20:46:31', '2018-03-09 22:46:31');

-- --------------------------------------------------------

--
-- Table structure for table `access_permissions`
--

CREATE TABLE `access_permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_permissions`
--

INSERT INTO `access_permissions` (`id`, `name`, `updated_at`, `created_at`) VALUES
(1, 'Create FMB Admin Users', '2018-02-13 12:07:04', '2018-02-06 14:05:15'),
(2, 'Update FMB Admin Users', '2018-02-13 12:09:52', '2018-02-06 14:05:15'),
(3, 'View users', '2018-02-13 12:10:39', '2018-02-06 14:06:32'),
(4, 'Create FMB staff', '2018-02-13 12:10:51', '2018-02-06 14:06:32'),
(5, 'Update FMB staff', '2018-02-13 12:11:10', '2018-02-13 12:53:22'),
(6, 'View Accounts', '2018-02-13 12:11:45', '2018-02-13 18:11:45'),
(7, 'Update Accounts', '2018-02-13 12:11:45', '2018-02-13 18:11:45'),
(8, 'Audit trail', '2018-02-13 12:12:18', '2018-02-13 18:12:18'),
(9, 'Pull Reports', '2018-02-13 12:13:15', '2018-02-13 18:13:15'),
(10, 'authorize user modification', '2018-02-27 12:26:25', '2018-02-27 14:26:25'),
(11, 'Create Beehive Admin Users', '2018-03-09 20:28:40', '2018-03-09 22:28:40'),
(12, 'Update Beehive Admin Users', '2018-03-09 20:28:40', '2018-03-09 22:28:40');

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `account_number` varchar(20) NOT NULL,
  `account_type_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `account_status` tinyint(4) DEFAULT NULL,
  `account_modification_id` int(11) DEFAULT NULL,
  `archiving_box_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `account_modifications`
--

CREATE TABLE `account_modifications` (
  `id` int(11) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `customer_modification_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `account_type_id` int(11) DEFAULT NULL,
  `account_number` varchar(20) NOT NULL,
  `account_status` tinyint(4) DEFAULT NULL,
  `initiator_id` int(11) DEFAULT NULL,
  `verifier_id` int(11) DEFAULT NULL,
  `verifier_comment` varchar(200) DEFAULT NULL,
  `archiving_box_id` int(11) DEFAULT NULL,
  `authorization_status` tinyint(1) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `account_types`
--

CREATE TABLE `account_types` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account_types`
--

INSERT INTO `account_types` (`id`, `name`, `updated_at`, `created_at`) VALUES
(1, 'Savings', '2018-01-24 04:43:25', '2018-01-24 14:43:25'),
(2, 'Current', '2018-01-24 04:43:25', '2018-01-24 14:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `activity_types`
--

CREATE TABLE `activity_types` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity_types`
--

INSERT INTO `activity_types` (`id`, `name`, `updated_at`, `created_at`) VALUES
(1, 'create', '2017-10-08 11:23:08', '2017-10-08 23:23:08'),
(2, 'view', '2017-10-08 11:23:08', '2017-10-08 23:23:08'),
(3, 'download', '2017-10-08 11:23:32', '2017-10-08 23:23:32'),
(4, 'update', '2017-10-08 11:23:32', '2017-10-08 23:23:32'),
(5, 'delete', '2017-10-08 11:23:56', '2017-10-08 23:23:56'),
(6, 'unauthorize', '2018-02-03 13:18:00', '2018-02-03 19:17:22'),
(7, 'authorize', '2018-02-03 13:18:11', '2018-02-03 19:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `archiving_boxes`
--

CREATE TABLE `archiving_boxes` (
  `id` int(11) NOT NULL,
  `box_number` varchar(20) DEFAULT NULL,
  `serial_number` varchar(45) DEFAULT NULL,
  `location` varchar(40) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `archiving_box_user`
--

CREATE TABLE `archiving_box_user` (
  `id` int(11) NOT NULL,
  `archiving_box_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `serial_number` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `name`, `updated_at`, `created_at`) VALUES
(1, 'Mzuzu', '2018-02-06 03:49:51', '2018-01-25 00:09:36'),
(2, 'Limbe', '2018-02-06 03:48:13', '2018-02-06 09:48:13'),
(3, 'Lilongwe', '2018-02-06 03:48:49', '2018-02-06 09:48:49'),
(4, 'Blantyre', '2018-02-06 03:49:07', '2018-02-06 09:49:07'),
(5, 'Capital City', '2018-02-06 03:49:29', '2018-02-06 09:49:29'),
(6, 'First Corporates Services', '2018-02-06 03:50:25', '2018-02-06 09:50:25'),
(7, 'Zomba', '2018-02-06 03:50:44', '2018-02-06 09:50:44'),
(8, 'Chichiri', '2018-02-06 03:51:05', '2018-02-06 09:51:05'),
(9, 'Cross Roads', '2018-02-06 03:51:18', '2018-02-06 09:51:18'),
(10, 'Kanengo', '2018-02-06 03:51:29', '2018-02-06 09:51:29'),
(11, 'Dwangwa', '2018-02-06 03:51:41', '2018-02-06 09:51:41'),
(12, 'Kasungu', '2018-02-06 03:51:51', '2018-02-06 09:51:51'),
(13, 'Karonga', '2018-02-06 03:52:00', '2018-02-06 09:52:00'),
(14, 'Dedza', '2018-02-06 03:53:47', '2018-02-06 09:53:47'),
(15, 'Mponela', '2018-02-06 03:54:00', '2018-02-06 09:54:00'),
(16, 'Mchinji', '2018-02-06 03:54:12', '2018-02-06 09:54:12'),
(17, 'Liwonde', '2018-02-06 03:54:36', '2018-02-06 09:54:36'),
(18, 'Mangochi', '2018-02-06 03:54:47', '2018-02-06 09:54:47'),
(19, 'Rumphi', '2018-02-06 03:54:59', '2018-02-06 09:54:59'),
(20, 'Mzimba', '2018-02-06 03:55:08', '2018-02-06 09:55:08'),
(21, 'Nkhatabay', '2018-02-06 03:55:18', '2018-02-06 09:55:18'),
(22, 'Likuni', '2018-02-06 03:55:27', '2018-02-06 09:55:27'),
(23, 'Area 2', '2018-02-06 03:55:41', '2018-02-06 09:55:41'),
(24, 'City Mall', '2018-02-06 03:55:55', '2018-02-06 09:55:55'),
(25, 'Ndirande', '2018-02-06 03:56:06', '2018-02-06 09:56:06'),
(26, 'Salima', '2018-02-06 03:56:15', '2018-02-06 09:56:15'),
(27, 'BT RPC', '2018-02-06 03:56:32', '2018-02-06 09:56:32'),
(28, 'Mulanje', '2018-02-06 03:56:47', '2018-02-06 09:56:47'),
(29, 'Haile Selassie', '2018-02-06 03:57:12', '2018-02-06 09:57:12'),
(30, 'Gateway Mall', '2018-02-06 03:57:27', '2018-02-06 09:57:27'),
(31, 'Kamuzu procession', '2018-02-06 03:57:48', '2018-02-06 09:57:48'),
(32, 'Limbe Central', '2018-02-06 03:58:09', '2018-02-06 09:58:09'),
(33, 'Malangalanga', '2018-02-06 03:58:34', '2018-02-06 09:58:34'),
(34, 'Area 25', '2018-02-06 03:58:51', '2018-02-06 09:58:51'),
(35, 'Blantyre Market', '2018-02-06 04:00:03', '2018-02-06 10:00:03'),
(36, 'Lujeri', '2018-02-06 04:00:22', '2018-02-06 10:00:22'),
(37, 'Mitundu', '2018-02-06 04:02:01', '2018-02-06 10:02:01');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `customer_number` varchar(20) NOT NULL,
  `customer_type_id` int(11) DEFAULT NULL,
  `business_name` varchar(200) DEFAULT NULL,
  `group_name` varchar(200) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `maiden_name` varchar(100) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `customer_modification_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_modifications`
--

CREATE TABLE `customer_modifications` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_number` varchar(20) DEFAULT NULL,
  `customer_type_id` int(11) DEFAULT NULL,
  `business_name` varchar(200) DEFAULT NULL,
  `group_name` varchar(200) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `maiden_name` varchar(100) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `initiator_id` int(11) DEFAULT NULL,
  `verifier_id` int(11) DEFAULT NULL,
  `authorization_status` tinyint(2) DEFAULT NULL,
  `verifier_comment` varchar(200) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_types`
--

CREATE TABLE `customer_types` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_types`
--

INSERT INTO `customer_types` (`id`, `name`, `updated_at`, `created_at`) VALUES
(1, 'Personal', '2018-01-24 14:25:03', '2018-01-25 00:25:03'),
(2, 'Group', '2018-01-24 14:25:17', '2018-01-25 00:25:17'),
(3, 'Business', '2018-01-24 14:25:17', '2018-01-25 00:25:17');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `document_modification_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `document_files`
--

CREATE TABLE `document_files` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `document_file_type_id` int(11) DEFAULT NULL,
  `document_file_modification_id` int(11) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `pages` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `document_file_modifications`
--

CREATE TABLE `document_file_modifications` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(100) DEFAULT '',
  `document_modification_id` int(11) DEFAULT NULL,
  `document_file_type_id` int(11) DEFAULT NULL,
  `pages` varchar(45) DEFAULT NULL,
  `authorization_status` tinyint(1) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `document_file_types`
--

CREATE TABLE `document_file_types` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `document_file_types`
--

INSERT INTO `document_file_types` (`id`, `name`, `updated_at`, `created_at`) VALUES
(1, 'Account Forms File', '2018-02-27 05:47:46', '2018-02-27 09:47:46'),
(2, 'Customer Attachments File', '2018-02-27 05:47:46', '2018-02-27 09:47:46'),
(3, 'Customer Signature File', '2018-02-27 05:47:58', '2018-02-27 09:47:58');

-- --------------------------------------------------------

--
-- Table structure for table `document_modifications`
--

CREATE TABLE `document_modifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_modification_id` int(11) DEFAULT NULL,
  `account_modification_id` int(11) DEFAULT NULL,
  `authorization_status` int(2) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `entities`
--

CREATE TABLE `entities` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `table_name` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entities`
--

INSERT INTO `entities` (`id`, `name`, `table_name`, `updated_at`, `created_at`) VALUES
(1, 'user', 'users', '2017-10-08 07:06:23', '2017-10-08 19:06:23'),
(2, 'customer', 'customers', '2018-01-30 21:32:30', '2017-10-08 19:06:23'),
(3, 'account', 'accounts', '2018-01-30 21:32:43', '2017-10-08 19:08:34'),
(4, 'document', 'documents', '2018-01-30 21:32:58', '2017-10-08 19:08:34'),
(5, 'document_file', 'document_files', '2018-01-30 21:33:10', '2018-01-31 07:30:07'),
(6, 'customer_modification', 'customer_modifications', '2018-01-30 21:33:47', '2018-01-31 07:30:07'),
(7, 'account_modification', 'account_modifications', '2018-02-02 16:01:00', '2018-01-31 07:30:45'),
(8, 'document_modification', 'document_modification', '2018-02-02 16:02:31', '2018-01-31 07:30:45'),
(9, 'document_file_modification', 'document_file_modification', '2018-02-02 16:02:25', '2018-02-02 21:20:07'),
(10, 'archiving_box', 'archiving_boxes', '2018-02-02 16:02:10', '2018-02-02 22:01:45'),
(11, 'user_modification', 'user_modifications', '2018-02-27 12:54:30', '2018-02-27 14:54:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_modification_id` int(11) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `branch_id` varchar(45) DEFAULT NULL,
  `access_level_id` int(11) DEFAULT NULL,
  `active` tinyint(2) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `remember_token` varchar(150) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_modification_id`, `username`, `first_name`, `last_name`, `branch_id`, `access_level_id`, `active`, `password`, `remember_token`, `updated_at`, `created_at`) VALUES
(1, 1, 'mhoni3G', 'Lawrence', 'Mhoni', NULL, 7, 1, '$2y$10$ASGnoRYbzjS5C2vIg3Hep.SN4ePr1YVjzoIZJ94tyBTgW81m6/vrO', 'FzdlesF8hJz2R8F0Rzs2DDRCwnEcaBlYlHdlaVRu2wYguS1QyerttJGxYXm5', '2018-03-12 11:42:13', '2018-01-24 19:12:59'),
(2, 29, 'PESHE', 'PATIENCE', 'BANDA', '5', 5, 1, NULL, NULL, '2018-03-09 22:37:36', '2018-02-06 09:37:52'),
(3, 3, 'Lupiya', 'Isaac', 'Lupiya', NULL, 7, 1, '$2y$10$23FpR1Cv6iYzJX7y52j6h.zIJrXHf.rhc/1iuOtTfE/A82wGoxOT.', 'AHd8kvLf1bWu7sNo3jjdCndKL6V11f9KxqbcXhYWrW1SlmRMqRd7NYDd5QvS', '2018-03-12 08:05:38', '2018-02-06 09:39:42'),
(4, 4, 'TAMMIE MAGNIFICO', 'Etta', 'Mlaliki', NULL, 7, 1, '$2y$10$3oHINNb6JqELWti7SYKJtujHIq4yiJn3D2nHMk5WzuWk4lCJ/d/w6', '21P84b5ZboJhAlyksDUz5VXoL1rCpxkniTXRkzbcMg5rOUDwRQIo5LwQR9zS', '2018-02-16 03:53:44', '2018-02-06 09:40:36'),
(5, 5, 'NESTAR', 'NESTAR', 'NAMACHA', NULL, 5, 1, '$2y$10$m0q64reaBuQ.kAvPxDDcUeMyH7bPeXY18G6JOC9MW.qDj1S7Tr0sG', NULL, '2018-02-06 05:38:15', '2018-02-06 09:41:42'),
(6, 30, 'SASPUE', 'Samuel', 'Chitsulo', NULL, 5, 1, NULL, NULL, '2018-03-09 23:24:46', '2018-02-06 09:42:35'),
(7, 8, 'mayB', 'May', 'Bikoko', NULL, 2, 1, '$2y$10$SzgldRJPuluIwUwhLmOasuTMZyaHMvF4m0iPhTSjeZnFWgYwkBHLS', NULL, '2018-02-14 01:03:57', '2018-02-14 06:03:12'),
(18, 15, 'Jdoe', 'John', 'Doe', NULL, 3, 1, '$2y$10$cAYjSZtqLMYxab93MSlZqeUlWksltO4ZG.F1Vk08ev/lCttofyWze', 'EwDHqLNlg73pvO0hzDRk6tXHuhwplTB1X4wLhaCTY1QkhhV3h4Cieb47g32Y', '2018-02-27 12:00:56', '2018-02-27 12:03:04'),
(20, 17, 'bond', 'James', 'Bond', NULL, 2, 1, '$2y$10$LPc.V1VPBDTzn8uXd7DkEO7ybrLba46oBX.nBgxX3G8o7cZyIMQW2', NULL, '2018-02-27 11:01:08', '2018-02-27 13:01:08'),
(21, 50, 'Jane', 'Jane', 'Doe', NULL, 5, 1, NULL, 'QdtpLMvQomOeHDD4qqLAMoKDA1hjY6UQyoAfln6nadamvHa1yPR2zuKGcigG', '2018-03-10 00:31:38', '2018-03-10 01:32:57');

-- --------------------------------------------------------

--
-- Table structure for table `user_access_permission`
--

CREATE TABLE `user_access_permission` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access_permission_id` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_access_permission`
--

INSERT INTO `user_access_permission` (`id`, `user_id`, `access_permission_id`, `updated_at`, `created_at`) VALUES
(80, 1, 1, '2018-02-14 08:00:30', '2018-02-14 12:00:30'),
(81, 1, 2, '2018-02-14 08:00:30', '2018-02-14 12:00:30'),
(82, 1, 3, '2018-02-14 08:00:30', '2018-02-14 12:00:30'),
(83, 1, 4, '2018-02-14 08:00:30', '2018-02-14 12:00:30'),
(84, 1, 5, '2018-02-14 08:00:30', '2018-02-14 12:00:30'),
(85, 1, 6, '2018-02-14 08:00:30', '2018-02-14 12:00:30'),
(86, 1, 7, '2018-02-14 08:00:30', '2018-02-14 12:00:30'),
(87, 1, 8, '2018-02-14 08:00:30', '2018-02-14 12:00:30'),
(94, 1, 10, '2018-02-27 12:27:01', '2018-02-27 14:27:01'),
(155, 1, 9, '2018-02-27 14:23:22', '2018-02-27 16:23:22'),
(156, 1, 11, '2018-03-09 20:31:12', '2018-03-09 22:31:12'),
(157, 1, 12, '2018-03-09 20:31:12', '2018-03-09 22:31:12'),
(159, 2, 3, '2018-03-10 00:37:37', '2018-03-10 02:37:37'),
(160, 2, 11, '2018-03-10 00:37:37', '2018-03-10 02:37:37'),
(161, 2, 12, '2018-03-10 00:37:37', '2018-03-10 02:37:37'),
(162, 6, 3, '2018-03-10 01:24:46', '2018-03-10 03:24:46'),
(163, 6, 11, '2018-03-10 01:24:46', '2018-03-10 03:24:46'),
(164, 6, 12, '2018-03-10 01:24:46', '2018-03-10 03:24:46'),
(165, 6, 8, '2018-03-10 01:24:46', '2018-03-10 03:24:46'),
(166, 6, 9, '2018-03-10 01:24:46', '2018-03-10 03:24:46');

-- --------------------------------------------------------

--
-- Table structure for table `user_activities`
--

CREATE TABLE `user_activities` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activity_type_id` int(11) NOT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `user_session_id` int(11) DEFAULT NULL,
  `entity_primary_value` varchar(100) DEFAULT NULL,
  `entity_reference_field` varchar(45) DEFAULT NULL,
  `entity_reference_value` varchar(100) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_modifications`
--

CREATE TABLE `user_modifications` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `initiator_id` int(11) DEFAULT NULL,
  `verifier_id` int(11) DEFAULT NULL,
  `branch_id` varchar(45) DEFAULT NULL,
  `security_modification` tinyint(1) DEFAULT '0',
  `access_level_id` int(11) DEFAULT NULL,
  `active` tinyint(2) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `authorization_status` varchar(45) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_modifications`
--

INSERT INTO `user_modifications` (`id`, `username`, `user_id`, `first_name`, `last_name`, `initiator_id`, `verifier_id`, `branch_id`, `security_modification`, `access_level_id`, `active`, `password`, `authorization_status`, `updated_at`, `created_at`) VALUES
(1, 'mhoni3G', 1, 'Lawrence', 'Mhoni', NULL, NULL, NULL, 0, 2, 1, '$2y$10$tgnzCV4BSWCcUcuApgwZy.mUxyyHODNk3rkcaWrahPfuqsrQCdl2u', '1', '2018-02-27 13:20:02', '2018-01-24 19:12:59'),
(2, 'PESHE', 2, 'PATIENCE', 'BANDA', 1, 1, NULL, 0, 5, 1, '$2y$10$g6tIhtp5UYkxcRbTQw6cJeRfo/65MnnNSBp7pOwZ3gytSmcaaxKvC', '1', '2018-02-27 13:20:02', '2018-02-06 09:37:52'),
(3, 'Lupiya', 3, 'Isaac', 'Lupiya', 1, 1, NULL, 0, 5, 1, '$2y$10$23FpR1Cv6iYzJX7y52j6h.zIJrXHf.rhc/1iuOtTfE/A82wGoxOT.', '1', '2018-02-27 13:20:02', '2018-02-06 09:39:42'),
(4, 'TAMMIE MAGNIFICO', 4, 'Etta', 'Mlaliki', 1, 1, NULL, 0, 5, 1, '$2y$10$3oHINNb6JqELWti7SYKJtujHIq4yiJn3D2nHMk5WzuWk4lCJ/d/w6', '1', '2018-02-27 13:20:02', '2018-02-06 09:40:36'),
(5, 'NESTAR', 5, 'NESTAR', 'NAMACHA', 1, 1, NULL, 0, 5, 1, '$2y$10$m0q64reaBuQ.kAvPxDDcUeMyH7bPeXY18G6JOC9MW.qDj1S7Tr0sG', '1', '2018-02-27 13:20:02', '2018-02-06 09:41:42'),
(6, 'SASPUE', 6, 'Samuel', 'Chitsulo', 1, 1, NULL, 0, 5, 1, '$2y$10$tBhwjfiuvDzkv/U9UdaZseZwreazB/VIVE4iY5xWNKArH0Sz/pQpC', '1', '2018-02-27 13:20:02', '2018-02-06 09:42:35'),
(8, 'mayB', 7, 'May', 'Bikoko', 1, 1, NULL, 1, 2, 1, '$2y$10$E7Q7BHPGGUbhIF7GhklotecClG2.k0mO0klIBvyDW.CVxeX6ekJ1S', '1', '2018-02-27 13:20:02', '2018-02-14 06:03:12'),
(10, 'Jdoe', NULL, 'John', 'Doe', 1, 1, NULL, 1, 3, 1, '$2y$10$kimwgyzrcBfmxvPIWnWX6elsLMmTXINIWmuYXDqrUx1BUkFEsY9Rq', '1', '2018-02-27 09:48:47', '2018-02-27 11:44:43'),
(11, 'Jdoe', 18, 'John', 'Doe', 1, 1, NULL, 0, 3, 1, NULL, '1', '2018-02-27 10:39:26', '2018-02-27 12:38:26'),
(12, 'Jdoe', 18, 'John', 'Doe', 1, 1, NULL, 1, 3, 1, '$2y$10$VA7OS1Pr4ZRB0ZZw78adiurmB2msWaPFJfuwTakbAJkBV4vSOzDy.', '1', '2018-02-27 10:43:30', '2018-02-27 12:42:31'),
(13, 'Jdoe', 18, 'John', 'Doe', 1, NULL, NULL, 1, 3, 1, '$2y$10$Kj7od3mZq/KCI7W6MFHvPOOyvMYeItDLvZNjUF/3nVgRh/Nqqsj6S', '2', '2018-02-27 13:18:53', '2018-02-27 12:45:20'),
(14, 'Jdoe', 18, 'John', 'Doe', 1, 1, NULL, 0, 3, 1, NULL, '1', '2018-02-27 10:46:46', '2018-02-27 12:45:51'),
(15, 'Jdoe', 18, 'John', 'Doe', 1, 1, NULL, 1, 3, 1, '$2y$10$cAYjSZtqLMYxab93MSlZqeUlWksltO4ZG.F1Vk08ev/lCttofyWze', '1', '2018-02-27 12:00:56', '2018-02-27 12:50:00'),
(17, 'bond', NULL, 'James', 'Bond', 1, 1, NULL, 1, 2, 1, '$2y$10$LPc.V1VPBDTzn8uXd7DkEO7ybrLba46oBX.nBgxX3G8o7cZyIMQW2', '1', '2018-02-27 11:01:08', '2018-02-27 13:00:04'),
(19, 'Jdoe', 18, 'John', 'Doe', 1, 1, NULL, 1, 2, 1, '$2y$10$.Ka7L9jUnRjfDZyMl.F/oemuX3AFeg9.fXDflePJs9YIM9Pu12xwu', '0', '2018-02-27 14:02:53', '2018-02-27 14:02:05'),
(20, 'PESHE', 2, 'PATIENCE', 'BANDA', 1, NULL, NULL, 0, 5, 1, NULL, '0', '2018-03-09 18:54:51', '2018-03-09 20:54:51'),
(21, 'PESHE', 2, 'PATIENCE', 'BANDA', 1, NULL, NULL, 0, 5, 1, NULL, '0', '2018-03-09 18:56:41', '2018-03-09 20:56:41'),
(22, 'PESHE', 2, 'PATIENCE', 'BANDA', 1, NULL, NULL, 0, 5, 1, NULL, '0', '2018-03-09 18:57:15', '2018-03-09 20:57:15'),
(23, 'PESHE', 2, 'PATIENCE', 'BANDA', 1, NULL, NULL, 0, 5, 1, NULL, '0', '2018-03-09 18:57:24', '2018-03-09 20:57:24'),
(24, 'PESHE', 2, 'PATIENCE', 'BANDA', 1, NULL, NULL, 0, 5, 1, NULL, '0', '2018-03-09 18:57:49', '2018-03-09 20:57:49'),
(25, 'PESHE', 2, 'PATIENCE', 'BANDA', 1, NULL, NULL, 0, 5, 1, NULL, '0', '2018-03-09 19:07:31', '2018-03-09 21:07:31'),
(26, 'mhoni3G', 1, 'Lawrence', 'Mhoni', 1, NULL, NULL, 0, 5, 1, NULL, '0', '2018-03-09 22:25:33', '2018-03-10 00:25:33'),
(27, 'PESHE2', 2, 'PATIENCE', 'BANDA', 1, NULL, NULL, 0, 5, 1, NULL, '0', '2018-03-09 22:25:46', '2018-03-10 00:25:46'),
(28, 'PESHE', 2, 'PATIENCE', 'BANDA', 1, 1, NULL, 0, 5, 1, NULL, '1', '2018-03-09 22:37:07', '2018-03-10 00:37:07'),
(29, 'PESHE', 2, 'PATIENCE', 'BANDA', 1, 1, NULL, 0, 5, 1, NULL, '1', '2018-03-09 22:37:36', '2018-03-10 00:37:36'),
(30, 'SASPUE', 6, 'Samuel', 'Chitsulo', 1, 1, NULL, 0, 5, 1, NULL, '1', '2018-03-09 23:24:46', '2018-03-10 01:24:46'),
(31, 'Jane', NULL, 'Jane', 'Doe', 1, 1, NULL, 1, 5, 1, '$2y$10$NnFwthaWi8YH3plIYAdPn.yzxWUtlv1ykwdtHdQrPO6bLi/sy96je', '1', '2018-03-09 23:32:57', '2018-03-10 01:32:57'),
(32, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 0, 5, 1, NULL, '1', '2018-03-09 23:33:39', '2018-03-10 01:33:39'),
(33, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 0, 5, 1, NULL, '1', '2018-03-09 23:34:40', '2018-03-10 01:34:39'),
(34, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 1, 5, 1, '$2y$10$72Or9uZ6RvsHavgfAKYri.2p5Ju1yvxZm0dpIC.oCmes1mVle7BTW', '1', '2018-03-09 23:36:02', '2018-03-10 01:36:02'),
(35, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 0, 5, 1, NULL, '1', '2018-03-10 00:13:50', '2018-03-10 02:13:50'),
(36, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 1, 5, 1, '$2y$10$OJ56BNFBe1QsoPKmSsAb2eogXc3sq0aNoSTjaO1DgteGOEVEpoZba', '1', '2018-03-10 00:14:27', '2018-03-10 02:14:27'),
(37, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 1, 5, 1, '$2y$10$lqg2qeG3HEgWI/uZJ5trSet/EJKAWCXN9fM1Z4jsC.bC6O84mS7xG', '1', '2018-03-10 00:14:43', '2018-03-10 02:14:43'),
(38, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 0, 5, 1, NULL, '1', '2018-03-10 00:15:26', '2018-03-10 02:15:26'),
(39, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 1, 5, 1, '$2y$10$c38nUWwkq3CHgIbkDp8V.eTnZYtK.DqE8IiRVzUUbAkWHlAy.uF/G', '1', '2018-03-10 00:21:56', '2018-03-10 02:21:56'),
(40, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 0, 5, 1, NULL, '1', '2018-03-10 00:22:00', '2018-03-10 02:22:00'),
(41, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 1, 5, 1, '$2y$10$BEd4VR6S6ZNN4AA.XsC4suh0qkGnkfimGvCechx1YiVfyc14IKCIe', '1', '2018-03-10 00:23:45', '2018-03-10 02:23:45'),
(42, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 0, 5, 1, NULL, '1', '2018-03-10 00:23:51', '2018-03-10 02:23:51'),
(43, 'Jane', NULL, 'Jane', 'Doe', 1, NULL, NULL, 1, 5, 1, '$2y$10$E2PWyWGuG.BXXGh0f6SgfeQKCnfLbbqc5TGn1r/3oBy6676M9Zzki', '0', '2018-03-10 00:25:45', '2018-03-10 02:25:45'),
(44, 'Jane', NULL, 'Jane', 'Doe', 1, NULL, NULL, 1, 5, 1, '$2y$10$BobytldcmnnUQbRP8ONVOeogJQzwDdU5ALA7YGFtSBP4NkZwgm5ja', '0', '2018-03-10 00:26:03', '2018-03-10 02:26:03'),
(45, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 1, 5, 1, '$2y$10$IYpbFareJSulYoeePWhM..9lFcJQHHOzlLb0HVQTlBp1p7spvjJRm', '1', '2018-03-10 00:26:12', '2018-03-10 02:26:12'),
(46, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 0, 5, 1, NULL, '1', '2018-03-10 00:26:27', '2018-03-10 02:26:27'),
(47, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 1, 5, 1, '$2y$10$jsHifb/gtTgiE65qFNARM.kBUxZDDGmXV/O.T8pQc7ub5nHVk.awu', '1', '2018-03-10 00:27:43', '2018-03-10 02:27:43'),
(48, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 1, 5, 1, '$2y$10$spwuUMiF5YHR0jRUjrK6CewkOlXTzqyw157JoWMCbv0AFFAC52zku', '1', '2018-03-10 00:30:18', '2018-03-10 02:30:18'),
(49, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 1, 5, 1, '$2y$10$brv9320dAp/RLqE4OxPnheAWBqULTc83HLa27N1XdutQsFZ2Fm3ba', '1', '2018-03-10 00:31:15', '2018-03-10 02:31:15'),
(50, 'Jane', 21, 'Jane', 'Doe', 1, 1, NULL, 0, 5, 1, NULL, '1', '2018-03-10 00:31:38', '2018-03-10 02:31:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_modification_access_permission`
--

CREATE TABLE `user_modification_access_permission` (
  `id` int(11) NOT NULL,
  `user_modification_id` int(11) NOT NULL,
  `access_permission_id` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_modification_access_permission`
--

INSERT INTO `user_modification_access_permission` (`id`, `user_modification_id`, `access_permission_id`, `updated_at`, `created_at`) VALUES
(4, 10, 6, '2018-02-27 11:44:43', '2018-02-27 13:44:43'),
(5, 10, 9, '2018-02-27 11:44:43', '2018-02-27 13:44:43'),
(6, 11, 6, '2018-02-27 12:38:26', '2018-02-27 14:38:26'),
(7, 12, 6, '2018-02-27 12:42:31', '2018-02-27 14:42:31'),
(8, 12, 9, '2018-02-27 12:42:31', '2018-02-27 14:42:31'),
(9, 13, 6, '2018-02-27 12:45:20', '2018-02-27 14:45:20'),
(10, 13, 9, '2018-02-27 12:45:20', '2018-02-27 14:45:20'),
(11, 14, 6, '2018-02-27 12:45:51', '2018-02-27 14:45:51'),
(12, 14, 9, '2018-02-27 12:45:51', '2018-02-27 14:45:51'),
(13, 15, 6, '2018-02-27 12:50:00', '2018-02-27 14:50:00'),
(14, 15, 9, '2018-02-27 12:50:00', '2018-02-27 14:50:00'),
(24, 17, 1, '2018-02-27 13:00:04', '2018-02-27 15:00:04'),
(25, 17, 2, '2018-02-27 13:00:04', '2018-02-27 15:00:04'),
(26, 17, 3, '2018-02-27 13:00:04', '2018-02-27 15:00:04'),
(27, 17, 4, '2018-02-27 13:00:04', '2018-02-27 15:00:04'),
(28, 17, 5, '2018-02-27 13:00:04', '2018-02-27 15:00:04'),
(29, 17, 6, '2018-02-27 13:00:04', '2018-02-27 15:00:04'),
(30, 17, 7, '2018-02-27 13:00:04', '2018-02-27 15:00:04'),
(31, 17, 8, '2018-02-27 13:00:04', '2018-02-27 15:00:04'),
(32, 17, 9, '2018-02-27 13:00:04', '2018-02-27 15:00:04'),
(42, 19, 1, '2018-02-27 14:02:05', '2018-02-27 16:02:05'),
(43, 19, 2, '2018-02-27 14:02:05', '2018-02-27 16:02:05'),
(44, 19, 3, '2018-02-27 14:02:05', '2018-02-27 16:02:05'),
(45, 19, 4, '2018-02-27 14:02:05', '2018-02-27 16:02:05'),
(46, 19, 5, '2018-02-27 14:02:05', '2018-02-27 16:02:05'),
(47, 19, 6, '2018-02-27 14:02:05', '2018-02-27 16:02:05'),
(48, 19, 7, '2018-02-27 14:02:05', '2018-02-27 16:02:05'),
(49, 19, 8, '2018-02-27 14:02:05', '2018-02-27 16:02:05'),
(50, 19, 9, '2018-02-27 14:02:05', '2018-02-27 16:02:05'),
(51, 20, 3, '2018-03-09 20:54:51', '2018-03-09 22:54:51'),
(52, 23, 3, '2018-03-09 20:57:24', '2018-03-09 22:57:24'),
(53, 25, 3, '2018-03-09 21:07:31', '2018-03-09 23:07:31'),
(54, 26, 3, '2018-03-10 00:25:33', '2018-03-10 02:25:33'),
(55, 26, 11, '2018-03-10 00:25:33', '2018-03-10 02:25:33'),
(56, 26, 12, '2018-03-10 00:25:33', '2018-03-10 02:25:33'),
(57, 26, 8, '2018-03-10 00:25:33', '2018-03-10 02:25:33'),
(58, 26, 9, '2018-03-10 00:25:33', '2018-03-10 02:25:33'),
(59, 28, 3, '2018-03-10 00:37:07', '2018-03-10 02:37:07'),
(60, 29, 3, '2018-03-10 00:37:37', '2018-03-10 02:37:37'),
(61, 29, 11, '2018-03-10 00:37:37', '2018-03-10 02:37:37'),
(62, 29, 12, '2018-03-10 00:37:37', '2018-03-10 02:37:37'),
(63, 30, 3, '2018-03-10 01:24:46', '2018-03-10 03:24:46'),
(64, 30, 11, '2018-03-10 01:24:46', '2018-03-10 03:24:46'),
(65, 30, 12, '2018-03-10 01:24:46', '2018-03-10 03:24:46'),
(66, 30, 8, '2018-03-10 01:24:46', '2018-03-10 03:24:46'),
(67, 30, 9, '2018-03-10 01:24:46', '2018-03-10 03:24:46'),
(68, 31, 8, '2018-03-10 01:32:57', '2018-03-10 03:32:57'),
(69, 31, 9, '2018-03-10 01:32:57', '2018-03-10 03:32:57'),
(70, 32, 8, '2018-03-10 01:33:39', '2018-03-10 03:33:39'),
(71, 32, 9, '2018-03-10 01:33:39', '2018-03-10 03:33:39'),
(72, 33, 8, '2018-03-10 01:34:40', '2018-03-10 03:34:40'),
(73, 33, 9, '2018-03-10 01:34:40', '2018-03-10 03:34:40'),
(74, 34, 3, '2018-03-10 01:36:02', '2018-03-10 03:36:02'),
(75, 34, 8, '2018-03-10 01:36:02', '2018-03-10 03:36:02'),
(76, 34, 9, '2018-03-10 01:36:02', '2018-03-10 03:36:02'),
(77, 35, 3, '2018-03-10 02:13:50', '2018-03-10 04:13:50'),
(78, 35, 8, '2018-03-10 02:13:50', '2018-03-10 04:13:50'),
(79, 35, 9, '2018-03-10 02:13:50', '2018-03-10 04:13:50'),
(80, 36, 3, '2018-03-10 02:14:27', '2018-03-10 04:14:27'),
(81, 36, 8, '2018-03-10 02:14:27', '2018-03-10 04:14:27'),
(82, 36, 9, '2018-03-10 02:14:27', '2018-03-10 04:14:27'),
(83, 37, 3, '2018-03-10 02:14:43', '2018-03-10 04:14:43'),
(84, 37, 8, '2018-03-10 02:14:43', '2018-03-10 04:14:43'),
(85, 37, 9, '2018-03-10 02:14:43', '2018-03-10 04:14:43'),
(86, 38, 3, '2018-03-10 02:15:26', '2018-03-10 04:15:26'),
(87, 38, 8, '2018-03-10 02:15:26', '2018-03-10 04:15:26'),
(88, 38, 9, '2018-03-10 02:15:26', '2018-03-10 04:15:26'),
(89, 39, 3, '2018-03-10 02:21:56', '2018-03-10 04:21:56'),
(90, 39, 8, '2018-03-10 02:21:56', '2018-03-10 04:21:56'),
(91, 39, 9, '2018-03-10 02:21:56', '2018-03-10 04:21:56'),
(92, 40, 3, '2018-03-10 02:22:00', '2018-03-10 04:22:00'),
(93, 40, 8, '2018-03-10 02:22:00', '2018-03-10 04:22:00'),
(94, 40, 9, '2018-03-10 02:22:00', '2018-03-10 04:22:00'),
(95, 41, 3, '2018-03-10 02:23:46', '2018-03-10 04:23:46'),
(96, 41, 8, '2018-03-10 02:23:46', '2018-03-10 04:23:46'),
(97, 41, 9, '2018-03-10 02:23:46', '2018-03-10 04:23:46'),
(98, 42, 3, '2018-03-10 02:23:51', '2018-03-10 04:23:51'),
(99, 42, 8, '2018-03-10 02:23:51', '2018-03-10 04:23:51'),
(100, 42, 9, '2018-03-10 02:23:51', '2018-03-10 04:23:51'),
(101, 45, 3, '2018-03-10 02:26:12', '2018-03-10 04:26:12'),
(102, 45, 8, '2018-03-10 02:26:12', '2018-03-10 04:26:12'),
(103, 45, 9, '2018-03-10 02:26:12', '2018-03-10 04:26:12'),
(104, 46, 3, '2018-03-10 02:26:27', '2018-03-10 04:26:27'),
(105, 46, 8, '2018-03-10 02:26:27', '2018-03-10 04:26:27'),
(106, 46, 9, '2018-03-10 02:26:27', '2018-03-10 04:26:27'),
(107, 47, 3, '2018-03-10 02:27:43', '2018-03-10 04:27:43'),
(108, 47, 8, '2018-03-10 02:27:43', '2018-03-10 04:27:43'),
(109, 47, 9, '2018-03-10 02:27:43', '2018-03-10 04:27:43'),
(110, 48, 3, '2018-03-10 02:30:18', '2018-03-10 04:30:18'),
(111, 48, 8, '2018-03-10 02:30:18', '2018-03-10 04:30:18'),
(112, 48, 9, '2018-03-10 02:30:18', '2018-03-10 04:30:18'),
(113, 49, 3, '2018-03-10 02:31:15', '2018-03-10 04:31:15'),
(114, 49, 8, '2018-03-10 02:31:15', '2018-03-10 04:31:15'),
(115, 49, 9, '2018-03-10 02:31:15', '2018-03-10 04:31:15'),
(116, 50, 3, '2018-03-10 02:31:38', '2018-03-10 04:31:38'),
(117, 50, 8, '2018-03-10 02:31:38', '2018-03-10 04:31:38'),
(118, 50, 9, '2018-03-10 02:31:38', '2018-03-10 04:31:38');

-- --------------------------------------------------------

--
-- Table structure for table `user_sessions`
--

CREATE TABLE `user_sessions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_sessions`
--

INSERT INTO `user_sessions` (`id`, `user_id`, `ip_address`, `updated_at`, `created_at`) VALUES
(26, 1, '127.0.0.1', '2018-03-10 07:37:57', '2018-03-10 09:37:57'),
(27, 1, '127.0.0.1', '2018-03-11 11:03:20', '2018-03-11 13:03:20'),
(28, 1, '10.0.12.28', '2018-03-11 12:03:05', '2018-03-11 14:03:05'),
(29, 1, '10.0.11.158', '2018-03-12 05:25:54', '2018-03-12 07:25:54'),
(30, 3, '10.0.11.158', '2018-03-12 05:45:52', '2018-03-12 07:45:52'),
(31, 1, '10.0.11.158', '2018-03-12 06:37:22', '2018-03-12 08:37:22'),
(32, 1, '10.0.11.158', '2018-03-12 07:42:18', '2018-03-12 09:42:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `access_level_access_permission`
--
ALTER TABLE `access_level_access_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_access_level_access_permission_1_idx` (`access_level_id`),
  ADD KEY `fk_access_level_access_permission_2_idx` (`access_permission_id`);

--
-- Indexes for table `access_permissions`
--
ALTER TABLE `access_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_number_UNIQUE` (`account_number`),
  ADD KEY `fk_accounts_2_idx` (`branch_id`),
  ADD KEY `fk_accounts_1_idx` (`customer_id`),
  ADD KEY `fk_accounts_3_idx` (`account_modification_id`),
  ADD KEY `fk_accounts_5_idx` (`archiving_box_id`),
  ADD KEY `fk_accounts_4_idx` (`account_type_id`);

--
-- Indexes for table `account_modifications`
--
ALTER TABLE `account_modifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_modifications_1_idx` (`customer_modification_id`),
  ADD KEY `fk_account_modifications_2_idx` (`initiator_id`),
  ADD KEY `fk_account_modifications_3_idx` (`account_type_id`),
  ADD KEY `fk_account_modifications_4_idx` (`branch_id`),
  ADD KEY `fk_account_modifications_5_idx` (`verifier_id`),
  ADD KEY `fk_account_modifications_6_idx` (`archiving_box_id`),
  ADD KEY `fk_account_modifications_7_idx` (`account_id`);

--
-- Indexes for table `account_types`
--
ALTER TABLE `account_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_types`
--
ALTER TABLE `activity_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `archiving_boxes`
--
ALTER TABLE `archiving_boxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `archiving_box_user`
--
ALTER TABLE `archiving_box_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_archiving_boxes_users_2_idx` (`user_id`),
  ADD KEY `fk_archiving_boxes_users_1_idx` (`archiving_box_id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customer_number_UNIQUE` (`customer_number`),
  ADD KEY `fk_customers_1_idx` (`customer_type_id`),
  ADD KEY `fk_customers_2_idx` (`customer_modification_id`);

--
-- Indexes for table `customer_modifications`
--
ALTER TABLE `customer_modifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_customer_modifications_1_idx` (`initiator_id`),
  ADD KEY `fk_customer_modifications_2_idx` (`customer_type_id`),
  ADD KEY `fk_customer_modifications_3_idx` (`verifier_id`),
  ADD KEY `fk_customer_modifications_4_idx` (`customer_id`);

--
-- Indexes for table `customer_types`
--
ALTER TABLE `customer_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_documents_1_idx` (`account_id`),
  ADD KEY `fk_account_documents_2_idx` (`document_modification_id`),
  ADD KEY `fk_documents_3_idx` (`customer_id`);

--
-- Indexes for table `document_files`
--
ALTER TABLE `document_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_files_1_idx` (`document_id`),
  ADD KEY `fk_account_files_3_idx` (`document_file_modification_id`),
  ADD KEY `fk_document_files_1_idx` (`document_file_type_id`);

--
-- Indexes for table `document_file_modifications`
--
ALTER TABLE `document_file_modifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_modification_files_1_idx` (`user_id`),
  ADD KEY `fk_account_modification_files_3_idx` (`document_modification_id`),
  ADD KEY `fk_document_file_modifications_1_idx` (`document_file_type_id`);

--
-- Indexes for table `document_file_types`
--
ALTER TABLE `document_file_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_modifications`
--
ALTER TABLE `document_modifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_document_modifications_1_idx` (`user_id`),
  ADD KEY `fk_account_document_modifications_2_idx` (`account_modification_id`),
  ADD KEY `fk_document_modifications_3_idx` (`customer_modification_id`);

--
-- Indexes for table `entities`
--
ALTER TABLE `entities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `fk_users_1_idx` (`access_level_id`),
  ADD KEY `fk_users_2_idx` (`user_modification_id`);

--
-- Indexes for table `user_access_permission`
--
ALTER TABLE `user_access_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_access_permission_1_idx` (`user_id`),
  ADD KEY `fk_user_access_permission_2_idx` (`access_permission_id`);

--
-- Indexes for table `user_activities`
--
ALTER TABLE `user_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_activities_2_idx` (`activity_type_id`),
  ADD KEY `fk_user_activities_3_idx` (`entity_id`),
  ADD KEY `fk_user_activities_1_idx` (`user_id`);

--
-- Indexes for table `user_modifications`
--
ALTER TABLE `user_modifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_1_idx` (`access_level_id`),
  ADD KEY `fk_user_modifications_1_idx` (`user_id`),
  ADD KEY `fk_user_modifications_2_idx` (`initiator_id`),
  ADD KEY `fk_user_modifications_3_idx` (`verifier_id`);

--
-- Indexes for table `user_modification_access_permission`
--
ALTER TABLE `user_modification_access_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_access_permission_2_idx` (`access_permission_id`),
  ADD KEY `fk_user_access_permission_10_idx` (`user_modification_id`);

--
-- Indexes for table `user_sessions`
--
ALTER TABLE `user_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_sessions_1_idx` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `access_level_access_permission`
--
ALTER TABLE `access_level_access_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `access_permissions`
--
ALTER TABLE `access_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `account_modifications`
--
ALTER TABLE `account_modifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `account_types`
--
ALTER TABLE `account_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `activity_types`
--
ALTER TABLE `activity_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `archiving_boxes`
--
ALTER TABLE `archiving_boxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `archiving_box_user`
--
ALTER TABLE `archiving_box_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_modifications`
--
ALTER TABLE `customer_modifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_types`
--
ALTER TABLE `customer_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `document_files`
--
ALTER TABLE `document_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `document_file_modifications`
--
ALTER TABLE `document_file_modifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `document_file_types`
--
ALTER TABLE `document_file_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `document_modifications`
--
ALTER TABLE `document_modifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entities`
--
ALTER TABLE `entities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `user_access_permission`
--
ALTER TABLE `user_access_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;
--
-- AUTO_INCREMENT for table `user_activities`
--
ALTER TABLE `user_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_modifications`
--
ALTER TABLE `user_modifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `user_modification_access_permission`
--
ALTER TABLE `user_modification_access_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT for table `user_sessions`
--
ALTER TABLE `user_sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `access_level_access_permission`
--
ALTER TABLE `access_level_access_permission`
  ADD CONSTRAINT `fk_access_level_access_permission_1` FOREIGN KEY (`access_level_id`) REFERENCES `access_levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_access_level_access_permission_2` FOREIGN KEY (`access_permission_id`) REFERENCES `access_permissions` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `fk_accounts_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_accounts_2` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_accounts_3` FOREIGN KEY (`account_modification_id`) REFERENCES `account_modifications` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_accounts_4` FOREIGN KEY (`account_type_id`) REFERENCES `account_types` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_accounts_5` FOREIGN KEY (`archiving_box_id`) REFERENCES `archiving_boxes` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `account_modifications`
--
ALTER TABLE `account_modifications`
  ADD CONSTRAINT `fk_account_modifications_1` FOREIGN KEY (`customer_modification_id`) REFERENCES `customer_modifications` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_account_modifications_2` FOREIGN KEY (`initiator_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_account_modifications_3` FOREIGN KEY (`account_type_id`) REFERENCES `account_types` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_account_modifications_4` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_account_modifications_5` FOREIGN KEY (`verifier_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_account_modifications_6` FOREIGN KEY (`archiving_box_id`) REFERENCES `archiving_boxes` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_account_modifications_7` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `archiving_box_user`
--
ALTER TABLE `archiving_box_user`
  ADD CONSTRAINT `fk_archiving_boxes_users_1` FOREIGN KEY (`archiving_box_id`) REFERENCES `archiving_boxes` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_archiving_boxes_users_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `fk_customers_1` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_types` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_customers_2` FOREIGN KEY (`customer_modification_id`) REFERENCES `customer_modifications` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `customer_modifications`
--
ALTER TABLE `customer_modifications`
  ADD CONSTRAINT `fk_customer_modifications_1` FOREIGN KEY (`initiator_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_customer_modifications_2` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_types` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_customer_modifications_3` FOREIGN KEY (`verifier_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_customer_modifications_4` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `fk_documents_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_documents_2` FOREIGN KEY (`document_modification_id`) REFERENCES `document_modifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_documents_3` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `document_files`
--
ALTER TABLE `document_files`
  ADD CONSTRAINT `fk_account_files_1` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_account_files_3` FOREIGN KEY (`document_file_modification_id`) REFERENCES `document_file_modifications` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_document_files_1` FOREIGN KEY (`document_file_type_id`) REFERENCES `document_file_types` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `document_file_modifications`
--
ALTER TABLE `document_file_modifications`
  ADD CONSTRAINT `fk_account_modification_files_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_account_modification_files_3` FOREIGN KEY (`document_modification_id`) REFERENCES `document_modifications` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_document_file_modifications_1` FOREIGN KEY (`document_file_type_id`) REFERENCES `document_file_types` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `document_modifications`
--
ALTER TABLE `document_modifications`
  ADD CONSTRAINT `fk_account_document_modifications_2` FOREIGN KEY (`account_modification_id`) REFERENCES `account_modifications` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_document_modifications_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_document_modifications_3` FOREIGN KEY (`customer_modification_id`) REFERENCES `customer_modifications` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_1` FOREIGN KEY (`access_level_id`) REFERENCES `access_levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_users_2` FOREIGN KEY (`user_modification_id`) REFERENCES `user_modifications` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `user_access_permission`
--
ALTER TABLE `user_access_permission`
  ADD CONSTRAINT `fk_user_access_permission_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_access_permission_2` FOREIGN KEY (`access_permission_id`) REFERENCES `access_permissions` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `user_activities`
--
ALTER TABLE `user_activities`
  ADD CONSTRAINT `fk_user_activities_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_activities_2` FOREIGN KEY (`activity_type_id`) REFERENCES `activity_types` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_activities_3` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `user_modifications`
--
ALTER TABLE `user_modifications`
  ADD CONSTRAINT `fk_user_modifications_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_modifications_2` FOREIGN KEY (`initiator_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_modifications_3` FOREIGN KEY (`verifier_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_users_10` FOREIGN KEY (`access_level_id`) REFERENCES `access_levels` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `user_modification_access_permission`
--
ALTER TABLE `user_modification_access_permission`
  ADD CONSTRAINT `fk_user_access_permission_10` FOREIGN KEY (`user_modification_id`) REFERENCES `user_modifications` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_access_permission_20` FOREIGN KEY (`access_permission_id`) REFERENCES `access_permissions` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `user_sessions`
--
ALTER TABLE `user_sessions`
  ADD CONSTRAINT `fk_user_sessions_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
