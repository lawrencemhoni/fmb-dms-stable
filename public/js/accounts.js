var previewNode = document.querySelector("#dz-preview-template");

var previewNodeTemplate = previewNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);



var myDropzone = new Dropzone("div#document-signature", { 
	url: "/accounts/files/store-tmp",
	paramName: 'attachment',
	previewTemplate: previewNodeTemplate,
	init: function () {

       this.on("success", function (file, responseText) {

          var _this = this;
          var  originalFileName = file.name;

          if ( originalFileName.search(/SIG/) > -1 ) {
            $(".account_attachment_fields")
              .append('<input type="hidden"  name="files[signature_files][]" value="' + responseText + '" >');
              console.log("Signature files field appended.");

          } else if (originalFileName.search(/CIF/) > -1) {

            $(".account_attachment_fields")
              .append('<input type="hidden"  name="files[account_forms][]" value="' + responseText + '" >');
              console.log("Account files field appended.");
          } else if (originalFileName.search(/ATT/) > -1) {
            $(".account_attachment_fields")
              .append('<input type="hidden"  name="files[customer_attachments][]" value="' + responseText + '" >');
              console.log("Customer attachment files field appended.");
          }

    		  file.finalName = responseText;
          file.previewElement.setAttribute('data-name', responseText );
     	 });

      	this.on('sending', function(file, xhr, formData){
          var customerNumber =  $('[name=account_number]').val();
      		var accountNumber =  $('[name=account_number]').val();
      		var _this = this;
          var  originalFileName = file.name;

          if (customerNumber.length < 6 ) {
            _this.removeFile(file);
            alert("Please provide a valid customer number before uploading.");
            return;
          } else if (accountNumber.length < 9) {
            _this.removeFile(file);
            alert("Please provide a valid account number before uploading.");
            return;
          }

          if (originalFileName.search(/SIG/) > -1 || 
              originalFileName.search(/CIF/) > -1 ||
              originalFileName.search(/ATT/) > -1 ) { 
              formData.append('account_number', customerNumber );
              return;
          } else {
             alert("Please upload a file with a valid prefix.");
             _this.removeFile(file);
          }
        });

        this.on('error', function(file, response) {
            console.log(response);
        });


      this.on("removedfile", function(file) {
        var originalFileName = file.name;
        $(".account_attachment_fields").find('[value="'+ file.finalName + '"]').remove();   
      });

	},


});




$('.delete-account').on('click', function(){

  var accountNumber = $(this).attr('data-account-number');

  var msg  = "You are about to delete the account: " + accountNumber;
      msg += " this action is irreversible. Do you wish to proceed?";

  if(confirm(msg)){
    return true;
  }

  return false;
});




$(function() {
  $("#form-create-business-account").validate({
    // Specify validation rules
    rules: {
    business_name: "required",
    account_number: {
      required : true,
      minlength: 6
    },
    branch_id: "required",
    "attachments[]": "required"
    },
   
    messages: {
    business_name: "Business name is required",
    account_number: "Account number is required",
    branch_id: "Please specify branch",
    "attachments[]": "The file field cannot be empty"

    },

    submitHandler: function(form) {



     if (confirm("You did not include customer attachments")) {
      form.submit();
     }
    

    }
  });


  $("#form-create-group-account").validate({
    // Specify validation rules
    rules: {
    group_name: "required",
    account_number: {
      required : true,
      minlength : 6
    },
    branch_id: "required",
    "attachments[]": "required"
    },
   
    messages: {
    group_name: "Business name is required",
    account_number: "Account number is required",
    branch_id: "Please specify branch",
    "attachments[]": "The file field cannot be empty"

    },

    submitHandler: function(form) {
      form.submit();
    }
  });


  $("#form-create-personal-account").validate({
    // Specify validation rules
    rules: {
    first_name: "required",
    last_name: "required",
    account_number: {
      required : true,
      minlength : 6
    },
    date_of_birth: "required",
    branch_id: "required",
    "attachments[]": "required"
    },
   
    messages: {
    first_name: "First name is required",
    last_name: "Last name is required",
    account_number: "Account number is required",
    date_of_birth: "Date of birth is required",
    branch_id: "Please specify branch",
    "attachments[]": "The file field cannot be empty"

    },

    submitHandler: function(form) {
      form.submit();
    }
  });


  $("#form-edit-business-account").validate({
    // Specify validation rules
    rules: {
    business_name: "required",
    account_number: {
      required : true,
      minlength : 6
    },
    branch_id: "required",
    },
   
    messages: {
    business_name: "Business name is required",
    account_number: "Account number is required",
    branch_id: "Please specify branch",

    },

    submitHandler: function(form) {
      form.submit();
    }
  });


  $("#form-edit-group-account").validate({
    // Specify validation rules
    rules: {
    group_name: "required",
    account_number: {
      required : true,
      minlength : 6
    },
    branch_id: "required",
    },
   
    messages: {
    group_name: "Group name is required",
    account_number: "Account number is required",
    branch_id: "Please specify branch",

    },

    submitHandler: function(form) {
      form.submit();
    }
  });

  $("#form-edit-personal-account").validate({
      // Specify validation rules
      rules: {
      first_name: "required",
      last_name: "required",
      account_number: {
        required : true,
        minlength : 6
      },
      date_of_birth: "required",
      branch_id: "required",
      },
     
      messages: {
      first_name: "First name is required",
      last_name: "Last name is required",
      account_number: "Account number is required",
      date_of_birth: "Date of birth is required",
      branch_id: "Please specify branch",

      },

      submitHandler: function(form) {
        form.submit();
      }
    });

});

