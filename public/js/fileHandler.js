

function FileHandler() {


    PDFJS.workerSrc = "/js/pdf.worker.js";

    this.pdfFile;
    this.pdfPage;

    this.config = {
        url : '',
        scale : 1,
        page : 1,
        maxImageWidth : 750
    };


    this.currentFile = {

      totalPages : 0,
      fileType : '',
      fileFormat : '',
    }

    this.supportedImageFileFormats = [
      'jpg', 
      'JPG', 
      'jpeg',
      'JPEG',
      'png',
      'PNG'
    ];

    this.supportedPdfFileFormats = [
      'pdf'
    ];


    this.determineFileFormat = function () {

      var filePath = this.config.url;

      for (i in this.supportedPdfFileFormats) {

         var fileFormat = this.supportedPdfFileFormats[i];
         var regexp = '.' + fileFormat;
         var matchesArray = filePath.match(regexp);

          if (matchesArray !== null && matchesArray.length > 0) {
              this.currentFile.fileFormat = fileFormat;
              this.currentFile.fileType = 'PDF Document';
              return true;
          }

      }

      for (i in this.supportedImageFileFormats) {

         var fileFormat = this.supportedImageFileFormats[i];
         var regexp = '.' + fileFormat;
         var matchesArray = filePath.match(regexp);

          if (matchesArray !== null && matchesArray.length > 0) {
              this.currentFile.fileFormat = fileFormat;
              this.currentFile.fileType = 'Image';
              return true;
          }

      }

      return false;
    }

    this.getFile = function () {

          if (this.determineFileFormat()) {


            switch (this.currentFile.fileType) {
              case 'PDF Document' :
                this.pdfLoadFile().pdfGetPage().pdfRenderPage();
              break;

              case 'Image' :
                this.imageLoadFile();
              break;

              default :
              console.log('file not supported');

            }

          } else {

              console.log('file not supported');

          }

    }


    this.imageLoadFile = function () {
      var imageObj = new Image(); 
      imageObj.src = this.config.url;

      var scale = this.config.scale;
      
      var maxImageWidth = this.config.maxImageWidth;


      var container = document.getElementById("file-container");
      var div = document.createElement("div");


      div.setAttribute("style", "position: relative");

      container.innerHTML = "";
      container.appendChild(div);


      imageObj.onload = function() {

        console.log(imageObj.width);

        if (imageObj.width > maxImageWidth) {

            var quotient = ( maxImageWidth / imageObj.width )
            
            imageObj.width = imageObj.width * quotient;
            imageObj.height = imageObj.height * quotient;

            console.log('width : ', imageObj.width);
            console.log('height : ', imageObj.height);
        }


        imageObj.width = imageObj.width * scale;
        imageObj.height = imageObj.height * scale;


        var width = imageObj.width;
        var height = imageObj.height;

        var canvas  = document.createElement("canvas");
       
          canvas.width   = width;
          canvas.height  = height;

        var context = canvas.getContext('2d');

        div.appendChild(canvas);

        context.drawImage(imageObj, 0, 0, width, height);
      };


      this.renderDisplays();

    }


    this.pdfLoadFile =  function () {
        this.pdfFile =  PDFJS.getDocument(this.config.url).then(function(pdf) {
            return pdf;
        });

        return this;
    };


    this.pdfGetPage = function (page) {

        var pageNumber = (page !== undefined)? page : this.config.page;


         this.pdfPage  = this.pdfFile.then(function(pdf){

            if (pageNumber > pdf.numPages ) {
                 throw new Error('Whoops!');
            }

            return  pdf.getPage(pageNumber);
        });


        return this;
    };

    this.pdfActualFile = function (page) {

         var file = this.pdfFile.then(function(pdf){

            return pdf;
        });

        return this;
    };

    this.pdfRenderPage =  function() {

        var scale = this.config.scale;
        var container = document.getElementById("file-container");
            
        this.pdfPage.then(function(page) {

          var viewport = page.getViewport(scale);
          var div = document.createElement("div");

          // Set id attribute with page-#{pdf_page_number} format
          div.setAttribute("id", "page-" + (page.pageIndex + 1));

          // This will keep positions of child elements as per our needs
          div.setAttribute("style", "position: relative");

          // Append div within div#container
          container.innerHTML = "";
          container.appendChild(div);

          // Create a new Canvas element
          var canvas = document.createElement("canvas");

          // Append Canvas within div#page-#{pdf_page_number}
          div.appendChild(canvas);

          var context = canvas.getContext('2d');
          canvas.height = viewport.height;
          canvas.width = viewport.width;

          var renderContext = {
            canvasContext: context,
            viewport: viewport
          };

          // Render PDF page
          page.render(renderContext);

        });

        this.renderDisplays();

    };


    this.pdfLoadAndRenderMultiplePages = function () {

      var container = document.getElementById("file-container");
            document.getElementById("file-container").innerHTML = "";

      this.pdfFile.then(function(pdf) {
        // Get div#container and cache it for later use

        // Loop from 1 to total_number_of_pages in PDF document
        for (var i = 1; i <= pdf.numPages; i++) {

            // Get desired page
            pdf.getPage(i).then(function(page) {

              var scale = 1.5;
              var viewport = page.getViewport(scale);
              var div = document.createElement("div");

              // Set id attribute with page-#{pdf_page_number} format
              div.setAttribute("id", "page-" + (page.pageIndex + 1));

              // This will keep positions of child elements as per our needs
              div.setAttribute("style", "position: relative");

              // Append div within div#container
              container.appendChild(div);

              // Create a new Canvas element
              var canvas = document.createElement("canvas");

              // Append Canvas within div#page-#{pdf_page_number}
              div.appendChild(canvas);

              var context = canvas.getContext('2d');
              canvas.height = viewport.height;
              canvas.width = viewport.width;

              var renderContext = {
                canvasContext: context,
                viewport: viewport
              };

              // Render PDF page
              page.render(renderContext);
            });
        }

      });
    };


    this.pdfNextPage =  function () {

        try {

            this.config.page++;
            this.pdfGetPage().pdfRenderPage();
        } catch (e) {

            this.config.page--;

        } 


    };

    this.pdfPreviousPage =  function () {

        if ( this.config.page > 1) {
             this.config.page--;
             this.pdfGetPage().pdfRenderPage();
        }
    };


    this.zoomIn = function() {


      if (this.config.scale < 3) { 
          
          this.config.scale = this.config.scale + .4;

          switch (this.currentFile.fileType) {
            case 'PDF Document' :
              this.pdfGetPage().pdfRenderPage();
            break;

            case 'Image' :
              this.imageLoadFile();
            break;
          }
      }

    };


    this.zoomOut = function() {

       if (this.config.scale > 1) {
            this.config.scale = this.config.scale - .4;
           
          switch (this.currentFile.fileType) {
            case 'PDF Document' :
              this.pdfGetPage().pdfRenderPage();
            break;

            case 'Image' :
              this.imageLoadFile();
            break;
          }
       }

    };


    this.renderDisplays = function() {

        var pageDisplay = document.getElementById("rendered-page-display");
            pageDisplay.value = this.config.page;

        var pageScale = document.getElementById("rendered-scale-display");
            pageScale.value = (this.config.scale * 100).toString() + '%';
    }

    this.fetchFile = function (fileName) {




    }


}



