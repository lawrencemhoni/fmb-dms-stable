var app = angular.module('app', []);


app.controller("AccountVerificationController", function($scope, $http){

	$scope.pendingAccounts = [];
  $scope.verifierComment = null ;

	$scope.currentPendingAccount = null ;

	$scope.fileHandler = new FileHandler();
	$scope.filter = {};
	$scope.filter.box_number = null;
	$scope.filter.serial_number = null;

	$scope.pagination = {};


	this.CUSTOMER_TYPE_ID_PERSONAL = 1;
	this.CUSTOMER_TYPE_ID_GROUP = 2;
	this.CUSTOMER_TYPE_ID_BUSINESS = 3;

	this.testFunc = function () {
			console.log('This works');
	};

	this.isCustomerTypePersonal = function(typeId) {
		if(typeId == this.CUSTOMER_TYPE_ID_PERSONAL) {
			return true;
		}
		return false;
	};

	this.isCustomerTypeGroup = function(typeId) {
		if(typeId == this.CUSTOMER_TYPE_ID_GROUP) {
			return true;
		}
		return false;
	};

	this.isCustomerTypeBusiness = function(typeId) {
		if(typeId == this.CUSTOMER_TYPE_ID_BUSINESS) {
			return true;
		}
		return false;
	};


    this.hasPendingAccounts = function () {
      return $scope.pendingAccounts.length > 0 ? true : false;
    }


    this.nextFilePage = function () {
      $scope.fileHandler.pdfNextPage()
    };

    this.previousFilePage = function () {
      $scope.fileHandler.pdfPreviousPage()
    };


    this.zoomInFilePage = function () {
      $scope.fileHandler.zoomIn()
    };


    this.zoomOutFilePage = function () {
      $scope.fileHandler.zoomOut();

      console.log('Zooming out');
    };


    this.fetchFile = function (fileName) {

        console.log('fetching file : ', fileName);

        var url = '/pending-account-files/' + fileName + '/view';

        $scope.fileHandler.config.url = url;
        $scope.fileHandler.getFile();

    };


    this.findPendingAccount = function (accountModificationId) {

     var url = '/beehive-admin/ajax-pending-accounts/' + accountModificationId;

      $http.get(url, {
       
        method: "GET",
        params : $scope.filter,

      }).success(function(response) {
      	 $scope.currentPendingAccount  = response;

      	 	console.log(response);

      	 	var fileName = response.documents[0].document_file_modifications[0].name;

      	 	console.log($scope.filter);

      	    var url = '/pending-account-files/' + fileName + '/view';

            $scope.fileHandler.config.url = url;

            $scope.fileHandler.getFile();
  
      });



    }


    this.getPendingAccounts = function () {

    	console.log('Right here');

      var url = '/beehive-admin/ajax-pending-accounts';
      var obj = this;

      var params = $scope.filter;
      	  params.page = 1;

      $http.get(url, {
       
        method: "GET",
        params : $scope.filter,

      }).success(function(response) {

      		$scope.pagination.currentPage = response.current_page;
      		$scope.pagination.nextPage = response.next_page;

      	 	$scope.pendingAccounts = response.data;

          if (response.total > 0) {
      	 	   $scope.currentPendingAccount = obj.findPendingAccount(response.data[0].id);
      	  } else {
            $scope.currentPendingAccount = {};
          }
    
      });

    };


    this.authorizePendingAccounts = function () {

    	console.log('Authorizing account');

      var url = '/beehive-admin/ajax-pending-accounts/' + $scope.currentPendingAccount.id + '/authorize';

      var obj = this;

      var params = $scope.filter;
      	  params.page = 1;

      $http.get(url, {
       
        method: "GET",
        params : $scope.filter,

      }).success(function(response) {
      		obj.getPendingAccounts()
      });

    };



    this.unauthorizePendingAccounts = function () {

      var url = '/beehive-admin/ajax-pending-accounts/' + $scope.currentPendingAccount.id + '/unauthorize';

      var obj = this;

      var params = $scope.filter;
          params.page = 1;
          params.verifier_comment = $scope.verifierComment;

          $http.get(url, {
           
            method: "POST",
            params : $scope.filter,

          }).success(function(response) {
              obj.getPendingAccounts()
          });

    };






    this.getMorePendingAccounts = function () {

    	console.log('Right here');

      var url = '/beehive-admin/ajax-pending-accounts';
      var obj = this;

      nextPage = $scope.pagination.currentPage + 1;

      params = $scope.filter;
      params.page = nextPage;

      $http.get(url, {
       
        method: "GET",
        params : params,

      }).success(function(response) {

      		$scope.pagination.currentPage = response.current_page;
      		$scope.pagination.nextPage = response.next_page;

      		console.log(response);

      	 if (response.total > 0) {

      	 	for(acc in response.data) {
      	 		$scope.pendingAccounts.push(response.data[acc]);
      	 	}
      	 }

  
      });

    };


    this.getPendingAccounts();








});