function filterJsonItems(items, filterCallback) {
	var newArr = [];

	for(key in items) {
		var item = items[key];

		if(filterCallback(item)) {
			newArr.push(item);
		}
	}

	return newArr;
}


function findFirstJsonItem(items, filterCallback) {
	var newArr = filterJsonItems(items, filterCallback);
	if(newArr.length > 0) {
		return newArr[0];
	}
	return null;
}

function jsonItemValidate(items, filterCallback) {
	var exists = false;

	for(key in items) {
		var item = items[key];

		if(filterCallback(item)) {
			exists  = true; 
		}
	}

	return exists;

}
