@extends('layouts.master')

@section('main-content')

  <div class="bottom-space-lg clearfix"></div>
  <div class="bottom-space-lg clearfix"></div>




@section('main-content')










  <div class="col-sm-4 pull-center">





      @if (count($errors) > 0)
        <div class="alert alert-danger no-margin">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <div class="bottom-space clearfix"></div>
      @endif
      @if(Session::get('feedback-error'))

      <div class="alert alert-block alert-danger fade in no-margin">
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon mdi mdi-close"></i>
        </button>
         {!! Session::get('feedback-error') !!}
      </div>
      <div class="bottom-space clearfix"></div>


      @endif
      @if(Session::get('feedback-success'))
      <div class="alert alert-block alert-success fade in no-margin">
        <button data-dismiss="alert" class="close close-sm" type="button">
           <i class="icon mdi mdi-close"></i>
        </button>
         {!! Session::get('feedback-success') !!}
      </div>
       <div class="bottom-space clearfix"></div>
      @endif










    <div class="panel panel-default panel-table">
      <div class="panel-heading panel-heading-divider"> Login
      </div>
      <div class="panel-body">

         <div class="bottom-space"></div>
 
         <form action="/auth" method="POST" class="form-horizontal group-border-dashed">        

              <div class="bottom-space"></div>

              <div class="form-group">
                <label class="col-sm-4 control-label">Username</label>
                <div class="col-sm-7">
                  <input name="username" class="form-control" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Password</label>
                <div class="col-sm-7">
                  <input name="password" class="form-control" type="password">
                  {{ csrf_field() }}
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-4 control-label"></label>
                <div class="col-sm-7">
                  <input class="btn btn-success " type="submit" value="Login">
                </div>
              </div>
   



        </form>










      </div>
    </div>
  </div>



@stop