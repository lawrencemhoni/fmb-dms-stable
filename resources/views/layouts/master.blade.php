<!DOCTYPE html>
<html>
<head>
	<title>FMB DMS</title>
	<link rel="stylesheet" href="/css/bootstrap.min.css" />
	<link href="/css/materialdesignicons.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="/css/style.css" />
</head>
<body>


<nav id="header" class="navbar navbar-default navbar-fixed-top">
	<div class="logo-container col-sm-4">
		<div class="logo">
			<img src="/images/logo.png" />
		</div>
	</div>
</nav>



<div id="content-section">

@yield('main-content')

</div>







<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>

</body>
</html>