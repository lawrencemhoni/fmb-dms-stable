@extends('beehiveAdmin.layouts.master')

@section('main-content')


<div class="col-md-12">

  <div class="row">


<!--
      <div class="col-sm-4 bottom-space-xsm no-padding-right-md">
        <div class="panel panel-default panel-table">
          <div class="panel-heading">
             
             <div class="tools">
              <span class="icon mdi mdi-sync"></span>
              <span class="icon mdi mdi-dots-vertical"></span>
            </div>

            Welcome {{ \Auth::user()->first_name }}!
            <span class="panel-subtitle">Please do not forget to logout when you are done.</span>
          </div>
          <div class="panel-body">

              <div class="bottom-space"></div>




              <div class="clearfix bottom-space"></div>


              <div class="bottom-space clearfix"></div>
              <div class="col-md-12">
                <div class="bottom-space-sm">
                  You can pull a report of all customers, accounts and documents that are in the system.
                </div>
              
                <a href="" class="btn btn-default">Pull a report</a>
             
              </div>

        </div>


        <div class="clearfix bottom-space-sm"></div>

      </div>
      </div>

-->

      <div class="col-md-8">

            <div class="panel panel-default panel-table">
              <div class="panel-heading ">
                Most recent users
              </div>
              <div class="panel-body">

                    <div class="bottom-space-sm"></div>

                    <table class="table table-striped table-borderless">

                      <tbody>

                        @foreach($recentUsers as $user)
                        <tr>
                          <td>
                            {{$user->first_name}} {{$user->last_name}} - {{$user->access_level}} <br/>
                            <strong>{{$user->username}}</strong>
                          </td>
                        </tr>
                        @endforeach
            
             
                      </tbody>
                    </table>


              </div>
            </div>

      </div>


      <div class="col-md-4 no-padding-right-md">

            <div class="panel panel-default panel-table">
              <div class="panel-heading ">
                Most recent users
              </div>
              <div class="panel-body">

                    <div class="bottom-space-sm"></div>

                    <table class="table table-striped table-borderless">

                      <tbody>

                        @foreach($recentUsers as $user)
                        <tr>
                          <td>
                            {{$user->first_name}} {{$user->last_name}} - {{$user->access_level}} <br/>
                            <strong>{{$user->username}}</strong>
                          </td>
                        </tr>
                        @endforeach
            
             
                      </tbody>
                    </table>


              </div>
            </div>

      </div>





      <div class="col-md-4 no-padding-right-md">

            <div class="panel panel-default panel-table">
              <div class="panel-heading ">
                Recent Activities
              </div>
              <div class="panel-body">

                    <div class="bottom-space-sm"></div>

                    <table class="table table-striped table-borderless">

                      <tbody>
                        @foreach($recentActivities as $activity)
                        <tr>
                          <td>
                            {{$activity->first_name}} {{$activity->last_name}}
                            {{  \App\Libraries\UserActivityHandler::determineActivityPastActionByTypeId($activity->activity_type_id) }} {{str_replace( "_", " ", $activity->entity_name)}} <br />
                            <strong>{{ $activity->entity_reference_value  }}</strong>
                          </td>
                        
                        </tr>
                        @endforeach
             
                      </tbody>
                    </table>


              </div>
            </div>

      </div>





      <div class="col-md-4">

            <div class="panel panel-default panel-table">
              <div class="panel-heading ">
               Activity summary report
              </div>
              <div class="panel-body">

                    <div class="bottom-space-sm"></div>

                    <div class="col-sm-12 bottom-space">

                    You can pull a summary report to see how users were doing on a particular day or in a period of time. 

                    </div>

                    <div class="col-sm-12">
                      <form method="GET" action="/beehive-admin/perfomance-summary">
                        <div class="form-group col-md-12">
                          <label class="control-label col-md-3 text-right">From</label>
                          <div class="col-md-9">
                            <input class="form-control date-picker" name="from" type="text">
                          </div>
                          
                        </div>
                        <div class="form-group col-md-12">
                          <label class="control-label col-md-3 text-right">To</label>
                          <div class="col-md-9">
                            <input class="form-control date-picker" name="to" type="text">
                          </div>
                          
                        </div>
                        <div class="form-group col-md-12">
                          <label class="control-label col-md-3 text-right"></label>
                          <div class="col-md-9">
                            <input class="btn btn-primary" value="Submit" type="submit">
                          </div>
                          
                        </div>
                  
                      </form>
                    </div>


              </div>
            </div>

      </div>







  </div>


</div>










@stop
