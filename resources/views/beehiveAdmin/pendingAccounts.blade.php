@extends('beehiveAdmin.layouts.master')

@section('header-links')
  <script type="text/javascript" src="/js/pdf.js"></script>
  <script  src="/js/fileHandler.js"></script>
  <script src="/js/angular/angular.min.js"></script>
  <script  src="/js/accountVerificationController.js"></script>
@stop



@section('main-content')


  <div class="col-sm-12" 
       ng-app="app" 
       ng-controller="AccountVerificationController as accountVerification">

    <div  class="panel panel-default panel-table">
      <div class="panel-heading panel-heading-divider">
            <div class="col-md-7">
              Pending Accounts Verification
            </div>

            <div class="col-md-2">
              <input 
                    ng-model="filter.serial_number "
                    placeholder="Serial #" 
                    class="form-control dark" 
                    name="serial_number" 
                    value="{{ \Request::input('serial_number') }}">
            </div>            
            <div class="col-md-2">
              <input 
                    ng-model="filter.box_number "
                    placeholder="Box #" 
                    class="form-control dark" 
                    name="box_number" 
                    value="{{ \Request::input('box_number') }}">
            </div>             
            <div class="col-md-1">
              
              <button ng-click="accountVerification.getPendingAccounts()"
                    class="btn btn-default" >
                    Filter 
                    <i class="icon mdi mdi-sync"></i>
                
              </button>
            </div>           

            <div class="clearfix"></div>
  
      </div>
      <div class="panel-body" >






      <div ng-show="accountVerification.hasPendingAccounts()" >

          @include('beehiveAdmin.layouts.pendingAccountsSideBar')


          <div class="col-md-9">

              <div class="bottom-space"></div>

              <div class="col-md-4"  ng-show="accountVerification.isCustomerTypePersonal(currentPendingAccount.customer_type_id)">
                Customer # : @{{ currentPendingAccount.customer_number }}<br/>
                Name : @{{ currentPendingAccount.first_name }} @{{ currentPendingAccount.last_name }}<br/>
                Date of Birth : @{{ currentPendingAccount.date_of_birth }}<br/>
              </div>

              <div class="col-md-4"  ng-show="accountVerification.isCustomerTypeGroup(currentPendingAccount.customer_type_id)">
                Customer # : @{{ currentPendingAccount.customer_number }}<br/>
                Name : @{{ currentPendingAccount.group_name }}<br/>
              </div>   

              <div class="col-md-4"  ng-show="accountVerification.isCustomerTypeBusiness(currentPendingAccount.customer_type_id)">
                Customer # : @{{ currentPendingAccount.customer_number }}<br/>
                Name : @{{ currentPendingAccount.business_name }}<br/>
              </div>            

              <div class="col-md-4">

                Account # : @{{ currentPendingAccount.account_number }}<br/>
                Account Type : @{{ currentPendingAccount.account_type }}<br/>
                Branch : @{{ currentPendingAccount.branch }}<br/>

              </div>            

              <div class="col-md-4">
                Box # :  @{{ currentPendingAccount.box_number }}<br/>
                Serial # : @{{ currentPendingAccount.serial_number }}<br/>
                Location : @{{ currentPendingAccount.location }}<br/>
              </div>

              <div class="clearfix bottom-space"></div>


              <div class="clearfix bottom-space-sm"></div>

              <div class="col-md-12">
                  <div class="documents-viewer">

                    <div class="col-md-1 files-list text-center">
                        <ul ng-repeat="doc in currentPendingAccount.documents">
                          <li ng-repeat="file in doc.document_file_modifications"
                              ng-click="accountVerification.fetchFile(file.name)"> 
                              <span class="icon mdi mdi-file-pdf text-danger"></span>
                              @{{ file.title }}
                          </li>
                      
                        </ul>

                    </div>

                    <div class="col-md-11">

                        <div class="file-rendering-container file-rendering-section"> 
                          <div class="section-heading col-md-12">
                              
                              <div class="col-md-3">

                                <ul class="controls">
                                  <li> 
                                    Page 
                                    <input class="text-center"  type="text" id="rendered-page-display">
                                  </li>
                                  <li  ng-click="accountVerification.previousFilePage()"> 
                                    <i class="mdi mdi-arrow-left-bold" ></i> 
                                  </li>

                                  <li ng-click=" accountVerification.nextFilePage()"> 
                                    <i class="mdi mdi-arrow-right-bold" ></i> 
                                  </li>
                                </ul>

                              </div>
                              
                              <div class="col-md-3 ">

                                <ul class="controls">
                              <li> 
                                    Zoom 
                                    <input class="text-center" type="text" id="rendered-scale-display">
                                  </li>
                                  <li ng-click=" accountVerification.zoomOutFilePage()"> 
                                    <i class="mdi mdi-magnify-minus" ></i> 
                                  </li>

                                  <li  ng-click=" accountVerification.zoomInFilePage()"> 
                                    <i class="mdi mdi-magnify-plus" ></i> 
                                  </li>
                                </ul>

                              </div>
                
                          </div>
                          <div class="section-body col-md-12 no-padding">
                            <div id="file-container"></div>
                          </div>
                        </div>
                    </div>
                  </div>

              </div>
          </div>
          <div class="col-md-12">

            <div class="bottom-space-sm"></div>

            <div class="col-md-3 pull-right">

                <a ng-click="accountVerification.authorizePendingAccounts()" class="btn btn-success pull-right">
                  <i class="icon mdi mdi-thumb-up"></i>
                </a>
                <a style="margin-right:5px;" 
                    class="btn btn-danger pull-right"
                     data-toggle="modal" 
                    data-target="#reject-entry-modal">
                    <i class="icon mdi mdi-thumb-down"></i>
                </a>
               
            </div>

              <div class="bottom-space"></div> 
          </div>
          <div class="bottom-space clearfix"></div>


      </div>

      <div ng-hide="accountVerification.hasPendingAccounts()" >

        <div class="col-md-12">
          <div class="bottom-space"></div>
          <div class="col-md-12">
              There are no pending accounts.

          </div>

          <div class="bottom-space clearfix"></div>

          <div class="col-md-12">
              <button class="btn btn-primary" 
                      ng-click="accountVerification.getMorePendingAccounts()">
                        
                      Load more
              </button>
          </div>

          <div class="bottom-space clearfix"></div>
        </div>


      </div>











      </div>
    </div>












  <div id="reject-entry-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-responsive">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">You are rejecting an entry</h4>
        </div>
        <div class="modal-body">



            <div class="col-md-12"  ng-show="accountVerification.isCustomerTypePersonal(currentPendingAccount.customer_type_id)">
              Customer # : @{{ currentPendingAccount.customer_number }}<br/>
              Name : @{{ currentPendingAccount.first_name }} @{{ currentPendingAccount.last_name }}<br/>
              Date of Birth : @{{ currentPendingAccount.date_of_birth }}<br/>
            </div>

            <div class="col-md-12"  ng-show="accountVerification.isCustomerTypeGroup(currentPendingAccount.customer_type_id)">
              Customer # : @{{ currentPendingAccount.customer_number }}<br/>
              Name : @{{ currentPendingAccount.group_name }}<br/>
            </div>   

            <div class="col-md-12"  ng-show="accountVerification.isCustomerTypeBusiness(currentPendingAccount.customer_type_id)">
              Customer # : @{{ currentPendingAccount.customer_number }}<br/>
              Name : @{{ currentPendingAccount.business_name }}<br/>
            </div>  


            <div class="col-md-12">
              <div class="bottom-space"></div>

              <strong>Please specify why</strong>
              <div class="bottom-space sm"></div>
              <div>
                <textarea ng-model="verifierComment" style="width: 100%; min-height: 200px;">
                  

                </textarea>

              </div>

            </div>




          <div class="clearfix"></div>

        </div>
        <div class="modal-footer">

          <button ng-click="accountVerification.unauthorizePendingAccounts()"
                  type="button" 
                  class="btn btn-primary" 
                  data-dismiss="modal">
            <span>Submit</span>
          </button>
  


        </div>
      </div>

    </div>
  </div>
























  </div>














@stop