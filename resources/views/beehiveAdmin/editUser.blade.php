@extends('fmbAdmin.layouts.master')

@section('main-content')

  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading panel-heading-divider"> Edit user
      </div>
      <div class="panel-body">

         <div class="bottom-space"></div>

         <form action="/beehive-admin/users/{{$user->id}}/update" method="POST" class="form-horizontal group-border-dashed">

            <div class="col-sm-7">
              <div class="form-group">
                <label class="col-sm-4 control-label">First name</label>
                <div class="col-sm-7">
                  <input name="first_name" value="{{$user->first_name}}" class="form-control" type="text">
                </div>
              </div>         
              <div class="form-group">
                <label class="col-sm-4 control-label">Last name</label>
                <div class="col-sm-7">
                  <input value="{{$user->last_name}}"  name="last_name" class="form-control" type="text">
                </div>
              </div> 

              <div class="bottom-space"></div>

              <div class="form-group">
                <label class="col-sm-4 control-label">Access Level</label>
                <div class="col-sm-7">
   

                <select id="access-level" name="access_level_id" class="form-control filter-field" > 
                    @foreach($accessLevels as $level)
                      @if($level->id == $user->access_level_id)
                      <option value="{{$level->id}}" selected>{{$level->name}}</option>
                      @else 
                      <option value="{{$level->id}}">{{$level->name}}</option>
                      @endif
                     @endforeach
                  </select>


                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-4 control-label">Username</label>
                <div class="col-sm-7">
                  <input value="{{$user->username}}" name="username" class="form-control" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Password</label>
                <div class="col-sm-7">
                  <input name="password" class="form-control" type="password">
                </div>
              </div>     
              <div class="form-group">
                <label class="col-sm-4 control-label">Retype Password</label>
                <div class="col-sm-7">
                  <input name="password" class="form-control" type="password">
                </div>
              </div>        

              <div class="form-group">
                <label class="col-sm-4 control-label">Active</label>
                <div class="col-sm-7">
                  @if($user->active)
                  <input name="active" type="checkbox" checked>
                  @else
                    <input name="active" type="checkbox">
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-4 control-label"></label>
                <div class="col-sm-7">
                  <input class="btn btn-success " type="submit" value="Update">
                </div>
              </div>
            </div>


            <div class="col-sm-4">

              <h5>Access Permissions</h5>

              <div class="user-permissions"></div>



            </div>



        </form>


        <div class="bottom-space-lg clearfix"></div>







      </div>
    </div>
  </div>



@stop



@section('scripts')



<script type="text/javascript" src="/js/helpers.js"></script>
<script type="text/javascript" src="/js/underscore.js"></script>
<script type="text/javascript">

  var accessLevels = {!! $accessLevels->toJson() !!};
  var userAccessPermissions = {!! $user->permissions->toJson() !!};



</script>



<script type="text/template" class="permissions-template">
   

   <% if( permissions.length != 0) { %>
  
  <div class="checklist">

    <% _.each( permissions, function( permission ){ %>


      <div class="form-group">
        
        <div class="col-sm-1">


        <% 
          if(jsonItemValidate(userAccessPermissions, function(userPermission){

            return permission.id ==  userPermission.id ;

          })) {
        %>
        <input type="checkbox" name="access_permissions[]" value="<%- permission.id %>" checked />
        <% } else { %>
        <input type="checkbox" name="access_permissions[]" value="<%- permission.id %>"  />
        <% } %>








        </div>
        <label style="text-align: left !important" class="col-sm-7 control-label"><%- permission.name %></label>
      </div>

    <% }); %>



  </div>

  <% }  else { %>

      <div class="alert alert-info no-margin">      
        No permissions for the selected access level. 
      </div>


  <% }  %>

</script>





<script type="text/javascript">


  var accessLevel = findFirstJsonItem(accessLevels, function(accessLevel) {
    return accessLevel.id == $('#access-level').val();
  });



  _.templateSettings.variable = "permissions";
  var template = _.template(
      $( "script.permissions-template" ).html()
  );
  
  $('.user-permissions').html(template( accessLevel.permissions ));

  $('#access-level').on("change", function() {

    console.log("Changed");

    var accessLevelId = $(this).val();

    var accessLevel = findFirstJsonItem(accessLevels, function(accessLevel) {
      return accessLevel.id == accessLevelId;
    });

    $('.user-permissions').html(template( accessLevel.permissions ));
  });

</script>

@stop