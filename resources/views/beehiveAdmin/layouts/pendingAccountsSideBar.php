
        <div class="col-md-3">

          <table class="table">

            <thead>
              <tr>
                <th>Accounts</th>

              </tr>
              

            </thead>

            <tbody>
              <tr ng-repeat="acc in pendingAccounts">
                <td ng-click="accountVerification.findPendingAccount(acc.id)">
                   <span ng-show="accountVerification.isCustomerTypePersonal(acc.customer_type_id)">
                      {{acc.first_name}}  {{acc.last_name}} <br />
                   </span>
                  
                   <span ng-show="accountVerification.isCustomerTypeGroup(acc.customer_type_id)">
                      {{acc.group_name}} <br />
                   </span>



                   <span ng-show="accountVerification.isCustomerTypeBusiness(acc.customer_type_id)">
                      {{acc.business_name}} <br />
                   </span>
                  
                    <strong>{{acc.customer_number}}</strong>
                    <strong>{{acc.account_number}}</strong>
                </td>
              </tr>




            </tbody>
          </table>

          <div class="col-md-12">
              <button class="btn btn-primary col-md-12" 
                      ng-click="accountVerification.getMorePendingAccounts()">
                        
                      Load more
              </button>
          </div>

        </div>