<!DOCTYPE html>
<html>
<head>
	<title>FMB DMS</title>
	<link rel="stylesheet" href="/css/bootstrap.min.css" />
	<link href="/css/materialdesignicons.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="/css/style.css" />
	@yield('header-links')
</head>
<body>


<nav id="header" class="navbar navbar-default navbar-fixed-top">
	<div class="logo-container col-sm-4">
		<div class="logo">
			<img src="/images/logo.png" />
		</div>
	</div>

	<div class="navs-container col-sm-6 pull-right">


		<ul class="nav navbar-nav navbar-right user-nav">
			<li class="dropdown">
				<a  class="icon-circle" data-toggle="dropdown" role="button" aria-expanded="fale" class="dropdown-toggle">
					<span class="icon mdi mdi-account"></span>
				</a>

				<ul role="menu" class="dropdown-menu">
				
	               <li>
                    	<div class="user-info">
	                      <div class="user-title">{{\Auth::user()->username}}</div>
	                      <div class="user-subtitle">{{\Auth::user()->first_name}} {{\Auth::user()->last_name}}</div>
	                    </div>
	                </li>
	                <li>
	                	<a href="/clerks/my-account-settings">
                      <span class="icon mdi mdi-settings"></span> Settings
                      </a>
	                </li>
	    			<li>
	    				<a href="/logout"><span class="icon mdi mdi-power">
	    				</span> Logout</a>
	    			</li>

				</ul>
			</li>
		</ul>

		<ul class="nav navbar-nav navbar-right navigation">
			<li>
				<a data-toggle="dropdown" role="button" aria-expanded="fale" class="dropdown-toggle"><span class="icon mdi mdi-apps"></span></a>

				<ul role="menu" class="dropdown-menu">
					<li>

		                <div class="navigation-content">
		                  
		                    <div class="row">
		                      <div class="col-xs-4 navigation-item">
		                        <a href="/admin/dashboard" class="connection-item">
		                        	<i class="icon mdi mdi-view-dashboard"></i>
		                        	<span>Home</span>
		                        </a>
		                      </div>


		                      <div class="col-xs-4 navigation-item"><a href="/beehive-admin/pending-accounts" class="connection-item">
		                      	<i class="icon mdi mdi-dots-horizontal"></i>
		                      	<span>Pending</span></a>
		                      </div>

                      		  <div class="col-xs-4 navigation-item">
		                        <a href="/beehive-admin/accounts" class="connection-item">
		                        	<i class="icon mdi mdi-folder-account"></i>
		                        	<span>Accounts</span>
		                        </a>
		                      </div>

		                      <div class="col-xs-4 navigation-item">
		                        <a href="/beehive-admin/accounts-report" class="connection-item">
		                        	<i class="icon mdi mdi-chart-timeline"></i>
		                        	<span>Reports</span>
		                        </a>
		                      </div>

		                      <div class="col-xs-4 navigation-item">
		                      	<a href="/beehive-admin/activities" class="connection-item">
			                      	<i class="icon mdi mdi-history"></i>
			                      	<span>Audit Trail</span>
		                      	</a>
		                      </div>

		                      <div class="col-xs-4 navigation-item">
		                        <a href="/beehive-admin/users" class="connection-item">
		                        	<i class="icon mdi mdi-account"></i>
		                        	<span>Users</span>
		                        </a>
		                      </div>
		       
		          


		             
		                  </div>
		                </div>
		                <div class="navigation-footer"> <a>Navigation</a></div>

					</li>
				</ul>


			</li>
		</ul>


	

	</div>

</nav>



<div id="content-section">

@yield('main-content')

</div>







<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
@yield('scripts')

</body>
</html>