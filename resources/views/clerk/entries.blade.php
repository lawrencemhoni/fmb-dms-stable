@extends('clerk.layouts.master')

@section('main-content')


  <div class="col-sm-12 bottom-space-sm">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">Filter Accounts
        <div class="tools">
          <a href="/clerks/entries">
            <span class="icon mdi mdi-sync"></span>
          </a>
        </div>
      </div>
      <div class="panel-body">

            <form  method="GET" action="/clerks/entries">

            <div class="filter-accounts col-md-12">

              <div class="form-group col-md-3">
                <label class="col-md-12 control-label">Customer / Account #</label>
                <div class="col-md-12">
                  <input placeholder="Filter number" class="form-control filter-field" name="filter_number" value="{{\Request::input('filter_number')}}">
                </div>
              </div>

              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">Branch</label>
                <div class="col-md-12">

                  <select name="branch" class="form-control filter-field" > 
                    <option value="">All branches</option>

                    @foreach($branches as $branch)

                      @if(\Request('branch') && \Request('branch') == $branch->id) 
                        <option value="{{$branch->id}}" selected>{{$branch->name}}</option>
                      @else
                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                      @endif

                     @endforeach
                  </select>
              
                </div>
              </div>

              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">Type</label>
                <div class="col-md-12">

                  <select name="account_type" class="form-control filter-field" > 
                    <option value="">All types</option>

                    @foreach($accountTypes as $type)

                      @if(\Request('account_type') && \Request('account_type') == $type->id) 
                        <option value="{{$type->id}}" selected>{{$type->name}}</option>
                      @else
                        <option value="{{$type->id}}">{{$type->name}}</option>
                      @endif

                     @endforeach
                  </select>
              
                </div>
              </div>

              <div class="form-group col-md-3">
                <label class="col-md-12 control-label">Name</label>
                <div class="col-md-12">
                  <input placeholder="Account name" 
                         class="form-control filter-field" 
                         name="name" value="{{\Request::input('name')}}">
                </div>
              </div>


              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">Date of birth</label>
                <div class="col-md-12">
                  <input id="date-of-birth" placeholder="Date of birth" class="form-control filter-field" name="date_of_birth" value="{{\Request::input('date_of_birth')}}">
                </div>
              </div>


            </div>

            <div class="col-md-12">

              <div style="padding-left: 40px; padding-top: 10px;" class="col-md-1 form-group">
                <input name="filter" value="1" type="hidden">
            
                <button type="submit" class="btn btn-success btn-filter">Search</button>
              </div>
              

            </div>

        </form>

      </div>
    </div>
  </div>

  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">Results
      </div>
      <div class="panel-body">
        <table class="table table-striped table-borderless">
          <thead>
            <tr>
              <th>Name</th>
              <th>Customer Number</th>
              <th>Account Number</th>
              <th>Type</th>
              <th>Branch</th>
              <th></th>
            </tr>
          </thead>
          <tbody>

            @foreach($accountModifications as $account)
              <tr>
                <td>
                    @if($account->customer_type_id == \App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_PERSONAL) 
                      {{ $account->first_name }} {{ $account->last_name }}
                    @elseif($account->customer_type_id == \App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_GROUP) 
                      {{ $account->group_name }}
                    @elseif($account->customer_type_id == \App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_BUSINESS) 
                      {{ $account->business_name }}
                    @endif
                </td>
                <td>{{$account->customer_number}}</td>
                <td class="number">{{ $account->account_number }}</td>
                <td class="number">{{ $account->account_type_name }}</td>
                <td class="number">{{ $account->branch }}</td>
                <td class="actions">
                  <a href="#" class="icon"><i class="mdi mdi-open-in-new"></i></a>
                  &nbsp;
                  <a href="/clerks/entries/{{$account->id}}/" class="icon text-warning"><i class="mdi mdi-pencil"></i></a>
                </td>
              </tr>
            @endforeach
           

          </tbody>
        </table>
      </div>
    </div>

    {!! $accountModifications->appends(\Request::except('page'))->links() !!}

  </div>




@stop