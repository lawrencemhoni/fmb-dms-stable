@extends('clerk.layouts.master')

@section('main-content')


<div class="col-md-12">

  <div class="row">

      <div class="col-sm-12 bottom-space-xsm">
        <div class="panel panel-default panel-table">
          <div class="panel-heading">
             
             <div class="tools">
              <span class="icon mdi mdi-sync"></span>
              <span class="icon mdi mdi-dots-vertical"></span>
            </div>

            Welcome {{ \Auth::user()->first_name }}!
            <span class="panel-subtitle">Please do not forget to logout when you are done.</span>
          </div>
          <div class="panel-body">

              <div class="bottom-space"></div>




                    
            <div class="col-sm-6">
               @include('clerk.layouts.entriesTodayStats') 
            </div>
            <div class="col-sm-6">
               @include('clerk.layouts.entriesThisMonthStats') 
            </div>
          
  


              <div class="clearfix bottom-space"></div>


              <div class="bottom-space clearfix"></div>
 
        </div>


        <div class="clearfix bottom-space-sm"></div>

      </div>
      </div>



  </div>


</div>






















<div class="col-md-12">

  <div class="row">

      <div class="col-sm-12 bottom-space-xsm">
        <div class="panel panel-default panel-table">
          <div class="panel-heading">
             
             <div class="tools">
              <span class="icon mdi mdi-sync"></span>
              <span class="icon mdi mdi-dots-vertical"></span>
            </div>

            Weekly perfomance chart
           <!--  <span class="panel-subtitle">Please do not forget to logout when you are done.</span> -->
          </div>
          <div class="panel-body">


              <div class="col-md-12">

                <div class="bottom-space "></div>
                <div style="width:100%;">
                    <canvas id="canvas"></canvas>
                </div>
              </div>





 


              <div class="clearfix bottom-space"></div>


              <div class="bottom-space clearfix"></div>
 
        </div>


        <div class="clearfix bottom-space-sm"></div>

      </div>
      </div>



  </div>


</div>







@stop

@section('scripts')



<script src="/js/js-charts/Chart.bundle.js"></script>
<script src="/js/js-charts/utils.js"></script>

<script>


    var MONTHS = {!! json_encode( $weeklyPerformanceSummary['days'] ) !!};

    var config = {
        type: 'line',
        data: {
            labels: {!! json_encode( $weeklyPerformanceSummary['days'] ) !!},
            datasets: [
             {
                label: "Uploads this week",
                fill: true,
                backgroundColor: '#81ecec',
                borderColor: '#81ecec',
                data: {!! json_encode( $weeklyPerformanceSummary['totals'] ) !!}
             }





            ]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text:'Performance Chart'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Day'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Entries'
                    }
                }]
            }
        }
    };

    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = new Chart(ctx, config);
    };

</script>

@stop