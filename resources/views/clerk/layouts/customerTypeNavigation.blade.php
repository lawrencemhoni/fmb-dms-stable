<div class="col-md-1 no-padding-left">

  <div class="vertical-navigation">
    <ul>
      <li>
        <a href="/clerks/customers/{{\App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_PERSONAL}}/accounts/create">
          <i class="icon mdi mdi-account-star"></i>
          <span>Personal</span>
        </a>
      </li>
      <li>
        <a href="/clerks/customers/{{\App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_GROUP}}/accounts/create">
          <i class="icon mdi mdi-account-multiple"></i>
          <span>Group</span>
        </a>
      </li>
      <li>
        <a href="/clerks/customers/{{\App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_BUSINESS}}/accounts/create">
          <i class="icon mdi mdi-briefcase"></i>
          <span>Business</span>
        </a>
      </li>
    </ul>
  </div>

</div>