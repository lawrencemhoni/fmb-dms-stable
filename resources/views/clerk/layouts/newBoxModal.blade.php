
<div id="new-box-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-responsive">

    <!-- Modal content-->
    <div class="modal-content">


     	<form id="form-create-personal-account" 
           action="/archiving-boxes/store" 
           method="POST" 
           class="form-horizontal">

	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add a new box</h4>
	      </div>
	      <div class="modal-body">

	            <div class="bottom-space"></div>

		      	<div class="col-sm-12">
		          <div class="form-group">
		            <label class="col-md-3 control-label">Serial number</label>
		            <div class="col-sm-7">
		              <input name="serial_number" class="form-control" type="text">
		            </div>
		          </div>         
		          <div class="form-group">
		            <label class="col-md-3 control-label">Box number</label>
		            <div class="col-sm-7">
		              <input name="box_number" class="form-control" type="text">
		            </div>
		          </div>        
		          <div class="form-group">
		            <label class="col-md-3 control-label">Location</label>
		            <div class="col-sm-7">
		              <input name="location" class="form-control" type="text">
		            </div> 
		          </div> 
		        </div>      

	        <div class="clearfix"></div>

	      </div>
	      <div class="modal-footer">

			<div class="col-sm-10">
				<button type="submit" class="btn btn-success" >
					<span>Add box</span>
				</button>
			</div>




	      </div>

  	  	</form>  
    </div>

  </div>
</div>