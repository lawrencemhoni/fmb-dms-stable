  @if($currentUserBox)
  <div class="col-sm-12 bottom-space-xsm ">
    <div class="panel panel-default ">
      <div class="panel-heading">
        You are now working on box {{ $currentUserBox->box_number }}
        <div class="tools">
            <button class="btn btn-default" 
                    data-toggle="modal" 
                    data-target="#new-box-modal">
                <span class="icon mdi mdi-plus"></span> 
                New box
            </button>
            <button class="btn btn-default" 
                    data-toggle="modal" 
                    data-target="#search-box-modal">
                <span class="icon mdi mdi-sync"> </span> 
                Search for box
            </button>

        </div>

      </div>
      <div class="panel-body no-padding-top">
          <div class="col-lg-12 no-padding-top">
            Location : {{ $currentUserBox->location }} <br />
            Serial number : {{ $currentUserBox->serial_number }}
          </div>
          

      </div>
    </div>
  </div>
  @else 


  <div class="col-sm-12 bottom-space-xsm ">
    <div class="panel panel-default ">
      <div class="panel-heading">
        You are not working on any box at the moment
        <div class="tools">
            <button class="btn btn-default" 
                    data-toggle="modal" 
                    data-target="#new-box-modal">
                <span class="icon mdi mdi-plus"></span> 
                New box
            </button>
            <button class="btn btn-default" 
                    data-toggle="modal" 
                    data-target="#search-box-modal">
                <span class="icon mdi mdi-sync"> </span> 
                Search for box
            </button>

        </div>

      </div>
      <div class="panel-body no-padding-top">
          <div class="col-lg-12 no-padding-top">

          </div>
          

      </div>
    </div>
  </div>


  @endif