
<div class="col-md-3">
    <div class="bottom-space"></div>
    <div class="account_attachment_fields"></div>
    <div id="attachments-message"></div>

    <div id="dz-preview-template" class="dz-preview dz-file-preview">
          <div class="dz-details">

            <div class="col-sm-10">

            <span class="dz-filename" data-dz-name></span>

            <div class="dz-error-message text-danger">
              File was not uploaded
            </div>
          </div>
          <div  class="col-sm-2" data-dz-remove>
            <span class="icon mdi mdi-delete text-danger"></span>
          </div>
          
      </div>
      <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
            <div class="progress-bar progress-bar-default" style="width:0%;" data-dz-uploadprogress></div>
      </div>
    </div>

      <div class="bottom-space">
        Drag and drop or click in the box

      </div>

     <div id="document-signature" class="attachments-box dz-clickable"></div>
</div>


