    <div class="col-sm-3">
      <div class="panel panel-default panel-table">
        <div class="panel-heading">
          Most recent entries
        </div>
        <div class="panel-body">

                <div class="bottom-space clearfix"></div>
                <table class="table table-striped table-borderless">
                  <tbody>

                    @foreach($mostRecentEntries as $entry)
                    <tr>
                      <td>
                          @if($entry->customer_type_id == \App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_PERSONAL)
                            {{$entry->first_name}}  {{$entry->last_name}}<br />
                          @elseif ($entry->customer_type_id == \App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_GROUP) 
                            {{$entry->group_name}}<br />
                          @elseif($entry->customer_type_id == \App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_BUSINESS) 
                            {{$entry->business_name}}<br />
                          @endif

                          <strong>{{ $entry->account_number }}</strong> - 
                          <strong>{{ $entry->customer_number }}</strong>

                      </td>
                      <td class="actions">
                          <a href="#" class="icon icon-warning"><i class="mdi mdi-pencil"></i></a>
                      </td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>
        </div>
      </div>
    </div>