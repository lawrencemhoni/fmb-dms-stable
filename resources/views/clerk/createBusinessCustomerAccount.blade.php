@extends('clerk.layouts.master')


@section('header-links')

    <link rel="stylesheet" type="text/css" href="/js/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="/js/bootstrap-timepicker/css/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="/js/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="/js/bootstrap-datetimepicker/css/datetimepicker.css" />

@stop



@section('main-content')

  @include('clerk.layouts.currentBoxDisplay')

  <div class="col-sm-9 no-padding-right-md">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">Business Account
      
      </div>
      <div class="panel-body">

        <div class="bottom-space"></div>

        @include('clerk.layouts.customerTypeNavigation')

         <form action="/accounts/store" method="POST" class="form-horizontal group-border-dashed">

            <div class="col-sm-8">

              <div class="bottom-space"></div>


              <div class="form-group">
                <label class="col-md-4 control-label">Customer number</label>
                <div class="col-sm-7">
                  <input name="customer_number" class="form-control" type="text">
                  <input name="customer_type_id" 
                         type="hidden" 
                         value="{{\App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_BUSINESS}}">
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label">Account number</label>
                <div class="col-sm-7">
                  <input name="account_number" class="form-control" type="number">
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-4 control-label">Business name</label>
                <div class="col-sm-7">
                  <input name="business_name" class="form-control" type="text">
                </div>
              </div>             


              <div class="bottom-space"></div>

              <div class="form-group">
                <label class="col-md-4 control-label">Account Type</label>
                <div class="col-sm-7">
                    <select name="account_type_id" class="form-control">
                      <option value="">Select Type</option>
                      @foreach($accountTypes as $type)

                        @if(old('account_type_id') && old('account_type_id') == $type->id )
                          <option value="{{$type->id}}" selected>{{$type->name}}</option>
                        @else
                          <option value="{{$type->id}}">{{$type->name}}</option>
                        @endif

                      @endforeach
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">Branch</label>
                <div class="col-sm-7">
                  <select name="branch_id" class="form-control">
                    <option value="">Select Branch</option>
                    @foreach($branches as $branch)

                      @if(old('branch_id') && old('branch_id') == $branch->id )
                        <option value="{{$branch->id}}" selected>{{$branch->name}}</option>
                      @else
                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                      @endif

                    @endforeach
                  </select>

                </div>
              </div>

            </div>

            @include('clerk.layouts.addAttachment')

            <div class="form-group">
                  
              <div class="col-md-3">
                <div class="bottom-space clearfix"></div>
                <div class="col-sm-7"></div>
                <div class="col-sm-5">
                  <input class="btn btn-success pull-right" type="submit" value="Submit">
                </div>
                
              </div>

            </div>



         </form>


         <div class="bottom-space clearfix"></div>
         <div class="bottom-space clearfix"></div>

      </div>
    </div>
  </div>

  @include('clerk.layouts.mostRecentEntries')
  @include('clerk.layouts.newBoxModal')
  @include('clerk.layouts.searchBoxModal')


@stop



@section('scripts')





<script type="text/javascript" src="/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script src="/js/jquery.validate.min.js"></script>

<!-- <script type="text/javascript" src="/js/underscore.js"></script> -->


<script type="text/javascript" src="/js/dropzone.js"></script>



<script>


$(function(){
    window.prettyPrint && prettyPrint();
    $('.default-date-picker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('.dpYears').datepicker({
        autoclose: true
    });
    $('.dpMonths').datepicker({
        autoclose: true
    });
});



</script>




<script type="text/javascript" src="/js/accounts.js"></script>



<script type="text/javascript">


//$('#existing-account-modal').modal({show:true});


</script>



@stop