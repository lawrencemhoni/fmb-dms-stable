@extends('fmbAdmin.layouts.master')

@section('main-content')

  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading panel-heading-divider"> Create user
      </div>
      <div class="panel-body">

         <div class="bottom-space"></div>
 
         <form action="#" class="form-horizontal group-border-dashed">


            <div class="col-sm-7">
              <div class="form-group">
                <label class="col-sm-4 control-label">First name</label>
                <div class="col-sm-7">
                  <input class="form-control" type="text">
                </div>
              </div>         
              <div class="form-group">
                <label class="col-sm-4 control-label">Last name</label>
                <div class="col-sm-7">
                  <input class="form-control" type="text">
                </div>
              </div>         
              <div class="form-group">
                <label class="col-sm-4 control-label">Branch</label>
                <div class="col-sm-7">
                  <input class="form-control" type="text">
                </div>
              </div>  

              <div class="bottom-space"></div>

              <div class="form-group">
                <label class="col-sm-4 control-label">Username</label>
                <div class="col-sm-7">
                  <input class="form-control" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Password</label>
                <div class="col-sm-7">
                  <input class="form-control" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Access Level</label>
                <div class="col-sm-7">
                  <input class="form-control" type="text">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-4 control-label"></label>
                <div class="col-sm-7">
                  <input class="btn btn-success " type="submit" value="Create">
                </div>
              </div>
            </div>


            <div class="col-sm-3">

              <h4>Access Permissions</h4>

              <div class="form-group">
                <label class="col-sm-5 control-label">Create users</label>
                <div class="col-sm-5">
                  <input class="btn btn-success " type="checkbox" value="Create">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-5 control-label">Update users</label>
                <div class="col-sm-5">
                  <input class="btn btn-success " type="checkbox" value="Create">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-5 control-label">View accounts</label>
                <div class="col-sm-5">
                  <input class="btn btn-success " type="checkbox" value="Create">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-5 control-label">Pull reports</label>
                <div class="col-sm-5">
                  <input class="btn btn-success " type="checkbox" value="Create">
                </div>
              </div>

            </div>



        </form>










      </div>
    </div>
  </div>



@stop