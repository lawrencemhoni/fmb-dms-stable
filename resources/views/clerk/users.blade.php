@extends('fmbAdmin.layouts.master')

@section('main-content')

  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">Users
        <div class="tools"><span class="icon mdi mdi-download"></span><span class="icon mdi mdi-dots-vertical"></span></div>
      </div>
      <div class="panel-body">
        <table class="table table-striped table-borderless">
          <thead>
            <tr>
              <th>Username</th>
              <th>Name</th>
              <th>Branch</th>
              <th>Access Level</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>third-generation</td>
              <td>Lawrence Mhoni</td>
              <td>Blantyre Livingstone Towers</td>
              <td>Superuser</td>
              <td>Active</td>
              <td class="actions"><a href="#" class="icon"><i class="mdi mdi-open-in-new"></i></a></td>
            </tr>
            <tr>
              <td>mayB</td>
              <td>May Bikoko</td>
              <td>Beehive CSE</td>
              <td>Beehive Admin</td>
              <td>Active</td>
              <td class="actions"><a href="#" class="icon"><i class="mdi mdi-open-in-new"></i></a></td>
            </tr>
            <tr>
              <td>sabali</td>
              <td>Patience Banda</td>
              <td>Beehive CSE</td>
              <td>Beehive Admin</td>
              <td>Active</td>
              <td class="actions"><a href="#" class="icon"><i class="mdi mdi-open-in-new"></i></a></td>
            </tr>

          </tbody>
        </table>
      </div>
    </div>
  </div>



@stop