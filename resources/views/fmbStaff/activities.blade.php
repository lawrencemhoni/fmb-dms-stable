@extends('fmbAdmin.layouts.master')

@section('main-content')


  <div class="col-sm-12 bottom-space-sm">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">Filter Activities
        <div class="tools">
          <span class="icon mdi mdi-sync"></span>
        </div>
      </div>
      <div class="panel-body">

        <form  method="GET" action="/admin/activities">

            <div class="filter-accounts col-md-12">

  
          <div class="col-md-12">


              <div class="form-group col-md-3">
                <label class="col-md-12 control-label">Activity types</label>
                <div class="col-md-12">

                  <select name="activity_type" class="form-control filter-field" >  
                    <option value="">All types</option>

                    @foreach($activityTypes as $type)

                      @if(\Request('activity_type') && \Request('activity_type') == $type->id) 
                        <option value="{{$type->id}}" selected>{{$type->name}}</option>
                      @else
                        <option value="{{$type->id}}">{{$type->name}}</option>
                      @endif

                     @endforeach
                  </select>
              
                </div>
              </div>

              <div class="form-group col-md-3">
                <label class="col-md-12 control-label">Entity</label>
                <div class="col-md-12">


                  <select name="entity" class="form-control filter-field" > 
                    <option value="">All entities</option>

                    @foreach($entities as $entity)

                      @if(\Request('entity') && \Request('entity') == $entity->id) 
                        <option value="{{$entity->id}}" selected>{{ ucfirst(str_replace('_', ' ', $entity->name )) }}</option>
                      @else
                        <option value="{{$entity->id}}">{{ ucfirst(str_replace('_', ' ', $entity->name )) }}</option>
                      @endif

                     @endforeach
                  </select>

    
              
                </div>
              </div>


              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">Username</label>
                <div class="col-md-12">
                  <input placeholder="Username" class="form-control filter-field" name="username" value="{{\Request::input('username')}}" />
                </div>
              </div>


              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">From Date</label>
                <div class="col-md-12">
                  <input id="from-date" placeholder="From date" class="form-control filter-field" name="from_date" value="{{\Request::input('from_date')}}" />
                </div>
              </div>

              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">To Date</label>
                <div class="col-md-12">
                  <input id="to-date" placeholder="To date" class="form-control filter-field" name="to_date" value="{{\Request::input('to_date')}}" />
                </div>
              </div>


            </div>


            <div class="col-md-12">

              <div class="form-group col-md-3">
                <label class="col-md-12 control-label">IP address</label>
                <div class="col-md-12">

                  <input placeholder="IP address" class="form-control filter-field" name="ip_address" value="{{\Request::input('ip_address')}}" />
                </div>
              </div>



              <div class="form-group col-md-3">
                <label class="col-md-12 control-label">Entity reference</label>
                <div class="col-md-12">
                  <input placeholder="Reference" class="form-control filter-field" name="entity_reference" value="{{\Request::input('entity_reference')}}" />
                </div>
              </div>
<!-- 
              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">Primary ID</label>
                <div class="col-md-12">
                  <input placeholder="Primary ID" class="form-control filter-field" name="primary_id" value="{{\Request::input('primary_id')}}" />
                </div>
              </div> -->



            </div>


            </div>

            <div class="col-md-12">

              <div style="padding-left: 40px; padding-top: 10px;" class="col-md-1 form-group">
                <input name="filter" value="1" type="hidden">
            
                <button type="submit" class="btn btn-success btn-filter">Search</button>
              </div>
              

            </div>

        </form>
      </div>
    </div>
  </div>

  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">Activities
  
      </div>
      <div class="panel-body">


            <table class="table table-striped table-borderless vertical-align-middle">
               
              <tbody>

                @foreach($activities as $activity)
        
                <tr>

                      <td style="padding: 15px 20px;">
                        <span class="text-default">

                          <a class="text-color-dark-blue" href="/admin/users/{{$activity->user_id}}">
                            <strong>{{$activity->first_name}} {{$activity->last_name}}</strong>
                          </a>
                        </span>
                        <span>
                          {{  \App\Libraries\UserActivityHandler::determineActivityPastActionByTypeId($activity->activity_type_id) }} {{str_replace( "_", " ", $activity->entity_name)}}
                        </span>

                        <span>
                          with {{ str_replace( "_", " ", $activity->entity_reference_field ) }} 
                        </span>
                        
                        @if($activity->entity_id == \App\Libraries\EntityHandler::ENTITY_ID_ACCOUNT)
                        <a class="text-color-dark-blue" href="/admin/accounts/{{$activity->entity_primary_value}}">
                          {{ $activity->entity_reference_value  }}
                        </a>
                        
                        @elseif($activity->entity_id == \App\Libraries\EntityHandler::ENTITY_ID_USER)
                        <a class="text-color-dark-blue" href="/admin/users/{{$activity->entity_primary_value}}">
                          {{ $activity->entity_reference_value  }}
                        </a>
                        
                        @else
                        <a class="text-color-dark-blue" >
                          {{ $activity->entity_reference_value  }}
                        </a>
                        @endif
                        <span>
                          from  {{$activity->ip_address}}
                        </span>
                        <span>
                          on {{ date( "D, F j Y  ", strtotime($activity->created_at) ) }}
                          at {{ date( "H:i a  ", strtotime($activity->created_at) ) }}
                        </span>

                      </td>
                  </tr>
                  @endforeach

              </tbody>

            </table>

      </div>
    </div>

     {!! $activities->appends(\Request::except('page'))->links() !!}

  </div>



@stop



@section('scripts')

<script type="text/javascript" src="/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<script>

$(function(){
    window.prettyPrint && prettyPrint();
    $('#date-of-birth').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('.dpYears').datepicker({
        autoclose: true
    });
    $('.dpMonths').datepicker({
        autoclose: true
    });
});


$(function(){
    window.prettyPrint && prettyPrint();
    $('#from-date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('.dpYears').datepicker({
        autoclose: true
    });
    $('.dpMonths').datepicker({
        autoclose: true
    });
});

$(function(){
    window.prettyPrint && prettyPrint();
    $('#to-date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('.dpYears').datepicker({
        autoclose: true
    });
    $('.dpMonths').datepicker({
        autoclose: true
    });
});


</script>


@stop