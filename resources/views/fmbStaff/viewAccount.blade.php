@extends('fmbStaff.layouts.master')

@section('main-content')

  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">View Account
        <div class="tools">

        </div>
      </div>
      <div class="panel-body">


        <div class="col-md-12">


            <div class="col-md-6">

              <table class="table no-padding table-borderless">
                <tr >
                  <td style="width:50%">Customer number :</td>
                  <td>{{$account->customer_number}}</td>
                </tr>

                @if ($account->customer_type_id == \App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_PERSONAL) 
                <tr>
                  <td>First name :</td>
                  <td>{{$account->first_name}}</td>
                </tr>   
                <tr>
                  <td>Maiden name :</td>
                  <td>{{ $account->maiden_name ? $account->maiden_name : 'N/A' }}</td> 
                </tr>  
                <tr>
                  <td>Last name :</td>
                  <td>{{$account->last_name}}</td> 
                </tr>
                <tr>
                  <td>Date of birth :</td>
                  <td>{{$account->date_of_birth}}</td>
                </tr>
                @elseif($account->customer_type_id == \App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_GROUP) 
                <tr>
                  <td>Group name :</td>
                  <td>{{$account->group_name}}</td>
                </tr> 
                @elseif($account->customer_type_id == \App\Libraries\CustomerHandler::CUSTOMER_TYPE_ID_BUSINESS)
                <tr>
                  <td>Business name :</td>
                  <td>{{$account->business_name}}</td>
                </tr> 
                 @endif
              </table>


              <div class="bottom-space clearfix"></div>


              <table class="table no-padding table-borderless">
                <tr>
                  <td  style="width:50%">Account number :</td>
                  <td>{{$account->account_number}}</td>
                </tr>
                <tr>
                  <td>Account type :</td>
                  <td>{{$account->account_type}}</td>
                </tr>
                <tr>
                  <td>Branch :</td>
                  <td>{{$account->branch}}</td>
                </tr>
              </table>

              <div class="bottom-space clearfix"></div>

              <table class="table no-padding table-borderless">
                <tr>
                  <td  style="width:50%">Serial number :</td>
                  <td>{{$account->serial_number}}</td>
                </tr>
                <tr>
                  <td>Box number :</td>
                  <td>{{$account->box_number}}</td>
                </tr>
                <tr>
                  <td>Location :</td>
                  <td>{{$account->location}}</td>
                </tr>
              </table>

              <div class="bottom-space clearfix"></div>

            </div>

            <div class="col-md-5">
              <h4 class="text-primary">Account Documents</h4>

                <ul class="account-documents-list">

                    @foreach($documents as $document)
                    <li>
                      <div class="col-md-12">
                        <i class="icon mdi mdi-folder"></i>
                        <span>{{$document->name}}</span>
                      </div>
                      <ul>
                        @foreach($document->files as $file)
                        <li>
                          <div class="col-md-9">
                            <i class="icon mdi mdi-file-pdf text-danger"></i>
                            <span>{{$file->title}}</span>

                          </div>
                          <div class="col-md-3">
                            <a target="child" href="/account-files/{{$file->name}}/download">
                              <i class="icon mdi mdi-download "></i>
                            </a>
                            <a target="child" href="/account-files/{{$file->name}}/view">
                              <i class="icon mdi mdi-open-in-new "></i>
                            </a>
                            
                          </div>

                        </li>
                        @endforeach
                      </ul>
                    </li>
                    @endforeach
                </ul>


            </div>






        </div>






      </div>
    </div>




  </div>



@stop