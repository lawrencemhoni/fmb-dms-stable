<!DOCTYPE html>
<html>
<head>
	<title>FMB DMS</title>
	<link rel="stylesheet" href="/css/bootstrap.min.css" />
	<link href="/css/materialdesignicons.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="/css/style.css" />

    <link rel="stylesheet" type="text/css" href="/js/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="/js/bootstrap-timepicker/css/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="/js/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="/js/bootstrap-datetimepicker/css/datetimepicker.css" />

</head>
<body>

@include('fmbStaff.layouts.header')


<div id="content-section">


	@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <div class="bottom-space"></div>
	@endif
	@if(Session::get('feedback-error'))

	<div class="alert alert-block alert-danger fade in">
		<button data-dismiss="alert" class="close close-sm" type="button">
		    <i class="icon mdi mdi-close"></i>
		</button>
		 {!! Session::get('feedback-error') !!}
	</div>



	@endif
	@if(Session::get('feedback-success'))
	<div class="alert alert-block alert-success fade in">
		<button data-dismiss="alert" class="close close-sm" type="button">
		   <i class="icon mdi mdi-close"></i>
		</button>
		 {!! Session::get('feedback-success') !!}
	</div>
	 <div class="bottom-space"></div>
	@endif




@yield('main-content')

</div>







<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
@yield('scripts')

</body>
</html>