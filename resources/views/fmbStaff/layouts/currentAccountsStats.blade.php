 <div class="col-md-12 ">


        <div class="row">

          <div class=" stat-badge background-color-flat-dark-purple col-sm-12 no-padding-right bottom-space-sm">


               <div class="bottom-space clearfix"></div>

              <div class="text-center">
                 <i class="mdi mdi-folder-account text-color-white"></i>
              </div>

              <div >
                <h4 class="text-center text-color-white" >CURRENT ACCOUNTS</h4>
              </div>

               <div class="bottom-space clearfix"></div>   

              <div >
                <h3 class="text-center text-color-white" >
                   {{
                      number_format(  
                          $personalCurrentAccountsCount 
                          + $groupCurrentAccountsCount
                          + $businessCurrentAccountsCount

                      )
                  }}
               </h3>
              </div>
             

              <div class="bottom-space-lg clearfix"></div>
           
              <div class="stat-badge-sm col-sm-4  bottom-space-sm no-border">
                  <div >
                    <h3 class="text-center text-color-white" >PERSONAL</h3>
                    <h4 class="text-center text-color-white" >
                       {{ number_format( $personalCurrentAccountsCount )  }}
                    </h4>
    
                  </div>

              </div>

              <div class="stat-badge-sm col-sm-4  bottom-space-sm">
                  <div >
                    <h3 class="text-center text-color-white" >BUSINESS</h3>
                    <h4 class="text-center text-color-white" >
                       {{ number_format( $businessCurrentAccountsCount ) }}
                   </h4>
                  </div>
              </div>

              <div class="stat-badge-sm col-sm-4  bottom-space-sm">
                <div >
                  <h3 class="text-center text-color-white" >GROUP</h3>
                  <h4 class="text-center text-color-white" >
                     {{ number_format( $groupCurrentAccountsCount ) }}
                  </h4>
            
                  
                </div>

              </div>































            </div>

       
        </div>  <!-- END OF ROW-->

</div> 





