@extends('fmbAdmin.layouts.master')

@section('main-content')

  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">Users
        <div class="tools">
          <a href="/admin/users/create" class="btn btn-default">
            Add user
            <span class="icon mdi mdi-plus"></span>
          </a>
          
        </div>
      </div>
      <div class="panel-body">
        <table class="table table-striped table-borderless">
          <thead>
            <tr>
              <th>Username</th>
              <th>Name</th>
              <th>Access Level</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>

            @foreach($users as $user)
              <tr>
                <td>{{$user->username}}</td>
                <td>{{$user->first_name}} {{$user->last_name}}</td>
                <td>{{$user->access_level_name}}</td>
                <td>{{$user->active == 1 ? 'Active' : 'Not active'}}</td>
                <td class="actions text-right">
<!--                   <a href="#" class="icon"><i class="mdi mdi-open-in-new"></i></a>&nbsp; -->
                  <a href="/admin/users/{{$user->id}}/edit" class="icon text-warning"><i class="mdi mdi-pencil"></i></a>
                </td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>



@stop