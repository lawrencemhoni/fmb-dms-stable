@extends('fmbAdmin.layouts.master')

@section('main-content')

  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading ">Verify user modification
        <div class="tools">
          <a class="btn btn-default" href="">
           <!--    <i class="icon mdi mdi-arrow-back"></i> -->
              Back
        </a>
        </div>
      </div>
      <div class="panel-body">



        <div class="bottom-space"></div>




         @if ($precedingUserModification) 
        <div class="col-md-4">

            <div class="col-md-12">

              <h4>Before</h4>

              <table class="table no-padding table-borderless">

                <tr>
                  <td width="50%">First name :</td>
                  <td>{{$precedingUserModification->first_name}}</td>
                </tr>

                <tr>
                  <td>Last name :</td>
                  <td>{{$precedingUserModification->last_name}}</td>
                </tr> 

                <tr>
                  <td>Username :</td>
                  <td>{{$precedingUserModification->username}}</td>
                </tr> 

                <tr>
                  <td>Status :</td>
                  <td>{{$precedingUserModification->active == 1? 'Active' : 'Not active'}}</td>
                </tr> 

              </table>

              <div class="bottom-space clearfix"></div>


            </div>

            <div class="col-md-5">
              <h4 class="text-primary">Permissions</h4>

                <ul class="account-documents-list">

                    @foreach($precedingUserModification->permissions as $permission)
                    <li>
                        <span>{{$permission->name}}</span>
                    </li>
                    @endforeach
                </ul>


            </div>

           


        </div>
         @endif


        <div class="col-md-4">

            <div class="col-md-12">

              <h4>After</h4>

              <table class="table no-padding table-borderless">

                <tr>
                  <td width="60%">First name :</td>
                  <td>{{$userModification->first_name}}</td>
                </tr>

                <tr>
                  <td>Last name :</td>
                  <td>{{$userModification->last_name}}</td>
                </tr> 

                <tr>
                  <td>Username :</td>
                  <td>{{$userModification->username}}</td>
                </tr> 

                <tr>
                  <td>Status :</td>
                  <td>{{$userModification->active == 1? 'Active' : 'Not active'}}</td>
                </tr> 



              </table>

              <div class="bottom-space clearfix"></div>


            </div>

            <div class="col-md-5">
              <h4 class="text-primary">Permissions</h4>

                <ul class="account-documents-list">

                    @foreach($userModification->permissions as $permission)
                    <li>
                        <span>{{$permission->name}}</span>
                    </li>
                    @endforeach
                </ul>


            </div>
            

        </div>

        <div class="col-md-3" style="background: #eee">

          <div class="bottom-space"></div>

          <h4>Verify modification</h4>
          <div>
              <button class="btn btn-success">Authorize</button>
              <button class="btn btn-danger">Reject</button>
          </div>

          <div class="bottom-space"></div>
        </div>


      </div>
    </div>




  </div>



@stop