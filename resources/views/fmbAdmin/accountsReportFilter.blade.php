@extends('fmbAdmin.layouts.master')

@section('main-content')


  <div class="col-sm-12 bottom-space-sm">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">Generate accounts report
        <div class="tools">
          <a href="/admin/files-report">
            <span class="icon mdi mdi-sync"></span>
          </a>
        </div>
      </div>
      <div class="panel-body">

        <form  method="GET" action="/admin/accounts-report/download">

         <div class="filter-accounts col-md-12">

              <div class="form-group col-md-3">
                <label class="col-md-12 control-label">Customer / Account #</label>
                <div class="col-md-12">
                  <input placeholder="Filter number" class="form-control filter-field" name="filter_number" value="{{\Request::input('filter_number')}}">
                </div>
              </div>

              <div class="form-group col-md-3">
                <label class="col-md-12 control-label">Name</label>
                <div class="col-md-12">
                  <input placeholder="Account name" 
                         class="form-control filter-field" 
                         name="name" value="{{\Request::input('name')}}">
                </div>
              </div>


              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">Branch</label>
                <div class="col-md-12">

                  <select name="branch" class="form-control filter-field" > 
                    <option value="">All branches</option>

                    @foreach($branches as $branch)

                      @if(\Request('branch') && \Request('branch') == $branch->id) 
                        <option value="{{$branch->id}}" selected>{{$branch->name}}</option>
                      @else
                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                      @endif

                     @endforeach
                  </select>
              
                </div>
              </div>

              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">Type</label>
                <div class="col-md-12">

                  <select name="account_type" class="form-control filter-field" > 
                    <option value="">All types</option>

                    @foreach($accountTypes as $type)

                      @if(\Request('account_type') && \Request('account_type') == $type->id) 
                        <option value="{{$type->id}}" selected>{{$type->name}}</option>
                      @else
                        <option value="{{$type->id}}">{{$type->name}}</option>
                      @endif

                     @endforeach
                  </select>
              
                </div>
              </div>




              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">Date of birth</label>
                <div class="col-md-12">
                  <input id="date-of-birth" placeholder="Date of birth" class="form-control filter-field" name="date_of_birth" value="{{\Request::input('date_of_birth')}}">
                </div>
              </div>


              <div class="form-group col-md-3">
                <label class="col-md-12 control-label">Box Serial #</label>
                <div class="col-md-12">
                  <input placeholder="Serial number" class="form-control filter-field" name="serial_number" value="{{\Request::input('serial_number')}}">
                </div>
              </div>

              <div class="form-group col-md-3">
                <label class="col-md-12 control-label">Box #</label>
                <div class="col-md-12">
                  <input placeholder="Box number" class="form-control filter-field" name="box_number" value="{{\Request::input('box_number')}}">
                </div>
              </div>



              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">From date</label>
                <div class="col-md-12">
                  <input id="from-date" placeholder="From date" class="form-control filter-field" name="from_date" value="{{\Request::input('from_date')}}">
                </div>
              </div>

              <div class="form-group col-md-2">
                <label class="col-md-12 control-label">To date</label>
                <div class="col-md-12">
                  <input id="to-date" placeholder="To date" class="form-control filter-field" name="to_date" value="{{\Request::input('to_date')}}">
                </div>
              </div>


            </div>



            <div class="col-md-12">

              <div style="padding-left: 40px; padding-top: 10px;" class="col-md-1 form-group">
                <input name="filter" value="1" type="hidden">
            
                <button type="submit" class="btn btn-success btn-filter">Export to CSV</button>
              </div>
              

            </div>

        </form>
      </div>
    </div>
  </div>





@stop



@section('scripts')

<script type="text/javascript" src="/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<script>

$(function(){
    window.prettyPrint && prettyPrint();
    $('#date-of-birth').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('.dpYears').datepicker({
        autoclose: true
    });
    $('.dpMonths').datepicker({
        autoclose: true
    });
});


$(function(){
    window.prettyPrint && prettyPrint();
    $('#from-date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('.dpYears').datepicker({
        autoclose: true
    });
    $('.dpMonths').datepicker({
        autoclose: true
    });
});

$(function(){
    window.prettyPrint && prettyPrint();
    $('#to-date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('.dpYears').datepicker({
        autoclose: true
    });
    $('.dpMonths').datepicker({
        autoclose: true
    });
});


</script>


@stop