@extends('fmbAdmin.layouts.master')

@section('main-content')

<div class="col-md-12">

  <div class="row">

      <div class="col-sm-12 bottom-space-xsm">
        <div class="panel panel-default panel-table">
          <div class="panel-heading">
             
             <div class="tools">
              <span class="icon mdi mdi-sync"></span>
              <span class="icon mdi mdi-dots-vertical"></span>
            </div>

            Welcome {{ \Auth::user()->first_name }}!
            <span class="panel-subtitle">Please do not forget to logout when you are done.</span>
          </div>
          <div class="panel-body">

              <div class="bottom-space"></div>



        <div class="col-md-12 bottom-space-sm">
            <div class="stat-badge background-color-flat-light-purple col-sm-12 no-padding-right bottom-space-sm">

                <div class="col-sm-12">
                  <div class="col-sm-12">
                    <h4 class="col-sm-8 text-color-white">TOTAL NUMBER OF ACCOUNTS UPLOADED</h4>
                    <div class="col-sm-4 text-color-white text-right">
                      <span style="text-transform: uppercase; font-size: 15px">

                       {{
                          number_format(  
                              $personalSavingsAccountsCount 
                              + $groupSavingsAccountsCount
                              + $businessSavingsAccountsCount
                              + $personalCurrentAccountsCount
                              + $businessCurrentAccountsCount
                              + $groupCurrentAccountsCount

                          )
                      }}

                      </span>
                    </div>
                  </div>

                  </div>
              </div>
        </div>       






 
                  <div class="col-md-6">
                    @include('fmbAdmin.layouts.currentAccountsStats')     
                  </div>
 
                  <div class="col-md-6">
                    @include('fmbAdmin.layouts.savingsAccountsStats')     
                  </div>  


              <div class="clearfix bottom-space"></div>


              <div class="bottom-space clearfix"></div>
              <div class="col-md-12">
                <div class="bottom-space-sm">
                  You can pull a report of all customers, accounts and documents that are in the system.
                </div>
              
                <a href="/admin/accounts-report" class="btn btn-default">Pull a report</a>
             
              </div>

        </div>


        <div class="clearfix bottom-space-sm"></div>

      </div>
      </div>



      <div class="col-md-4 no-padding-right-md">

            <div class="panel panel-default panel-table">
              <div class="panel-heading ">
                Most recent users
              </div>
              <div class="panel-body">

                    <div class="bottom-space-sm"></div>

                    <table class="table table-striped table-borderless">

                      <tbody>

                        @foreach($recentUsers as $user)
                        <tr>
                          <td>
                            {{$user->first_name}} {{$user->last_name}} - {{$user->access_level}} <br/>
                            <strong>{{$user->username}}</strong>
                          </td>
                        </tr>
                        @endforeach
            
             
                      </tbody>
                    </table>


              </div>
            </div>

      </div>





      <div class="col-md-4 no-padding-right-md">

            <div class="panel panel-default panel-table">
              <div class="panel-heading ">
                Recent Activities
              </div>
              <div class="panel-body">

                    <div class="bottom-space-sm"></div>

                    <table class="table table-striped table-borderless">

                      <tbody>
                        @foreach($recentActivities as $activity)
                        <tr>
                          <td>
                            {{$activity->first_name}} {{$activity->last_name}}
                            {{  \App\Libraries\UserActivityHandler::determineActivityPastActionByTypeId($activity->activity_type_id) }} {{str_replace( "_", " ", $activity->entity_name)}} <br />
                            <strong>{{ $activity->entity_reference_value  }}</strong>
                          </td>
                        
                        </tr>
                        @endforeach
             
                      </tbody>
                    </table>


              </div>
            </div>

      </div>









  </div>


</div>









@stop




@section('scripts')

<script type="text/javascript" src="/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<script>



$(function(){
    window.prettyPrint && prettyPrint();
    $('#from-date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('.dpYears').datepicker({
        autoclose: true
    });
    $('.dpMonths').datepicker({
        autoclose: true
    });
});

$(function(){
    window.prettyPrint && prettyPrint();
    $('#to-date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('.dpYears').datepicker({
        autoclose: true
    });
    $('.dpMonths').datepicker({
        autoclose: true
    });
});


</script>


@stop