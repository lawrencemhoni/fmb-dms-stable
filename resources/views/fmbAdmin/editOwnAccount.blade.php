@extends('fmbAdmin.layouts.master')

@section('main-content')

  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading panel-heading-divider"> Update Account
      </div>
      <div class="panel-body">

         <div class="bottom-space"></div>

         <form action="/update-my-account" method="POST" class="form-horizontal group-border-dashed">

            <div class="col-sm-7">
              <div class="form-group">
                <label class="col-sm-4 control-label">First name</label>
                <div class="col-sm-7">
                  <input class="form-control" type="text" disabled value="{{$user->first_name}}">
                </div>
              </div>         
              <div class="form-group">
                <label class="col-sm-4 control-label">Last name</label>
                <div class="col-sm-7">
                  <input class="form-control" type="text" value="{{$user->last_name}}" disabled>
                </div>
              </div> 

              <div class="bottom-space"></div>

              <div class="form-group">
                <label class="col-sm-4 control-label">Username</label>
                <div class="col-sm-7">
                  <input  class="form-control" type="text" value="{{$user->username}}" disabled>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Password</label>
                <div class="col-sm-7">
                  <input name="password" class="form-control" type="password">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-4 control-label"></label>
                <div class="col-sm-7">
                  <input class="btn btn-success " type="submit" value="Update">
                </div>
              </div>
            </div>

        </form>










      </div>
    </div>
  </div>



@stop