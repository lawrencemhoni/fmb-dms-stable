@extends('fmbAdmin.layouts.master')

@section('main-content')

  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">Pending user modifications
      </div>
      <div class="panel-body">
        <table class="table table-striped table-borderless">
          <thead>
            <tr>
              <th>Username</th>
              <th>Name</th>
              <th>Access Level</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>

            @foreach($userModifications as $userModification)
              <tr>
                <td>{{$userModification->username}}</td>
                <td>{{$userModification->first_name}} {{$userModification->last_name}}</td>
                <td>{{$userModification->access_level_name}}</td>
                <td>{{$userModification->active == 1 ? 'Active' : 'Not active'}}</td>
                <td class="actions text-right">
                  <a href="/admin/users/pending/{{$userModification->id}}" class="icon text-primary"><i class="mdi mdi-open-in-new"></i></a>
                </td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>



@stop