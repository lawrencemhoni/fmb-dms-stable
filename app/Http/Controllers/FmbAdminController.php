<?php

namespace App\Http\Controllers;

use \Request;
use \Hash;
use \Auth;
use \Validator;
use \Redirect;
use \App\Libraries\CustomerHandler;
use \App\Libraries\EntityHandler;
use \App\Libraries\UserActivityHandler;
use \App\Libraries\AccountHandler;
use \App\Libraries\DocumentHandler;
use \App\Libraries\DocumentFileHandler;
use \App\Libraries\UserHandler;
use \App\Libraries\AccessLevelHandler;

use App\User;
use App\UserModification;
use App\DocumentFile;


use App\Exceptions\ExistingUserException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class FmbAdminController extends Controller
{
    public function dashboard() {

        $data = [

            'totalCustomersCount' => CustomerHandler::totalCustomersCount(),
            'totalAccountsCount' =>  AccountHandler::totalAccountsCount(),
            'totalDocumentsCount' => DocumentHandler::totalDocumentsCount(),
            'totalDocumentFilesCount' =>   DocumentFileHandler::totalDocumentFilesCount(),

            'recentActivities' => UserActivityHandler::mostRecentActivities(4),
            'recentUsers' => UserHandler::mostRecentUsers(4),

            'personalSavingsAccountsCount' => AccountHandler::cusTypeAccTypeTotalCount(
                CustomerHandler::CUSTOMER_TYPE_ID_PERSONAL,
                AccountHandler::ACCOUNT_TYPE_ID_SAVINGS),

            'personalCurrentAccountsCount' => AccountHandler::cusTypeAccTypeTotalCount(
                CustomerHandler::CUSTOMER_TYPE_ID_PERSONAL,
                AccountHandler::ACCOUNT_TYPE_ID_CURRENT),

            'personalSavingsFilesCount' => DocumentFileHandler::cusTypeAccTypeFilesTotalCount(CustomerHandler::CUSTOMER_TYPE_ID_PERSONAL,
                AccountHandler::ACCOUNT_TYPE_ID_SAVINGS),

            'personalCurrentFilesCount' =>  DocumentFileHandler::cusTypeAccTypeFilesTotalCount(CustomerHandler::CUSTOMER_TYPE_ID_PERSONAL,
                AccountHandler::ACCOUNT_TYPE_ID_CURRENT),

            'businessCurrentAccountsCount' => AccountHandler::cusTypeAccTypeTotalCount(
                CustomerHandler::CUSTOMER_TYPE_ID_BUSINESS,
                AccountHandler::ACCOUNT_TYPE_ID_CURRENT),

            'businessSavingsAccountsCount'=> AccountHandler::cusTypeAccTypeTotalCount(
                CustomerHandler::CUSTOMER_TYPE_ID_BUSINESS,
                AccountHandler::ACCOUNT_TYPE_ID_SAVINGS),

            'businessSavingsFilesCount' => DocumentFileHandler::cusTypeAccTypeFilesTotalCount( 
                CustomerHandler::CUSTOMER_TYPE_ID_BUSINESS,
                AccountHandler::ACCOUNT_TYPE_ID_SAVINGS),

            'businessCurrentFilesCount' => DocumentFileHandler::cusTypeAccTypeFilesTotalCount( 
                CustomerHandler::CUSTOMER_TYPE_ID_BUSINESS,
                AccountHandler::ACCOUNT_TYPE_ID_CURRENT),

            'groupSavingsAccountsCount' => AccountHandler::cusTypeAccTypeTotalCount(
                 CustomerHandler::CUSTOMER_TYPE_ID_GROUP,
                AccountHandler::ACCOUNT_TYPE_ID_SAVINGS),

            'groupCurrentAccountsCount' => AccountHandler::cusTypeAccTypeTotalCount(
                 CustomerHandler::CUSTOMER_TYPE_ID_GROUP,
                AccountHandler::ACCOUNT_TYPE_ID_CURRENT),

            'groupSavingsFilesCount' => DocumentFileHandler::cusTypeAccTypeFilesTotalCount(
                 CustomerHandler::CUSTOMER_TYPE_ID_GROUP,
                AccountHandler::ACCOUNT_TYPE_ID_SAVINGS),

            'groupCurrentFilesCount' => DocumentFileHandler::cusTypeAccTypeFilesTotalCount(
                 CustomerHandler::CUSTOMER_TYPE_ID_GROUP,
                AccountHandler::ACCOUNT_TYPE_ID_CURRENT),

        ];

    	return view('fmbAdmin.dashboard', $data);
    }

    public function accounts() {


        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_VIEW_ACCOUNTS

         ], function () {

            $data = [];

            $data['accountTypes'] = AccountHandler::getAccountTypes();
            $data['branches'] = AccountHandler::getBranches();
            $data['accounts'] = AccountHandler::filterAccounts();

            return view('fmbAdmin.accounts', $data);
         });

    }

    public function accountsReport() {

        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_PULL_REPORTS

             ], function () {

            $data = [];

            $data['accountTypes'] = AccountHandler::getAccountTypes();
            $data['branches'] = AccountHandler::getBranches();
            $data['accounts'] = AccountHandler::filterAccounts();

            return view('fmbAdmin.accountsReportFilter', $data);
        });
    }



    public function downloadAccountsReport() {


        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_PULL_REPORTS

             ], function () {

            $account = AccountHandler::filterReportAccounts();

            \Excel::create('FMB-accounts', function($excel) use ($account)  {

                $excel->sheet('Excel sheet', function($sheet) use ($account)  {

                    $sheet->setOrientation('landscape');
                    $sheet->fromModel( $account );

                });

            })->download('xls');

        });


    }


    public function viewAccount($accountId) {

        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_VIEW_ACCOUNTS

         ], function ()  use($accountId) {

            $data = [];
            
            $data['account']  = AccountHandler::findAccount($accountId);
            $data['documents'] = DocumentHandler::getDocumentByAccountId($accountId);

            return view('fmbAdmin.viewAccount', $data);
        });



    }


    public function users() {

        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_VIEW_USERS

         ], function () {


            $data = [];
            $data['users'] = \App\User::select('users.*', 'access_levels.name as access_level_name')
                                        ->join('access_levels', 'users.access_level_id', 'access_levels.id')
                                        ->paginate(20);
            return view('fmbAdmin.users', $data );
         });

    }


    public function userModifications() {

        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_VIEW_USERS

         ], function () {

            $data = [];
            $data['userModifications'] = UserModification::select(
                                        'user_modifications.*', 'access_levels.name as access_level_name')
                                        ->where('authorization_status', '=', '0')
                                        ->join('access_levels', 'user_modifications.access_level_id', 'access_levels.id')
                                        ->paginate(20);
            return view('fmbAdmin.userModifications', $data );
         });

    }




    public function createUser() {

        $permissions = [
            AccessLevelHandler::ACCESS_PERMISSION_ID_CREATE_FMB_ADMIN_USER,
            AccessLevelHandler::ACCESS_PERMISSION_ID_CREATE_FMB_STAFF,
        ];

        return AccessLevelHandler::checkAccessExec($permissions, function () {

            $data = [];
            $data['permissions'] =  \App\AccessPermission::all();
            $data['accessLevels'] = AccessLevelHandler::getAccessLevelsByIds([
                AccessLevelHandler::ACCESS_LEVEL_ID_FMB_ADMIN,
                AccessLevelHandler::ACCESS_LEVEL_ID_FMB_STAFF,
            ]);

            return view('fmbAdmin.createUser', $data);
        });

    }

    public function editUser($userId) {

        try {

            $permissions = [];
            $user = User::select()->where('id', '=', $userId)
                                  ->with('permissions')
                                  ->firstOrFail();

            switch ($user->access_level_id) {
                case AccessLevelHandler::ACCESS_LEVEL_ID_FMB_ADMIN:
                     $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_UPDATE_FMB_ADMIN_USER;
                    break;
                case AccessLevelHandler::ACCESS_LEVEL_ID_FMB_STAFF:
                    $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_UPDATE_FMB_STAFF;
                    break;
                default:
                    # code...
                    break;
            }

            return AccessLevelHandler::checkAccessExec($permissions, function () use ($user) {

                $data = [];
                $data['user'] =  $user;
                $data['permissions'] =  \App\AccessPermission::all();
                $data['accessLevels'] = AccessLevelHandler::getAccessLevelsByIds([
                    AccessLevelHandler::ACCESS_LEVEL_ID_FMB_ADMIN,
                    AccessLevelHandler::ACCESS_LEVEL_ID_FMB_STAFF,
                ]);

                return view('fmbAdmin.editUser', $data);
            }); 

        } catch (\Exception $e) {


        }



    }

    public function userActivities() {

        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_AUDIT_TRAIL

         ], function () {

                $data = [
                    "entities" => EntityHandler::getEntities(),
                    "activityTypes" => UserActivityHandler::getActivityTypes(),
                    "activities" => UserActivityHandler::filterActivities(),
                ];

                return view('fmbAdmin.activities', $data);

         });
    }




    public function storeUser() {

        $accessLevelId = Request::input('access_level_id');

        $permissions = [];

        switch ($accessLevelId ) {
            case AccessLevelHandler::ACCESS_LEVEL_ID_FMB_ADMIN:
                $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_CREATE_FMB_ADMIN_USER;
                break;
            case AccessLevelHandler::ACCESS_LEVEL_ID_FMB_STAFF  :
                $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_CREATE_FMB_STAFF;
                break;
            default:
                $feedback = "Please select a access level which you have permissions for.";
                return Redirect::back()->with('feedback-error', $feedback);
                break;
        }

        return AccessLevelHandler::checkAccessExec($permissions, function () use ($accessLevelId) {

                $username = Request::input('username');

                $rules = [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'username' => 'required',
                    'password' => 'required|regex:[^(?=.*\d)(?=.*?[a-zA-Z])(?=.*?[\W_])(.{6,10}$)]',
                ];


                $validator = Validator::make(Request::all(), $rules );

                if ($validator->passes()) { 
                    try {

                        $accessLevelId = Request::input('access_level_id');

                        UserHandler::attemptUserCreate();

                        $feedback = "You have created user account : $username ";
                        $feedback .= "and is waiting for verification.";
                        
                        return \Redirect::to('/admin/users')->with('feedback-success', $feedback);

                    } catch (ExistingUserException $e) {
                        $feedback = "The username  is already taken by another user.";
                        return Redirect::back()->with('feedback-error', $feedback);
                    }
                }

                return Redirect::back()->withErrors($validator);
        });

    }


    public function authorizeUserModification($userModificationId) {

        $permissions = [
            AccessLevelHandler::ACCESS_PERMISSION_ID_AUTHORIZE_USER_MODIFICATION,
        ];

        return AccessLevelHandler::checkAccessExec($permissions, function () 
             use ($userModificationId) { 

            try {
                UserHandler::authorizeAccountModificationById ( $userModificationId );

                $feedback = "You have successfully authorized the update. ";
                return Redirect::back()->with('feedback-success', $feedback);

            }catch(ExistingUserException $e) { 
                $feedback = "The username has already been used. ";
                $feedback .= "It could be that the pending modification has already been verified.";
                return Redirect::back()->with('feedback-error', $feedback);
            }catch(ModelNotFoundException $e) {
                $feedback = "The user or the pending modification was not found. ";
                $feedback .= "Please contact the administrator.";
                return Redirect::back()->with('feedback-error', $feedback);
            }

        });
    }



    public function updateUser($userId) {

        $accessLevelId = Request::input('access_level_id');
        $permissions = [];

        switch ($accessLevelId ) {
            case AccessLevelHandler::ACCESS_LEVEL_ID_FMB_ADMIN:
                $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_UPDATE_FMB_ADMIN_USER;
                break;
            case AccessLevelHandler::ACCESS_LEVEL_ID_FMB_STAFF:
                $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_UPDATE_FMB_STAFF ;
                break;
            default:
                $feedback = "Please select a access level which you have permissions for.";
                return Redirect::back()->with('feedback-error', $feedback);
                break;
        }

        return AccessLevelHandler::checkAccessExec($permissions, function () 
             use ($userId, $accessLevelId) {

                $username = Request::input('username');
                $rules = [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'username' => 'required', 
                ];

                if (\Request::input('password')) {
                    $rules['password'] ='required|regex:[^(?=.*\d)(?=.*?[a-zA-Z])(?=.*?[\W_]).{6,10}$]';
                }

                $validator = Validator::make(Request::all(), $rules );

                if ($validator->passes()) { 
                    try {

                        $accessLevelId = Request::input('access_level_id');

                        UserHandler::attemptUserUpdate($userId);

                         $feedback = "You have successfully updated user account : $username.";
                         return \Redirect::to('/admin/users')->with('feedback-success', $feedback);

                    } catch (ExistingUserException $e) {
                        $feedback = "The username  is already taken by another user.";
                        return Redirect::back()->with('feedback-error', $feedback);
                    }
                }

                return Redirect::back()->withErrors($validator);

        });

    }




    public function viewUserModification($userModificationId) {

        try {

            $userModification = UserModification::where('id', '=', $userModificationId)
                                                ->with('permissions')
                                                ->firstOrFail();


            $precedingUserModification = UserModification::where('id', '<', $userModificationId)
                                                         ->where('user_id', '=', $userModification->user_id ) 
                                                         ->where('user_id', '>', 0)
                                                         ->with('permissions')
                                                         ->orderBy('id', 'DESC')
                                                         ->first();



            $data = [
                    "userModification" => $userModification,
                    "precedingUserModification" => $precedingUserModification,
            ];

            return view('fmbAdmin.viewUserModification', $data);



          



        }catch (ModelNotFoundException $e) {

        }




    }


    public function viewPendingUserModification($userModificationId) {

        try {

            $userModification = UserModification::where('id', '=', $userModificationId)
                                                ->with('permissions')
                                                ->firstOrFail();


            $precedingUserModification = UserModification::where('id', '<', $userModificationId)
                                                         ->where('user_id', '=', $userModification->user_id ) 
                                                         ->where('user_id', '>', 0)
                                                         ->with('permissions')
                                                         ->orderBy('id', 'DESC')
                                                         ->first();

            $data = [
                    "userModification" => $userModification,
                    "precedingUserModification" => $precedingUserModification,
            ];

            return view('fmbAdmin.viewPendingUserModification', $data);



          



        }catch (ModelNotFoundException $e) {

        }




    }




    public function myAccount () {

        $data = [];
        $data['user'] = Auth::user();

        return view('fmbAdmin.editOwnAccount', $data);
    }


    public function updateOwnAccount () {

        $password = \Request::input('password');
        $userId = Auth::user()->id;

        $count =   

        $rules = [];

        if ($password ) {
            $rules['password'] = 'required|regex:[^(?=.*\d)(?=.*?[a-zA-Z])(?=.*?[\W_])(.{6,10}$)]';
        }

        $validator = Validator::make(Request::all(), $rules );

        if ($validator->passes()) {

            try {

                 UserHandler::secureUpdateOwnAccount();
                 $feedback = "You have successfully updated your account.";
                 return Redirect::back()->with('feedback-success', $feedback);
            } catch (ExistingUserException $e) {
                $feedback = "The username  is already taken by another user.";
                return Redirect::back()->with('feedback-error', $feedback);
            }
        }

        return Redirect::back()->withErrors($validator);
    }





}
