<?php

namespace App\Http\Controllers;

use \Request;
use \Hash;
use \Auth;
use \Validator;
use \Redirect;

use \App\Libraries\CustomerHandler;
use \App\Libraries\EntityHandler;
use \App\Libraries\UserActivityHandler;
use \App\Libraries\AccountHandler;
use \App\Libraries\DocumentHandler;
use \App\Libraries\DocumentFileHandler;
use \App\Libraries\UserHandler;
use \App\Libraries\AccessLevelHandler;
use \App\Libraries\DataHandler;


use App\AccountModification;
use App\CustomerModification;
use App\DocumentModification;

use App\User;
use App\UserModification;
use App\DocumentFile;

use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BeehiveAdminController extends Controller
{
    public function dashboard() {

        $data = [];
        $data['totalCustomersCount'] = CustomerHandler::totalCustomersCount();
        $data['totalAccountsCount'] = AccountHandler::totalAccountsCount();
        $data['totalDocumentsCount'] = DocumentHandler::totalDocumentsCount();
        $data['totalDocumentFilesCount'] = DocumentFileHandler::totalDocumentFilesCount();
        $data['recentActivities'] = UserActivityHandler::mostRecentActivities(4);
        $data['recentUsers'] = UserHandler::mostRecentUsers(4);


    	return view('beehiveAdmin.dashboard', $data);
    }    


    public function accounts() {


        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_VIEW_ACCOUNTS

         ], function () {

            $data = [];

            $data['accountTypes'] = AccountHandler::getAccountTypes();
            $data['branches'] = AccountHandler::getBranches();
            $data['accounts'] = AccountHandler::filterAccounts();

            return view('beehiveAdmin.accounts', $data);
         });

    }


    public function viewAccount($accountId) {

        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_VIEW_ACCOUNTS

         ], function ()  use($accountId) {

            $data = [];
            
            $data['account']  = AccountHandler::findAccount($accountId);
            $data['documents'] = DocumentHandler::getDocumentByAccountId($accountId);

            return view('beehiveAdmin.viewAccount', $data);
        });
    }



    public function users() {

        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_VIEW_USERS

         ], function () {


            $data = [];
            $data['users'] = \App\User::select('users.*', 'access_levels.name as access_level_name')
                                        ->join('access_levels', 'users.access_level_id', 'access_levels.id')
                                        ->paginate(20);
            return view('beehiveAdmin.users', $data );
         });

    }



    public function storeUser() {

        $accessLevelId = Request::input('access_level_id');

        $permissions = [];

        switch ($accessLevelId ) {
            case AccessLevelHandler::ACCESS_LEVEL_ID_BEEHIVE_ADMIN:
                $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_CREATE_BEEHIVE_ADMIN_USER;
                break;
            case AccessLevelHandler::ACCESS_LEVEL_ID_BEEHIVE_CLERK:
                $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_CREATE_BEEHIVE_CLERK;
                break;
            default:
                $feedback = "Please select a access level which you have permissions for.";
                return Redirect::back()->with('feedback-error', $feedback);
                break;
        }

        return AccessLevelHandler::checkAccessExec($permissions, function () use ($accessLevelId) {

                $username = Request::input('username');

                $rules = [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'username' => 'required',
                    'password' => 'required|regex:[^(?=.*\d)(?=.*?[a-zA-Z])(?=.*?[\W_])(.{6,10}$)]',
                ];


                $validator = Validator::make(Request::all(), $rules );

                if ($validator->passes()) { 
                    try {

                        $accessLevelId = Request::input('access_level_id');

                        UserHandler::attemptUserCreate();

                        $feedback = "You have created user account : $username ";
                        $feedback .= "and is waiting for verification.";
                        
                        return \Redirect::to('/admin/users')->with('feedback-success', $feedback);

                    } catch (ExistingUserException $e) {
                        $feedback = "The username  is already taken by another user.";
                        return Redirect::back()->with('feedback-error', $feedback);
                    }
                }

                return Redirect::back()->withErrors($validator);
        });

    }




    public function updateUser($userId) {




        $accessLevelId = Request::input('access_level_id');
        $permissions = [];

        switch ($accessLevelId ) {
            case AccessLevelHandler::ACCESS_LEVEL_ID_BEEHIVE_ADMIN:
                $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_UPDATE_BEEHIVE_ADMIN_USER;
                break;
            case AccessLevelHandler::ACCESS_LEVEL_ID_BEEHIVE_CLERK:
                $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_UPDATE_BEEHIVE_CLERK;
                break;
            default:
                $feedback = "Please select a access level which you have permissions for.";
                return Redirect::back()->with('feedback-error', $feedback);
                break;
        }


        return AccessLevelHandler::checkAccessExec($permissions, function () 
             use ($userId, $accessLevelId) {

                $username = Request::input('username');
                $rules = [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'username' => 'required', 
                ];

                if (\Request::input('password')) {
                    $rules['password'] ='required|regex:[^(?=.*\d)(?=.*?[a-zA-Z])(?=.*?[\W_]).{6,10}$]';
                }

                $validator = Validator::make(Request::all(), $rules );

                if ($validator->passes()) { 
                    try {

                        $accessLevelId = Request::input('access_level_id');

                        UserHandler::attemptUserUpdate($userId);

                         $feedback = "You have successfully updated user account : $username.";
                         return \Redirect::to('/beehive-admin/users')->with('feedback-success', $feedback);

                    } catch (ExistingUserException $e) {
                        $feedback = "The username  is already taken by another user.";
                        return Redirect::back()->with('feedback-error', $feedback);
                    }
                }


                return Redirect::back()->withErrors($validator);

        });

    }



    public function createUser() {

        $permissions = [
            AccessLevelHandler::ACCESS_PERMISSION_ID_CREATE_FMB_ADMIN_USER,
            AccessLevelHandler::ACCESS_PERMISSION_ID_CREATE_FMB_STAFF,
        ];

        return AccessLevelHandler::checkAccessExec($permissions, function () {

            $data = [];
            $data['permissions'] =  \App\AccessPermission::all();
            $data['accessLevels'] = AccessLevelHandler::getAccessLevelsByIds([
                AccessLevelHandler::ACCESS_LEVEL_ID_BEEHIVE_ADMIN,
                AccessLevelHandler::ACCESS_LEVEL_ID_BEEHIVE_CLERK,
            ]);

            return view('beehiveAdmin.createUser', $data);
        });

    }

    public function editUser($userId) {

        try {

            $permissions = [];
            $user = User::select()->where('id', '=', $userId)
                                  ->with('permissions')
                                  ->firstOrFail();

            switch ($user->access_level_id) {
                case AccessLevelHandler::ACCESS_LEVEL_ID_BEEHIVE_ADMIN:
                     $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_UPDATE_BEEHIVE_ADMIN_USER;
                    break;
                case AccessLevelHandler::ACCESS_LEVEL_ID_BEEHIVE_CLERK:
                    $permissions[] = AccessLevelHandler::ACCESS_PERMISSION_ID_UPDATE_BEEHIVE_CLERK;
                    break;
                default:
                    # code...
                    break;
            }

            return AccessLevelHandler::checkAccessExec($permissions, function () use ($user) {

                $data = [];
                $data['user'] =  $user;
                $data['permissions'] =  \App\AccessPermission::all();
                $data['accessLevels'] = AccessLevelHandler::getAccessLevelsByIds([
                    AccessLevelHandler::ACCESS_LEVEL_ID_BEEHIVE_ADMIN,
                    AccessLevelHandler::ACCESS_LEVEL_ID_BEEHIVE_CLERK,
                ]);

                return view('beehiveAdmin.editUser', $data);
            }); 

        } catch (\Exception $e) {


            dd($e);

        }



    }


    public function accountsReport() {


        DataHandler::summaryReport();

        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_PULL_REPORTS

             ], function () {

            $data = [];

            $data['accountTypes'] = AccountHandler::getAccountTypes();
            $data['branches'] = AccountHandler::getBranches();
            $data['accounts'] = AccountHandler::filterAccounts();

            return view('beehiveAdmin.accountsReportFilter', $data);
        });
    }


    public function userActivities() {

        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_AUDIT_TRAIL

         ], function () {

                $data = [
                    "entities" => EntityHandler::getEntities(),
                    "activityTypes" => UserActivityHandler::getActivityTypes(),
                    "activities" => UserActivityHandler::filterActivities(),
                ];

                return view('beehiveAdmin.activities', $data);

         });
    }






    public function downloadAccountsReport() {


        return AccessLevelHandler::checkAccessExec([
                AccessLevelHandler::ACCESS_PERMISSION_ID_PULL_REPORTS

             ], function () {

            $account = AccountHandler::filterReportAccounts();

            \Excel::create('FMB-accounts', function($excel) use ($account)  {

                $excel->sheet('Excel sheet', function($sheet) use ($account)  {

                    $sheet->setOrientation('landscape');
                    $sheet->fromModel( $account );

                });

            })->download('xls');

        });


    }









    public function pendingAccounts() {
    	return view('beehiveAdmin.pendingAccounts');
    }


    public function ajaxPendingAccounts() {

    	$serialNumber = \Request::input('serial_number');
    	$boxNumber = \Request::input('box_number');


    	$rawQuery =  "(SELECT name FROM account_types WHERE id=account_modifications.account_type_id  ";
            $rawQuery .= "  LIMIT 1) as account_type";

        $rawQuery2 = "(SELECT name FROM branches WHERE id=account_modifications.branch_id) as branch";

    	$data = AccountModification::select(
    					'archiving_boxes.*',
			    		'customer_modifications.*', 
			    		'account_modifications.*',
			    		DB::raw($rawQuery),
			    		DB::raw($rawQuery2)
			    	)->join('customer_modifications','account_modifications.customer_modification_id', 
			    			'=', 'customer_modifications.id'
			    	)->join('archiving_boxes','account_modifications.archiving_box_id', '=', 
				    	'archiving_boxes.id')
			    	->where('account_modifications.authorization_status', '=', 0);

		if ($serialNumber) {
			$data = $data->where('archiving_boxes.serial_number', '=', $serialNumber);
		}


		if ($boxNumber) {
			$data = $data->where('archiving_boxes.box_number', '=', $boxNumber);
		}

		$data = $data->paginate(10);
  
        return response()->json( $data  );
	

    }


    public function ajaxPendingAccount($accountModificationId) {

    	try {

	    	$rawQuery =  "(SELECT name FROM account_types WHERE id=account_modifications.account_type_id  ";
	            $rawQuery .= "  LIMIT 1) as account_type";

	        $rawQuery2 = "(SELECT name FROM branches WHERE id=account_modifications.branch_id) as branch";

	    	$data = AccountModification::select(
	    					'archiving_boxes.*',
				    		'customer_modifications.*', 
				    		'account_modifications.*',
				    		DB::raw($rawQuery),
				    		DB::raw($rawQuery2)
				    	)->join('customer_modifications','account_modifications.customer_modification_id', '=', 'customer_modifications.id'
				    	)->join('archiving_boxes','account_modifications.archiving_box_id', '=', 
				    	'archiving_boxes.id')
	    				->where('account_modifications.id','=', $accountModificationId)
	    				->firstOrFail();

			$data->toArray();

			$documentModifications = DocumentModification::select()
														->where('account_modification_id', '=',$accountModificationId)
														->with('documentFileModifications')->get();

			$data['documents'] = $documentModifications->toArray();
	  
	        return response()->json( $data  );
	


    	} catch (ModelNotFoundException $e) {

    	}

    }

    public function ajaxAuthorizePendingAccount($accountModificationId) { 

        $accountModification =  AccountModification::select()
                                            ->where('id', '=', $accountModificationId)
                                            ->where('authorization_status', '=', 0)
                                            ->firstOrFail();

        $customerModification = CustomerModification::select()
                                            ->where('id','=', $accountModification->customer_modification_id)
                                            ->first();

        if ($customerModification) {

            if ($customerModification->authorization_status == 0) {

                $customer =  CustomerHandler::createCustomerFromCustomerModification($customerModification);
                $account = AccountHandler::createAccountFromAccountModification($accountModification, $customer);

                DocumentHandler::createDocumentsFromAccountModification($accountModification, $account);
                // DocumentFileHandler::createDocumentFilesFromAccountModification($accountModification);

            } else {


            }

        }


    }



    public function ajaxUnauthorizePendingAccount($accountModificationId) { 

    	$accountModification =  AccountModification::select()
    										->where('id', '=', $accountModificationId)
    										->where('authorization_status', '=', 0)
    										->firstOrFail();

    	$customerModification = CustomerModification::select()
    										->where('id','=', $accountModification->customer_modification_id)
    										->first();

    	if ($customerModification) {

            $verifierComment = Request::input('verifier_comment');
            $verifierId = Auth::user()->id;

            $accountModification->authorization_status = 2;
            $accountModification->verifier_id = $verifierId;
            $accountModification->verifier_comment = $verifierComment;
            $accountModification->save();

    		if ($customerModification->authorization_status == 0) {
                
                $customerModification->verifier_id = $verifierId;
                $customerModification->authorization_status = 2;
                $customerModification->verifier_comment = $verifierComment;
                $customerModification->save();
    		} else {


    		}

    	}


    }









}
