<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\AccountHandler;
use App\Libraries\UserHandler;

class ClerkController extends Controller
{
    public function dashboard() {

        $userId = \Auth::user()->id;

        $data = [];

        $data['weeklyPerformanceSummary'] = UserHandler::weeklyUserPerformanceSummary($userId);

        $data['pendingEntriesToday'] = UserHandler::userPendingEntriesToday($userId);
        $data['authorizedEntriesToday'] = UserHandler::userAuthorizedEntriesToday($userId);
        $data['unauthorizedEntriesToday'] = UserHandler::userUnauthorizedEntriesToday($userId);
       
        $data['pendingEntriesThisMonth'] = UserHandler::userPendingEntriesThisMonth($userId);
        $data['authorizedEntriesThisMonth'] = UserHandler::userAuthorizedEntriesThisMonth($userId);
        $data['unauthorizedEntriesThisMonth'] = UserHandler::userUnauthorizedEntriesThisMonth($userId);
        
    	return  view('clerk.dashboard', $data);
    }  

    public function entries() {
        $data = [];

        $data['accountTypes'] = AccountHandler::getAccountTypes();
        $data['branches'] = AccountHandler::getBranches();
        $data['accountModifications'] = AccountHandler::filterOwnAccountModifications();

    	return  view('clerk.entries', $data);
    }

    public function newEntry($customerTypeId) {

        $userId = \Auth::user()->id;

    	$data = [];
    	$data['accountTypes'] = AccountHandler::getAccountTypes();
    	$data['branches'] = AccountHandler::getBranches();
        $data['mostRecentEntries'] = UserHandler::mostRecentUserInitiatedAccountModifications($userId, 5);
    	$data['currentUserBox'] = UserHandler::currentUserArchivingBox($userId);


        switch ($customerTypeId) {
            case 1:
                return  view('clerk.createPersonalCustomerAccount', $data);
                break;
            case 2:
                return  view('clerk.createGroupCustomerAccount', $data);
            case 3:
                return  view('clerk.createBusinessCustomerAccount', $data);
            
            default:
                # code...
                break;
        }




    	
    }
}
