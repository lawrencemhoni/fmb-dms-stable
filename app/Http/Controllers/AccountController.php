<?php

namespace App\Http\Controllers;

use \Request;
use \Validator;
use Redirect;

use App\Libraries\CustomerHandler;
use App\Libraries\AccountHandler;
use App\Libraries\DocumentHandler;
use App\Libraries\DocumentFileHandler;


use App\Exceptions\NoUserArchivingBoxException;
use App\Exceptions\PendingAccountVerificationException;


class AccountController extends Controller
{
    #Rules required to validate a personal account
    private $personalCustomerTypeRules = [
            "first_name" => "required",
            "last_name" => "required",
            "date_of_birth" => "required",
            "account_number" => "required|min:6",
            "branch_id" => "required",
            "files" => "required",
    ];

    #Rules required to validate a group account
    private $groupCustomerTypeRules = [
            "group_name" => "required",
            "account_number" => "required|min:6",
            "branch_id" => "required",
            "files" => "required",
    ];

    #rules required to validate a business account
    private $businessCustomerTypeRules = [
            "business_name" => "required",
            "account_number" => "required|min:6",
            "branch_id" => "required",
            "files" => "required",
    ];


    /**
    *Store Account
    *Save a new account into the database 
    */
    public function store(\Illuminate\Http\Request $request) {

        $customerTypeId = Request::input('customer_type_id');
        $validatorPasses = false;

        switch ($customerTypeId) {

            case 1:
                    
                $validator = Validator::make(Request::all(), $this->personalCustomerTypeRules);
                $validatorPasses = $validator->passes();
                break;

            case 2:
                    
                $validator = Validator::make(Request::all(), $this->groupCustomerTypeRules);
                $validatorPasses = $validator->passes();
                break;

            case 3:
    
                $validator = Validator::make(Request::all(), $this->businessCustomerTypeRules);
                $validatorPasses = $validator->passes();
                break;
        }


        if ($validatorPasses) {

            try {

                if (CustomerHandler::checkExistingCustomerModification(
                    \Request::input('customer_number'))) {
                    $feedback = "That customer already exists.";
                    return Redirect::back()->with('feedback-error', $feedback);
                }

                if (AccountHandler::checkExistingAccountModification(
                    \Request::input('account_number'))){
                    $feedback = "That customer already exists.";
                    return Redirect::back()->with('feedback-error', $feedback);
                }

                $files = \Request::has('files')?  
                    \Request::input('files') : [];

                $customerModification =  CustomerHandler::createCustomerModification(0);
                $accountModification  =  AccountHandler::createAccountModification($customerModification, 0);
                
                $documentModification =  DocumentHandler::createDocumentModification($accountModification, 0);

                DocumentFileHandler::createAccountModificationFiles($documentModification, $files, false);
                DocumentFileHandler::moveFilesToModificationsDir($files);
      

                return \Redirect::back();


            } catch (NoUserArchivingBoxException $e) {

                $feedback = "You did not specify the archiving box you are working on.";
                return Redirect::back()->with('feedback-error', $feedback);
            } catch (PendingAccountVerificationException $e) {

                $feedback = "The account you entered is already has a pending verification.";
                return Redirect::back()->with('feedback-error', $feedback); 
            }

        }

    }









}
