<?php

namespace App\Http\Controllers;

use \Request;
use \Hash;
use \QueryException;
use \Auth;
use  \Redirect;

use \App\User;
use \App\AccessPermission;
use App\Libraries\UserSessionHandler;

class UserController extends Controller
{
    public function login () {
    	return  view('login');
    }

    public function register () {
    	return  view('register');
    }


    /**
    *Attempt to authenticate the user and redirect to the right resource when successful.
    *@return view of login resource
    */
    public function auth(\Illuminate\Http\Request $request) {
        $username = $request->input('username');
        $password = $request->input('password');

        if(Auth::attempt(['username' => $username, 'password' => $password ])) {
            $user = Auth::user();

            if( $user->active != 1) {
                \Auth::logout();

                $errorFeedback = "Your account is not active.";
                return Redirect::back()->with('feedback-error', $errorFeedback);
            }

            UserSessionHandler::registerSession(Auth::user(), $request);

        }

        $errorFeedback = "Incorrect username or password.";
        return Redirect::back()->with('feedback-error', $errorFeedback);
    }

    #logout
    public function logout() {
        \Auth::logout();
        return \Redirect::to('/login');
    }


    public function ajaxAccessPermissions () {

        $accessPermissions =  AccessPermission::select()->get();

        return  response()->json(  $accessPermissions->toArray() );

    }




        














}
