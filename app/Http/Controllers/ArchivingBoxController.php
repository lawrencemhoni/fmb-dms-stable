<?php

namespace App\Http\Controllers;

use Request;
use App\ArchivingBox;
use Validator;
use Redirect;

use App\Libraries\ArchivingBoxHandler;
use App\Exceptions\ExistingArchivingBoxException;

class ArchivingBoxController extends Controller
{


    #rules required to validate a business account
    private $rules = [
            "serial_number" => "required",
            "box_number" => "required",
            "location" => "required",
    ];


    public function store() {

    	$validator = Validator::make(Request::all(), $this->rules);

    	if ($validator->passes()) {

            try {

                ArchivingBoxHandler::createUserArchvingBox();
                return  \Redirect::back();
            } catch (ExistingArchivingBoxException $e) {
                $feedback = "The archiving box already exists.";
                return Redirect::back()->with('feedback-error', $feedback);
            }

    	}

        return Redirect::back()->withErrors($validator);

        
    }
}
