<?php namespace App\Libraries;

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\EntityHandler;
use App\Libraries\AccountHandler;
use App\Libraries\DocumentFileHandler;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class FileController extends Controller
{
    public function download($filename) {

        try {

        	$file = DocumentFileHandler::getFilePath($filename);



            // UserActivityHandler::pushActivity( 
            //     EntityHandler::ENTITY_ID_DOCUMENT_FILE,
            //      $user->id,
            //      UserActivityHandler::ACTIVITY_TYPE_ID_UPDATE,
            //      "name",
            //      $filename);
            

       		$headers = array( 'Content-Type: application/pdf');
       		return \Response::download($file, "$filename", $headers);

        } catch (FileNotFoundException $e) {
        	return \Response::make("The file {$filename} for does not exist.");
        }







    }    


    public function view($filename) {

        // try {



        	$file = DocumentFileHandler::getFilePath($filename);
        	
		    return response()->file($file);

        // } catch (FileNotFoundException $e) {
        // 	return \Response::make("The file {$filename} for does not exist.");
        // }

    }




    public function pendingAccountFileView($filename) {

        $file = DocumentFileHandler::getAccountModificationFilePath($filename);
        
        return response()->file($file);

    }

    public function storeTmpAccountFile() {

        try{

            return DocumentFileHandler::createTmpAccountFile();

        } catch (\Exception $e) {
           echo "problem";
        }

       
    }

    public function deleteTmpAccountFile() {

        if (AccountHandler::deleteTmpAccountFile(\Request::input('filename'))) {
            return "deleted";
        }

        return "not deleted";
    }

















}
