<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Redirect;
use App\Libraries\UserHandler;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
         
            switch (Auth::user()->access_level_id) {
                case UserHandler::ACCESS_LEVEL_ID_BEEHIVE_CLERK:
                    return Redirect::to('/clerks/dashboard');
                    break;
                case  UserHandler::ACCESS_LEVEL_ID_BEEHIVE_ADMIN:
                    return Redirect::to('/beehive-admin/dashboard');
                    break; 
                case  UserHandler::ACCESS_LEVEL_ID_FMB_ADMIN:
                    return Redirect::to('/admin/dashboard');
                    break; 
                case  UserHandler::ACCESS_LEVEL_ID_FMB_STAFF:
                    return Redirect::to('/user/dashboard');
                    break; 
                
                default:
                    break;
            }
        }

        return $next($request);
    }
}
