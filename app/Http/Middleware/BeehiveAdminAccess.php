<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Libraries\UserHandler;

class BeehiveAdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            
            if(Auth::user()->access_level_id == UserHandler::ACCESS_LEVEL_ID_BEEHIVE_ADMIN) {
                return $next($request);
            }

        }

       return redirect('/login'); 
    }
}
