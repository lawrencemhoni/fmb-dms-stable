<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Libraries\UserHandler;
use App\Libraries\LinksHelper;
use App\UserModification;
use DateTime;
use Date;
use Redirect;

class FmbAdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            $user = Auth::user();
            
            if($user->access_level_id == UserHandler::ACCESS_LEVEL_ID_FMB_ADMIN) {

               $UserModification = UserModification::where('username', '=', $user->username)
                                                    ->where('security_modification', '=', 1)
                                                    ->orderBy('id', 'DESC')
                                                    ->take(1)
                                                    ->first();
                if($UserModification) {

                    $fromDate = Date('Y-m-d', strtotime($UserModification->created_at));
                    $now = Date('Y-m-d');

                    $datetime1 = new DateTime($fromDate);
                    $datetime2 = new DateTime($now);
                    $interval = $datetime1->diff($datetime2);

                    $days = (int) $interval->format('%a');

                    if ($days >= 30) {

                        $feedback = "Security upadte required! You need to update your password.";
                        return Redirect::to(LinksHelper::URI_EDIT_OWN_ACCOUNT)
                                        ->with('feedback-error', $feedback);
                    }

                }



                return $next($request);
            }

        }

       return redirect('/login'); 
    }
}
