<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class DocumentModification extends Model {

	protected $table = 'document_modifications';

	public function documentFileModifications() {
		return $this->hasMany(\App\DocumentFileModification::class,  "document_modification_id");
	}







}
