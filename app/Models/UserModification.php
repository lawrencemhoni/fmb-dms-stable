<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use User;
use DB;

class UserModification extends Model 
{
    protected $table = 'user_modifications';

    public function permissions () {

        return  $this->belongsToMany(AccessPermission::class, 
                        "user_modification_access_permission", "user_modification_id", "access_permission_id");
    }
}
