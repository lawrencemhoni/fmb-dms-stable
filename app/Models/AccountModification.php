<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class AccountModification extends Model {

	protected $table = 'account_modifications';


	public function users() {
		return $this->belongsToMany(\App\User::class, 
							"user_account", "account_id", "user_id");
	}

	public function documentModificationFiles() {
		return $this->hasMany(\App\AccountModificationFile::class,  "account_modification_id");
	}

	public function documents() {
		return $this->hasMany(\App\AccountModificationDocument::class,  "account_modification_id");
	}







}
