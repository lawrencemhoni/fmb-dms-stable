<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use User;
use DB;
use App\AccessPermission;


class AccessLevel extends Model {

	protected $table = 'access_levels';

	public function permissions() {

		return $this->belongsToMany(AccessPermission::class, 
						'access_level_access_permission', 
						'access_level_id',
						'access_permission_id');
	}

}
