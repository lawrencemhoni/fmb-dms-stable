<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use User;
use DB;


class Category extends Model {

	protected $table = 'categories';


	public function branches() {
		return $this->belongsToMany(Branch::class, 
						"branch_category", "category_id", "branch_id");
	}


	
}
