<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class CustomerModification extends Model {

	protected $table = 'customer_modifications';

	public function documents() {
		return $this->hasMany(\App\AccountModificationDocument::class,  "customer_modification_id");
	}







}
