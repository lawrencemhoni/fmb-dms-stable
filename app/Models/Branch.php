<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use  App\Models\Category;

class Branch extends Model {
	
	protected $table = 'branches';


	public function categories() {
		return $this->belongsToMany(Category::class, 
									'branch_category', 'branch_id', 'category_id');
	}


}
