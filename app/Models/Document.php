<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Document extends Model {

	protected $table = 'documents';


	public function files() {
		return $this->hasMany(\App\DocumentFile::class,  "document_id");
	}

}
