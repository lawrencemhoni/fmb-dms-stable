<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Account extends Model {

	protected $table = 'accounts';


	public function users() {
		return $this->belongsToMany(\App\User::class, 
							"user_account", "account_id", "user_id");
	}


	public function documents() {
		return $this->hasMany(\App\Document::class,  "account_id");
	}

	// public function files() {
	// 	return $this->hasMany(\App\DocumentFile::class,  "account_id");
	// }

	public function categories() {

		return $this->belongsToMany(\App\Category::class, "account_category", "account_id", "category_id");;
	}








}
