<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class UserActivity extends Model {
	
	protected $table = 'user_activities';
	protected $fillable = [ 
		'user_id', 
		'user_session_id', 
		'entity_id', 
		'entity_primary_value', 
		'activity_type_id', 
		'entity_reference_field', 
		'entity_reference_value' ];
}
