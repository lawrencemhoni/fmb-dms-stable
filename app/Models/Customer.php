<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Customer extends Model {

	protected $table = 'customers';

	public function accounts() {
		return $this->hasMany(\App\Account::class,  "customer_id");
	}

	public function documents() {
		return $this->hasMany(\App\AccountDocument::class,  "customer_id");
	}


	public function files() {
		return $this->hasMany(\App\AccountFile::class,  "customer_id");
	}









}
