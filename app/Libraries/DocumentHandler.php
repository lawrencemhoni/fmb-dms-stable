<?php namespace App\Libraries;


use App\Account;
use App\Document;
use App\DocumentFile;
use App\AccountType;

use App\CustomerModification;
use App\AccountModification;
use App\DocumentModification;
use App\DocumentFileModification;
use App\User;
use App\Branch;

use Illuminate\Contracts\Filesystem\FileNotFoundException;

class DocumentHandler {

    const CACHE_KEY_TOTAL_DOCUMENTS_COUNT = "total_documents_count";

	public static function createDocumentModification (
		\App\AccountModification $accountModification, 
                $authorizationStatus = 0,
		$documentName = 'CIF files') {

                $documentModification = new DocumentModification;
                $documentModification->name = $documentName;
                $documentModification->account_modification_id = $accountModification->id;
                $documentModification->user_id =  \Auth::user()->id;
                $documentModification->authorization_status =  $authorizationStatus;
                $documentModification->save();

                return  $documentModification;
	}


        public static function createDocumentFromDocumentModification( 
                \App\DocumentModification $documentModification,
                \App\Account $account  ) {

                $document = new Document;
                $document->document_modification_id =  $documentModification->id;
                $document->name =  $documentModification->name;
                $document->description =  $documentModification->description;
                $document->account_id = $account->id;

                $document->save();

                return $document;
        }


        public static function createDocumentsFromAccountModification(
                \App\AccountModification $accountModification,
                \App\Account $account ) {

                $modificationDestinationPath = DocumentFileHandler::getModificationDir();
                $finalDestinationPath = DocumentFileHandler::getLastDir();

                $documentModifications = DocumentModification::select('document_modifications.*')
                                            ->where('account_modification_id', 
                                                    '=', $accountModification->id )
                                            ->with('documentFileModifications')
                                            ->get();

                foreach($documentModifications as $documentModification ) {

                        $document = new Document;
                        $document->document_modification_id = $documentModification->id;
                        $document->name = $documentModification->name;
                        $document->description = $documentModification->description;
                        $document->account_id = $account->id;
                        $document->save();

                        $files = $documentModification->documentFileModifications;

                        foreach($files as $file) {

                                $documentFile = new DocumentFile;
                                $documentFile->title = $file->title;
                                $documentFile->name = $file->name;
                                $documentFile->description = $file->description;
                                $documentFile->document_id = $document->id;
                                $documentFile->document_file_type_id = $file->document_file_type_id;
                                $documentFile->document_file_modification_id = $file->id;
                                $documentFile->save();

                                $modificationFile =  $modificationDestinationPath . '/' .$file->name;

                                if (file_exists($modificationFile)) {
                                        $newFile = $finalDestinationPath . '/' .$file->name;
                                        rename($modificationFile, $newFile);
                                } 
                        }


                }

        }


        public static function getDocumentByAccountId($accountId) {

            return Document::select()
                            ->where('account_id', '=', $accountId)
                            ->with('files')
                            ->get();
        }



        public static function totalDocumentsCount() {    

            if (\Cache::has(self::CACHE_KEY_TOTAL_DOCUMENTS_COUNT)) {
                return \Cache::get(self::CACHE_KEY_TOTAL_DOCUMENTS_COUNT);
            }
     
            $documents = Document::select(
                \DB::raw('(SELECT count(*) FROM documents 
                            inner join accounts on documents.account_id = accounts.id) as total')
                          )->first();

            $total = ( $documents  )?  $documents ->total : 0;

            \Cache::put(self::CACHE_KEY_TOTAL_DOCUMENTS_COUNT,  $total, 60);

            return $total;
        }








}


?>