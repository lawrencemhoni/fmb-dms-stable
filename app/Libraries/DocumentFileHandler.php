<?php namespace App\Libraries;

use App\Account;
use App\Document;
use App\DocumentFile;
use App\AccountType;

use App\CustomerModification;
use App\AccountModification;
use App\DocumentModification;
use App\DocumentFileModification;
use App\User;
use App\Branch;

use DB;
use Request;


use Illuminate\Contracts\Filesystem\FileNotFoundException;

class DocumentFileHandler {

    const DOCUMENT_FILE_TYPE_ID_ACCOUNT_FORM = 1; 
    const DOCUMENT_FILE_TYPE_ID_ATTACHMENT = 2; 
    const DOCUMENT_FILE_TYPE_ID_SIGNATURE= 3; 

	const ACCOUNT_FILES = "public/account_files";
	const ACCOUNT_FILES_2 = "public/account_files_2";
	const MODIFICATION_FILES_DIR = "public/account_files_2/modification_files";

	const CACHE_KEY_TOTAL_DOCUMENT_FILES_COUNT = "document_files_total_count";

	public static $fileDirs = [
		"public/account_files",
		"public/account_files_2"
	];


    public static function generateCustomerTypeAccountTypeFilesCountCacheKey(
        $customerTypeId, $accountTypeId ) {

        $cacheKey  = "customer-type-{$customerTypeId}-";
        $cacheKey .= "account-type-{$accountTypeId}-files-total-count";

        return $cacheKey;
    }


	public static function createTmpAccountFile() {


            // ini_set('upload_max_filesize', '30M');
            // ini_set('max_execution_time', '99');
        

		if(\Request::hasFile('attachment')) {

			$file = \Request::file('attachment');
			$tmpPath = storage_path('tmp_account_files');
			$destinationPath = DocumentFileHandler::getLastDir();

			$ext  =  $file->getClientOriginalExtension();
			$accountNumber = \Request::input('account_number');
			$fileName =  DocumentFileHandler::generateDocumentFileName($file, $accountNumber, 1);			
			$file->move($tmpPath, $fileName);

			return  $fileName;
		}

		return 'no attachment';
	}


	public static function getLastDir() {
		return storage_path(last(self::$fileDirs));
	}


	public static function getModificationDir() {
		return storage_path(self::MODIFICATION_FILES_DIR);
	}

	public static function getAccountModificationFilePath($filename) {
		$path = storage_path(self::MODIFICATION_FILES_DIR) . '/' . $filename;

		if (file_exists($path)) {
			return $path;
		}

		throw new FileNotFoundException("Account file not found.");

	}


	public static function getFilePath($filename) {

		foreach (self::$fileDirs as $dir) {

			$path = storage_path($dir) . '/' . $filename;

			if (file_exists($path)) {
				return $path;
			}

		}
		throw new FileNotFoundException("Account file not found.");
	} 


	public static function fileExists($filename) {
		foreach (self::$fileDirs as $dir) {
			$path = storage_path($dir) . '/' . $filename;
			if (file_exists($path)) {
				return true;
			}
		}

		return false;
	}


	/**
	*generate a unique account file name
	*@return a unique file name.
	*/
    public static function generateDocumentFileName($fileUpload, $accountNumber, $i) {

    	$ext  =  $fileUpload->getClientOriginalExtension();
    	
    	$fileName  = ($i == 1 )? "{$accountNumber}.{$ext}" : "{$accountNumber}-{$i}.{$ext}";

    	$tmpPath = storage_path('tmp_account_files') . '/' . $fileName;

    	if(self::fileExists($fileName) || file_exists($tmpPath) ) {
    		$i++;
    		return self::generateDocumentFileName($fileUpload, $accountNumber, $i);
    	}

    	return $fileName;
    }


    private static function prepareFileName($filename) {
    	return  str_replace("\r\n", "", $filename);
    }


    public static function createDocumentFilesFromAccountModification(
    	\App\AccountModification $accountModification ) {


    		$modificationDestinationPath = self::getModificationDir();
        	$finalDestinationPath = self::getLastDir();

            $documentModifications = DocumentModification::select()
                                            ->where('account_modification_id', 
                                                    '=', $accountModification->id )
                                            ->get();

            foreach($documentModifications as $documentModification ) {
                    $document = new Document;
                    $document->document_modification_id = $documentModification->id;
                    $document->name = $documentModification->name;
                    $document->description = $documentModification->description;
                    $document->account_id = $account->id;
                    $document->save();

                    $files = $documentModification->files();

                    foreach($documentFileModification as $file) {

			    		$documentFile = new DocumentFile;
			    		$documentFile->title = $file->title;
                        $documentFile->document_file_type_id = $file->document_file_type_id;
			    		$documentFile->name = $file->name;
			    		$documentFile->description = $file->description;
			    		$documentFile->document_id = $document->id;
			    		$documentFile->save();

						$modificationFile =  $modificationDestinationPath . '/' .$file->name;

				        if (file_exists($modificationFile)) {
				        	$newFile = $finalDestinationPath . '/' .$file->name;
				            rename($modificationFile, $newFile);
				        } 
			    	}

            }

    }


 	public static function createAccountModificationFiles (
		\App\DocumentModification $accountModificationDocument,  
		$files, $authorizationStatus = 0) {

 		foreach ($files as $type => $arr) {

 			foreach ($arr as $file) {

	 			$file = self::prepareFileName($file);

				$modificationAccountFile  =  new DocumentFileModification;

	 			switch ($type) {
	 				case 'signature_files':
			            
                        $modificationAccountFile->title = "Customer Signature File";
			            $modificationAccountFile->document_file_type_id = self::DOCUMENT_FILE_TYPE_ID_SIGNATURE;
	 					break;

	 				case 'account_forms':
			            $modificationAccountFile->title = "Account Forms File";
                        $modificationAccountFile->document_file_type_id = self::DOCUMENT_FILE_TYPE_ID_ACCOUNT_FORM;
	 				break;

	 				case 'customer_attachments':
	 					$modificationAccountFile->title = "Customer Attachments File";
                        $modificationAccountFile->document_file_type_id = self::DOCUMENT_FILE_TYPE_ID_ATTACHMENT;
	 				break;
	 				
	 				default:
	 					# code...
	 					break;
	 			}

	 			$modificationAccountFile->name = $file;
	            $modificationAccountFile->document_modification_id = $accountModificationDocument->id;
	            $modificationAccountFile->authorization_status =  $authorizationStatus;
	            $modificationAccountFile->save(); 				
 			}

 		}
	}


	public static function  moveFilesToModificationsDir ($files) {

        $modificationDestinationPath = self::getModificationDir();
        $finalDestinationPath = self::getLastDir();
        $tmpPath = storage_path('tmp_account_files');

 		foreach ($files as $type => $arr) {

 			foreach ($arr as $file) {

 				$tmpFile = $tmpPath . '/' .$file;

		        if (file_exists($tmpFile)) {
		        	$newFile = $modificationDestinationPath . '/' .$file;
		            rename($tmpFile, $newFile);
		        }
				
 			}

 		}

	}





    public static function totalDocumentFilesCount() {    

        if (\Cache::has(self::CACHE_KEY_TOTAL_DOCUMENT_FILES_COUNT)) {
            return \Cache::get(self::CACHE_KEY_TOTAL_DOCUMENT_FILES_COUNT);
        }
 
        $documentFiles = DocumentFile::select(
            \DB::raw('(SELECT count(*) FROM document_files  inner join documents
                        inner join accounts on document_files.document_id = documents.id
                        where documents.account_id = accounts.id  ) as total')
                      )->first();

        $total = ( $documentFiles  )?  $documentFiles->total : 0;

        \Cache::put(self::CACHE_KEY_TOTAL_DOCUMENT_FILES_COUNT,  $total, 60);

        return $total;
    }



    public static function cusTypeAccTypeFilesTotalCount($customerTypeId, $accountTypeId) {

        $cacheKey = self::generateCustomerTypeAccountTypeFilesCountCacheKey(
            $customerTypeId, $accountTypeId);

        if (\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }

        $rawQuery = "(SELECT count(*) FROM document_files 
        inner join documents  inner join accounts inner join customers 
        WHERE document_files.document_id = documents.id
        AND documents.account_id = accounts.id
        AND accounts.customer_id = customers.id 
        AND accounts.account_type_id={$accountTypeId} 
        AND customers.customer_type_id = {$customerTypeId} 
    	) as total ";

        $account = DocumentFile::select(
                    DB::raw($rawQuery))
                    ->take(1)
                    ->first();

        $total = ( $account )?  $account->total : 0;

         \Cache::put($cacheKey,  $total, 60);

        return $total;

    }










}


?>