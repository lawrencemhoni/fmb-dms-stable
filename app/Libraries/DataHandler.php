<?php namespace App\Libraries;

use App\Customer;
use App\Account;
use App\AccountDocument;
use App\AccountFile;
use App\AccountType;
use App\Entity;

use App\AccountModification;
use App\AccountDocumentModification;
use App\DocumentFileModification;
use App\User;
use App\Branch;
use Request;
use Auth;
use DB;


use App\Jobs\ProcessPersonalAccount;
use App\Jobs\ProcessGroupAccount;
use App\Jobs\ProcessBusinessAccount;

use App\Libraries\AccountFileHandler;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Libraries\UserActivityHandler;
use App\Libraries\EntityHandler;


class DataHandler {

    public static function correctNameCasing($name) {
    	return ucfirst($name);
    }


    public static  function summaryReport () { 

    	$rawQuery1 = "(SELECT count(*) from accounts) as total_accounts";
    	$rawQuery2 = "(SELECT count(*) from customers) as total_customers";
    	$rawQuery3 = "(SELECT count(*) from documents) as total_documents";
    	$rawQuery4 = "(SELECT count(*) from document_files) as total_files";

    	$record = Entity::select(
    		DB::raw($rawQuery1),
    		DB::raw($rawQuery2),
    		DB::raw($rawQuery3),
    		DB::raw($rawQuery4)
    	)->first();

    	return $record;
    }

}










?>
