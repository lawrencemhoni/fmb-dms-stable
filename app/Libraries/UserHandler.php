<?php namespace App\Libraries;

use Request;
use Hash;
use Auth;

use App\Account;
use App\Document;
use App\DocumentFile;
use App\AccountType;

use App\CustomerModification;
use App\AccountModification;
use App\DocumentModification;
use App\DocumentFileModification;

use App\ArchivingBox;
use App\User;
use App\UserModification;
use App\Branch;
use App\AccessLevel;

use App\Exceptions\ExistingUserException;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserHandler{


     const ACCESS_LEVEL_ID_SUPER_USER = 1;
     const ACCESS_LEVEL_ID_FMB_ADMIN = 2;
     const ACCESS_LEVEL_ID_FMB_STAFF = 3;
     const ACCESS_LEVEL_ID_FMB_TECH = 4;
     const ACCESS_LEVEL_ID_BEEHIVE_ADMIN = 5;
     const ACCESS_LEVEL_ID_BEEHIVE_TEAM_LEADER = 6;
     const ACCESS_LEVEL_ID_BEEHIVE_CLERK = 7;

     const CACHE_KEY_MOST_RECENT_USERS = "most_recent_users";

     public static function generateMostRecentUsersCacheKey($limit){
            return self::CACHE_KEY_MOST_RECENT_USERS .'_'.$limit;
     }

	 public static function mostRecentUserInitiatedAccountModifications($userId, $limit=5) {

        return AccountModification::select('account_modifications.*', 'customer_modifications.*')

                    ->join('customer_modifications', 
                    	   'account_modifications.customer_modification_id', 
                    	   'customer_modifications.id')
                    ->where('account_modifications.initiator_id', '=', $userId )
                    ->orderBy('account_modifications.id', 'DESC')
                    ->take($limit)
                    ->get();




    }

    public static function userPendingEntriesToday($userId) {

        $cacheKey = "user-{$userId}-total-pending-entries-today";
        $today = date('Y-m-d');

        if (\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }

        $rawQuery  = "(SELECT count(*) FROM account_modifications ";
        $rawQuery .= " WHERE account_modifications.authorization_status = 0 ";
        $rawQuery .= " AND initiator_id = {$userId}  AND created_at > '{$today} 00:00:00' ";
        $rawQuery .= " AND created_at < '{$today} 23:59:59' ) as total";

        $total = User::select(
        \DB::raw($rawQuery)
        )->first()->total;

        \Cache::put($cacheKey, $total, 1);

        return $total;
    }



    public static function userAuthorizedEntriesToday($userId) {

        $cacheKey = "user-{$userId}-total-authorized-entries-today";
        
        $today = date('Y-m-d');

        if (\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }

        $rawQuery  = "(SELECT count(*) FROM account_modifications ";
        $rawQuery .= " WHERE account_modifications.authorization_status = 1 ";
        $rawQuery .= " AND initiator_id = {$userId}  AND created_at > '{$today} 00:00:00' ";
        $rawQuery .= " AND created_at < '{$today} 23:59:59' ) as total";

        $total = User::select(
        \DB::raw($rawQuery)
        )->first()->total;

        \Cache::put($cacheKey, $total, 1);

        return $total;
    }




    public static function userUnauthorizedEntriesToday($userId) {

        $cacheKey = "user-{$userId}-total-rejected-entries-today";

        $today = date('Y-m-d');

        if (\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }

        $rawQuery  = "(SELECT count(*) FROM account_modifications ";
        $rawQuery .= " WHERE account_modifications.authorization_status = 2 ";
        $rawQuery .= " AND initiator_id = {$userId}  AND created_at > '{$today} 00:00:00' ";
        $rawQuery .= " AND created_at < '{$today} 23:59:59' ) as total";

        $total = User::select(
        \DB::raw($rawQuery)
        )->first()->total;

        \Cache::put($cacheKey, $total, 1);

        return $total;
    }



    public static function userPendingEntriesThisMonth($userId) {

        $pendingEntriesToday = self::userPendingEntriesToday($userId);
        $pendingEntriesThisMonthBeforeToday = self::userPendingEntriesThisMonthBeforeToday($userId);
        return $pendingEntriesToday  +  $pendingEntriesThisMonthBeforeToday;
    }


    public static function userAuthorizedEntriesThisMonth($userId) {

        $authorizedEntriesToday = self::userAuthorizedEntriesToday($userId);
        $authorizedEntriesThisMonthBeforeToday = self::userAuthorizedEntriesThisMonthBeforeToday($userId);
        return $authorizedEntriesToday  +  $authorizedEntriesThisMonthBeforeToday;
    }

    public static function userUnauthorizedEntriesThisMonth($userId) {

        $unauthorizedEntriesToday = self::userAuthorizedEntriesToday($userId);
        $unauthorizedEntriesThisMonthBeforeToday = self::userUnauthorizedEntriesThisMonthBeforeToday($userId);
        return $unauthorizedEntriesToday + $unauthorizedEntriesThisMonthBeforeToday;
    }



    public static function userPendingEntriesThisMonthBeforeToday($userId) {

        $cacheKey = "user-{$userId}-pending-entries-this-month-before-today";
        $today = date('Y-m-d');

        if (\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }

        $from = date("Y-m-01");
        $lastMonthDay =  date("t");
        $to = date("Y-m-{$lastMonthDay}");

        $subQuery  = "(SELECT count(*) FROM account_modifications WHERE initiator_id=users.id ";
        $subQuery .= " AND account_modifications.authorization_status = 0";
        $subQuery .= " AND created_at >= '$from' AND created_at <= '$to' ";
        $subQuery .= " AND created_at < '$today 00:00:00' AND created_at <= '$to' ) as total";

        $total = User::select("users.*", 
                        \DB::raw($subQuery))
                    ->where('users.id', '=', $userId)
                    ->first()->total;

        \Cache::put($cacheKey, $total, 30);

        return $total;
    }


    public static function userAuthorizedEntriesThisMonthBeforeToday($userId) {

        $cacheKey = "user-{$userId}-authorized-entries-this-month-before-today";
        $today = date('Y-m-d');

        if (\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }

        $from = date("Y-m-01");
        $lastMonthDay =  date("t");
        $to = date("Y-m-{$lastMonthDay}");

        $subQuery  = "(SELECT count(*) FROM account_modifications WHERE initiator_id=users.id ";
        $subQuery .= " AND account_modifications.authorization_status = 1";
        $subQuery .= " AND created_at >= '$from' AND created_at <= '$to' ";
        $subQuery .= " AND created_at < '$today 00:00:00' AND created_at <= '$to' ) as total";

        $total = User::select("users.*", 
                        \DB::raw($subQuery))
                    ->where('users.id', '=', $userId)
                    ->first()->total;

        \Cache::put($cacheKey, $total, 30);

        return $total;
    }


    public static function userUnauthorizedEntriesThisMonthBeforeToday($userId) {

        $cacheKey = "user-{$userId}-unauthorized-entries-this-month-before-today";
        $today = date('Y-m-d');

        if (\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }

        $from = date("Y-m-01");
        $lastMonthDay =  date("t");
        $to = date("Y-m-{$lastMonthDay}");

        $subQuery  = "(SELECT count(*) FROM account_modifications WHERE initiator_id=users.id ";
        $subQuery .= " AND account_modifications.authorization_status = 2";
        $subQuery .= " AND created_at >= '$from' AND created_at <= '$to' ";
        $subQuery .= " AND created_at < '$today 00:00:00' AND created_at <= '$to' ) as total";

        $total = User::select("users.*", 
                        \DB::raw($subQuery))
                    ->where('users.id', '=', $userId)
                    ->first()->total;

        \Cache::put($cacheKey, $total, 30);

        return $total;
    }



    public static function currentUserArchivingBox($userId) {

        $cacheKey = ArchivingBoxHandler::generateUserArchivingBoxCacheKey($userId);

        if (\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }

        try {

           $archivingBox =  ArchivingBox::select('archiving_boxes.*')
                                    ->join('archiving_box_user', 
                                            'archiving_box_user.archiving_box_id', 
                                            '=', 'archiving_boxes.id')
                                    ->join('users',
                                        'users.id', '=', 
                                        'archiving_box_user.user_id')
                                        ->where('users.id', '=', $userId)
                                        ->orderBy('archiving_box_user.id', 'DESC')
                                        ->firstOrFail();

            \Cache::put($cacheKey, $archivingBox, 15);

            return $archivingBox;

        } catch (ModelNotFoundException $e) {
            return null;
        }


    }


    public static function mostRecentUsers($limit = 5) {

        $cacheKey = self::generateMostRecentUsersCacheKey($limit);

        if (\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }

        $rawQuery = "(select name from access_levels 
                    where access_levels.id = users.access_level_id) as access_level";

        $users = User::select(\DB::raw($rawQuery), 'users.*' )->take($limit)->get();

        \Cache::put($cacheKey, $users, 15);

        return $users;
    }



    public static function determineDayOfThisWeek() {
        $day = (int) date('w');

        if ($day == 0) {
            return 6;
        }

       return ($day - 1);
    }


    public static function weeklyUserPerformanceSummary($userId) {

         $today = date('Y-m-d');
         $week = date("W", strtotime($today)); // get week
         $y =    date("Y", strtotime($today)); // get year

         $dayOfThisWeek = self::determineDayOfThisWeek();

         $firstDate =  date('Y-m-d',strtotime($y."W".$week)); //first date 

         $weekDates = [];

         $weekDates[] = $firstDate;
         $days = [];

         $days [] = date("l", strtotime($firstDate));

         $totals = [];

         for ($i = 1; $i <= $dayOfThisWeek; $i++) {
            $lastDate = last($weekDates);
            $dayDate = date("Y-m-d",strtotime("+1 day", strtotime($lastDate)));
            $weekDates[] = $dayDate;
            $days [] = date("l", strtotime($dayDate));
         }

         $totals = self::weeklyUserTotalsBeforeToday($userId, $weekDates);

         array_push($totals, self::userTotalRecordsToday($userId));

         return [
            'dates' => $weekDates,
            'days' => $days,
            'totals' => $totals,
         ]; 

    }


    public static function weeklyUserTotalsBeforeToday($userId, $dates) {

        $cacheKey = "user-{$userId}-weekly-total-records-before-today";

        array_pop($dates);

        if (\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }


        $totals = [];

        for ($i = 0; $i < count( $dates); $i++) {

            $dayDate = $dates[$i];
           
            $rawQuery  = "(SELECT count(*) FROM account_modifications ";
            $rawQuery .= " WHERE account_modifications.authorization_status = 1 ";
            $rawQuery .= " AND initiator_id = {$userId}  AND created_at > '{$dayDate} 00:00:00' ";
            $rawQuery .= " AND created_at < '{$dayDate} 23:59:59' ) as total";

            $totals[] = User::select(
            \DB::raw($rawQuery)
            )->first()->total;

        }

        \Cache::put($cacheKey, $totals, 180);

        return $totals;

    }



    public static function userTotalRecordsToday($userId) {

        $cacheKey = "user-{$userId}-total-records-today";

        $today = date('Y-m-d');

        if (\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }

        $rawQuery  = "(SELECT count(*) FROM account_modifications ";
        $rawQuery .= " WHERE account_modifications.authorization_status = 1 ";
        $rawQuery .= " AND initiator_id = {$userId}  AND created_at > '{$today} 00:00:00' ";
        $rawQuery .= " AND created_at < '{$today} 23:59:59' ) as total";

        $total = User::select(
        \DB::raw($rawQuery)
        )->first()->total;

        \Cache::put($cacheKey, $total, 1);

        return $total;
    }


    public static function usernameAlreadyExistsForUser($username, $userId) {
         $count =  User::where('username', '=', $username)
                     ->where('id', '!=', $userId)
                     ->count();

        return $count > 0 ? true : false;
    }

    public static function usernameAlreadyExists($username) {
        $count =  User::where('username', '=', $username)
                     ->count();

        return $count > 0 ? true : false;
    }


    public static function secureUpdateOwnAccount() {

        $userId =  Auth::user()->id;
        $username = Request::input('username');
        $password = Request::input('password');
        $securityModification = false;

        if (self::usernameAlreadyExistsForUser($username, $userId)) {
            $message = "username $username is already taken by another user";
            throw new ExistingUserException($message);
        }

        $userId =  Auth::user()->id;
        $user   =  User::where('id', '=', $userId)->firstOrFail();

        if ($password) {
            $user->password   =  Hash::make($password);
            $securityModification = true;
        }


        $userModification =  self::createUserModificationFromUser($user, $securityModification);
        $user = self::updateUserFromModification($user, $userModification);

        UserActivityHandler::pushActivity( 
            EntityHandler::ENTITY_ID_USER,
             $user->id,
             UserActivityHandler::ACTIVITY_TYPE_ID_UPDATE,
             "username",
             $user->username  );

    }


    public static function  authorizeAccountModificationById ($userModificationId) {


        $userModification = UserModification::where('id', '=', $userModificationId)
                                            ->with('permissions')
                                            ->firstOrFail();

        $permissions = $userModification->permissions->pluck('id')->toArray();

        if($userModification->user_id) {

             $user = User::where('id', '=', $userModification->user_id)->firstOrFail();
             $user = UserHandler::updateUserFromModification($user, $userModification);

             if (count($permissions)) {
                $user->permissions()->detach();
                $user->permissions()->attach( $permissions);
             }
             
        } else {

            $username = $userModification->username;
            if (self::usernameAlreadyExists( $username )) {
                $message = "username $username is already taken by another user";
                $message .= " or pending update has already been verified";
                throw new ExistingUserException($message);
            }

            $user = UserHandler::createUserFromModification($userModification);
            $user->permissions()->attach($permissions);
        }
    }



    public static function createUserModification () {

        $username =  Request::input('username');
        $password =  Request::input('password');
        $active =  Request::input('active')? 1 : 0;

        $userModification = new UserModification;
        $userModification->first_name =  Request::input('first_name');
        $userModification->last_name  =  Request::input('last_name');
        $userModification->username   =  $username;

        if ( strlen($password) >= 6) {
            $userModification->password   =  Hash::make($password);
            $userModification->security_modification  = 1;
        }
        
        $userModification->access_level_id = Request::input('access_level_id');
        $userModification->active  = $active;

        $userModification->initiator_id = Auth::user()->id;
        
        $userModification->save();

        return $userModification;
    }


    public static function createUserModificationFromUser (
        \App\User $user, 
        $securityModification = false, 
        $authorizationStatus = 0) {

        $userModification = new UserModification;
        $userModification->password   =  $user->password;

        $userModification->username =    $user->username;
        $userModification->first_name =  $user->first_name;
        $userModification->last_name  =  $user->last_name;
        $userModification->access_level_id = $user->access_level_id;
        $userModification->active     = $user->active;
        $userModification->authorization_status = $authorizationStatus;

        if ($securityModification) {
            $userModification->security_modification  = 1;
        } else {
            $userModification->security_modification  = 0;
        }

        $userModification->save();
        return $userModification;
    }



    public static function createUserFromModification(\App\UserModification $userModification) {

        $user = new User;
        $user->user_modification_id =  $userModification->id;
        $user->first_name =  $userModification->first_name;
        $user->last_name  =  $userModification->last_name;
        $user->username   =  $userModification->username;
        $user->password   =  $userModification->password;
        $user->access_level_id = $userModification->access_level_id;
        $user->active     = $userModification->active;
        $user->save();

        $userModification->authorization_status = 1;
        $userModification->verifier_id =  \Auth::user()->id;
        $userModification->save();

        UserActivityHandler::pushActivity( 
            EntityHandler::ENTITY_ID_USER_MODIFICATION,
             $user->id,
             UserActivityHandler::ACTIVITY_TYPE_ID_AUTHORIZE,
             "username",
              $user->username );

        return $user;
    }

    public static function updateUserFromModification(\App\User $user, \App\UserModification $userModification) {

        $user->user_modification_id =  $userModification->id;
        $user->first_name =  $userModification->first_name;
        $user->last_name  =  $userModification->last_name;
        $user->username   =  $userModification->username;
        $user->password   =  $userModification->password;
        $user->access_level_id = $userModification->access_level_id;
        $user->active     = $userModification->active;

        if ( $userModification->security_modification == 1 ) {
            $user->password   =  $userModification->password;
        }

        $user->save();

        $userModification->verifier_id = Auth::user()->id;
        $userModification->authorization_status = 1;
        $userModification->save();

        return $user;
    }


    public static function attemptUserCreate() {

        $username =  Request::input('username');

        if (self::usernameAlreadyExists($username)) {
            $message = "username $username is already taken by another user";
            throw new ExistingUserException($message);
        }

        $permissions =  Request::input('access_permissions');

        $userModification  = self::createUserModification(true, 0);
        $user = self::createUserFromModification($userModification);

        if ( $permissions && count($permissions) ) {
            $userModification->permissions()->attach($permissions);
            $user->permissions()->attach($permissions);
        }

        UserActivityHandler::pushActivity( 
            EntityHandler::ENTITY_ID_USER,
             $userModification->id,
             UserActivityHandler::ACTIVITY_TYPE_ID_CREATE,
             "username",
             $userModification->username );
    }



    public static function attemptUserUpdate($userId) {

        $username =  Request::input('username');
        $active =  Request::input('active')? 1 : 0;

        if (self::usernameAlreadyExistsForUser($username, $userId) ) {
            $message = "username $username is already taken by another user";
            throw new ExistingUserException($message);
        }

        $userModification = self::createUserModification();
        $user = User::where('id', '=', $userId )->firstOrFail();


        $userModification->user_id = $user->id;
        $userModification->save();
        
        self::updateUserFromModification($user, $userModification);

        $permissions = Request::input('access_permissions');

        if ( count($permissions) ) {
            $userModification->permissions()->detach();
            $userModification->permissions()->attach($permissions);
                
            $user->permissions()->detach();
            $user->permissions()->attach( $permissions);
        }

        UserActivityHandler::pushActivity( 
            EntityHandler::ENTITY_ID_USER,
             $user->id,
             UserActivityHandler::ACTIVITY_TYPE_ID_UPDATE,
             "username",
             $user->username  );
    }



    public static function userModificationById($userModificationId) {
        return  UserModification::where('id', '=', $userModificationId)
                                              ->with('permissions')
                                              ->firstOrFail();
    }








}