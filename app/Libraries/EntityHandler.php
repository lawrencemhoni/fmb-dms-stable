<?php namespace App\Libraries;

use App\Entity;

class EntityHandler {

	const ENTITY_ID_USER = 1;
	const ENTITY_ID_CUSTOMER = 2;
	const ENTITY_ID_ACCOUNT = 3;
	const ENTITY_ID_DOCUMENT = 4;
	const ENTITY_ID_DOCUMENT_FILE = 5;
	
	const ENTITY_ID_CUSTOMER_MODIFICATION = 6;
	const ENTITY_ID_ACCOUNT_MODIFICATION = 7;
	const ENTITY_ID_DOCUMENT_MODIFICATION = 8;
	const ENTITY_ID_DOCUMENT_FILE_MODIFICATION = 9;
	const ENTITY_ID_ARCHIVING_BOX = 10;
	const ENTITY_ID_USER_MODIFICATION = 11;

	public static function getEntities() {
		$entities = Entity::all();
		return $entities;
	}



}


?>