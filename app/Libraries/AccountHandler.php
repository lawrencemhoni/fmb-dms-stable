<?php namespace App\Libraries;

use App\Account;
use App\Document;
use App\DocumentFile;
use App\AccountType;

use App\CustomerModification;
use App\AccountModification;
use App\DocumentModification;
use App\DocumentFileModification;
use App\User;
use App\Branch;
use Request;
use Auth;
use DB;

use App\Libraries\DocumentFileHandler;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Libraries\UserActivityHandler;
use App\Libraries\EntityHandler;

use App\Exceptions\NoUserArchivingBoxException;
use App\Exceptions\PendingAccountVerificationException;

class AccountHandler extends DataHandler {

        const ACCOUNT_TYPE_ID_SAVINGS = 1;
        const ACCOUNT_TYPE_ID_CURRENT = 2;
        const CACHE_KEY_TOTAL_ACCOUNTS_COUNT = "total_accounts_count";

        public static function generateCustomerTypeAccountTypeCountCacheKey(
            $customerTypeId, $accountTypeId ) {

            $cacheKey  = "customer_type_{$customerTypeId}_";
            $cacheKey .= "account_type_{$accountTypeId}_total_count";

            return $cacheKey;
        }

        public static function checkPendingAccountModification ($customerNumber) {

                $count = AccountModification::where(
                        'account_number', '=', $customerNumber 
                        )->where('authorization_status','=', 0)
                         ->take(1)
                         ->get()
                         ->count();

                        if($count >= 1){
                                return true;
                        }

                        return false;
        }

        public static function checkExistingAccountModification ($customerNumber) {

                $count = Account::where(
                        'account_number', '=', $customerNumber 
                        )->take(1)
                         ->get()
                         ->count();

                        if($count >= 1){
                                return true;
                        }

                        return false;
        }


        public static function createAccountModification (
                \App\CustomerModification $customerModifcation,
                $authorizationStatus = 0) {

            $accountType = Request::input('account_type');
            $accountNumber = Request::input('account_number');
            $userId =  \Auth::user()->id;

            $pendingModifications = self::checkPendingAccountModification($accountNumber);
            $currentUserArchivingBox =  UserHandler::currentUserArchivingBox($userId);

            if (!$currentUserArchivingBox) {
                $message = "No archiving box specified";
                throw new NoUserArchivingBoxException($message);
            }

            if ( $pendingModifications ) {
                    $message = "Account {$accountNumber} has a pending verification";
                    throw new PendingAccountVerificationException($message);
            }

            $accountModification = new AccountModification;
            
            $accountModification->account_number =  $accountNumber;
            $accountModification->account_type_id =  Request::input('account_type_id');
            $accountModification->branch_id =  Request::input('branch_id');
            $accountModification->account_status =  1;
            $accountModification->authorization_status =  $authorizationStatus;
            $accountModification->customer_modification_id =  $customerModifcation->id;
            $accountModification->initiator_id =  $userId;

            if ($currentUserArchivingBox) {
                $accountModification->archiving_box_id = $currentUserArchivingBox->id;
            }

            $accountModification->save();

            UserActivityHandler::pushActivity(
                    EntityHandler::ENTITY_ID_ACCOUNT_MODIFICATION,
                    $accountModification->id,
                    UserActivityHandler::ACTIVITY_TYPE_ID_CREATE,
                    "customer_number",
                    $accountModification->customer_number  );

            return $accountModification;
        }

        public static function createAccountFromAccountModification(
           \App\AccountModification $accountModification, 
           \App\Customer $customer ) {

            $account = new Account;

            $account->account_modification_id = $accountModification->id;
            $account->account_number =  $accountModification->account_number;
            $account->account_type_id = $accountModification->account_type_id;
            $account->branch_id = $accountModification->branch_id;
            $account->account_status = $accountModification->account_status;

            $account->customer_id = $customer->id;
            $account->archiving_box_id = $accountModification->archiving_box_id;
            $account->save();

            $accountModification->authorization_status = 1;
            $accountModification->verifier_id = \Auth::user()->id;
            $accountModification->save();

            UserActivityHandler::pushActivity( 
                EntityHandler::ENTITY_ID_ACCOUNT,
                 $account->id,
                 UserActivityHandler::ACTIVITY_TYPE_ID_AUTHORIZE,
                 "account_number",
                 $account->account_number  );

            return $account;
            
        }


        public static function getBranches() {

            $cacheKey = 'branches';

            if (\Cache::has($cacheKey)) {
                return \Cache::get($cacheKey);
            }

            $branches = Branch::orderBy('name')->get();

            \Cache::put($cacheKey,  $branches , 60);

            return $branches;
        }


        public static function filterOwnAccountModifications() {

            $filterNumber = Request::input('filter_number');

            $name  = Request::input('name');
            $branchId  = Request::input('branch');
            $dateOfBirth  = Request::input('date_of_birth');
            $accountType  = Request::input('account_type');

            $rawQuery =  "(SELECT name FROM account_types WHERE id=account_modifications.account_type_id  ";
            $rawQuery .= "  LIMIT 1) as account_type_name";

            $rawQuery2 = "(SELECT name FROM branches WHERE id=account_modifications.branch_id) as branch";

        
            $account = AccountModification::select(
                                'customer_modifications.*', 
                                'account_modifications.*', 

                                \DB::raw($rawQuery ),
                                 DB::raw($rawQuery2)
                                )
                         ->join('customer_modifications', 
                                'account_modifications.customer_modification_id', 
                                'customer_modifications.id')

                         ->where('account_modifications.initiator_id', '=', Auth::user()->id );

            if($filterNumber) {
                $account->where(function($query) use($filterNumber){ 
                    $query->orWhere('customer_modifications.customer_number', '=', $filterNumber);
                    $query->orWhere('account_modifications.account_number', '=', $filterNumber);
                });
            }

            if($dateOfBirth) {
                $account->where('customer_modifications.date_of_birth', '=', $dateOfBirth);
            }

            if($accountType) {
                $account->where('account_modifications.account_type_id', '=', $accountType);
            }

            if($branchId) {
                $account->where('account_modifications.branch_id', '=', $branchId);
            }

            if($name) {
                $account->where(function($query) use($name){

                    $arr = explode(" ", $name);
                    $query->where('customer_modifications.first_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customer_modifications.last_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customer_modifications.maiden_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customer_modifications.group_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customer_modifications.business_name', 'LIKE', "%%{$name}%%");

                    foreach($arr as $item) {
                        $query->orWhere('customer_modifications.first_name', '=', $item);
                        $query->orWhere('customer_modifications.last_name', '=', $item);
                        $query->orWhere('customer_modifications.maiden_name', '=', $item);
                    }
                });
            }

            $account->orderBy('first_name');
            $account->orderBy('last_name');
            $account->orderBy('group_name');
            $account->orderBy('business_name');

            $account->orderBy('account_modifications.id', 'DESC');

            return $account->paginate();
        }


        public static function findAccount($accountId) {

            $rawQuery = "(SELECT name FROM branches WHERE id=accounts.branch_id) as branch";
            $rawQuery2 = "(SELECT name FROM account_types WHERE id=accounts.account_type_id) as account_type";

            return Account::select(
                    'archiving_boxes.*',
                    'customers.*',
                    'accounts.*',
                     DB::raw( $rawQuery ),
                     DB::raw( $rawQuery2 )

                    )

                    ->join('customers', 'accounts.customer_id', '=', 'customers.id')
                    ->join('archiving_boxes', 'accounts.archiving_box_id', '=', 'archiving_boxes.id')
                    ->where('accounts.id', '=', $accountId)
                    ->firstOrFail();
        }



        public static function filterAccounts() {

            $filterNumber = Request::input('filter_number');
            $name  = Request::input('name');
            $branchId  = Request::input('branch');
            $dateOfBirth  = Request::input('date_of_birth');
            $accountType  = Request::input('account_type');

            $fromDate = Request::input('from_date');
            $toDate = Request::input('to_date');

            $serialNumber = Request::input('serial_number');
            $boxNumber = Request::input('box_number');


            $rawQuery =  "(SELECT name FROM account_types WHERE id=accounts.account_type_id  ";
            $rawQuery .= "  LIMIT 1) as account_type_name";

            $rawQuery2 = "(SELECT name FROM branches WHERE id=accounts.branch_id) as branch";

            $rawQuery3 = "(CASE WHEN 
                            (SELECT count(*) FROM document_files inner join documents where documents.account_id = accounts.id AND document_files.document_id=documents.id
                              AND title = 'Customer Signature File' ) > 0 
                            THEN 'Y'
                            ELSE 'N'
                            END) AS 'signature_present'";


            $rawQuery4 = "(CASE WHEN 
                            (SELECT count(*) FROM document_files inner join documents where documents.account_id = accounts.id AND document_files.document_id=documents.id 
                             AND title = 'Account Forms File' ) > 0 
                            THEN 'Y'
                            ELSE 'N'
                            END) AS 'cif_present'";

            $rawQuery5 = "(CASE WHEN 
                            (SELECT count(*) FROM document_files inner join documents where documents.account_id = accounts.id AND document_files.document_id=documents.id 
                                AND title = 'Customer Attachments File') > 0 
                            THEN 'Y'
                            ELSE 'N'
                            END) AS 'attachment_present'";

            $account = Account::select(
                                'customers.*', 
                                'accounts.*', 
                                \DB::raw($rawQuery ),
                                 DB::raw($rawQuery2),
                                 DB::raw($rawQuery3),
                                 DB::raw($rawQuery4),
                                 DB::raw($rawQuery5)
                                )
                         ->join('customers', 
                                'accounts.customer_id', 
                                'customers.id')

                         ->join('archiving_boxes', 
                                'accounts.archiving_box_id', 
                                'archiving_boxes.id');

            if($serialNumber) {
                $account->where('archiving_boxes.serial_number', '=', $serialNumber);
            }

            if($boxNumber) {
                $account->where('archiving_boxes.box_number', '=', $boxNumber);
            }


            if ($fromDate) {
                $account->where('accounts.created_at', '>=', $fromDate);
            }

            if ($toDate) {
                $toDate = Request::input('to_date') . ' 23:59';
                $account->where('accounts.created_at', '<=', $toDate);
            }

            if($filterNumber) {
                $account->where(function($query) use($filterNumber){ 
                    $query->orWhere('customers.customer_number', '=', $filterNumber);
                    $query->orWhere('accounts.account_number', '=', $filterNumber);
                });
            }

            if($dateOfBirth) {
                $account->where('customers.date_of_birth', '=', $dateOfBirth);
            }

            if($accountType) {
                $account->where('accounts.account_type_id', '=', $accountType);
            }


            if($branchId) {

                $account->where('accounts.branch_id', '=', $branchId);
            }

            if($name) {
                $account->where(function($query) use($name){
                    $arr = explode(" ", $name);
                    $query->where('customers.first_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customers.last_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customers.maiden_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customers.group_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customers.business_name', 'LIKE', "%%{$name}%%");
                    foreach($arr as $item) {
                        $query->orWhere('customers.first_name', '=', $item);
                        $query->orWhere('customers.last_name', '=', $item);
                        $query->orWhere('customers.maiden_name', '=', $item);
                    }
                });
            }



            $account->orderBy('first_name');
            $account->orderBy('last_name');
            $account->orderBy('group_name');
            $account->orderBy('business_name');

            $account->orderBy('accounts.id', 'DESC');

            return $account->paginate(20);
        }


        public static function filterReportAccounts() {

            $filterNumber = Request::input('filter_number');
            $name  = Request::input('name');
            $branchId  = Request::input('branch');
            $dateOfBirth  = Request::input('date_of_birth');
            $accountType  = Request::input('account_type');

            $fromDate = Request::input('from_date');
            $toDate = Request::input('to_date');

            $serialNumber = Request::input('serial_number');
            $boxNumber = Request::input('box_number');

           $rawQuery = "(CASE WHEN customer_type_id = 1 
                 THEN CONCAT(last_name, ' ' , first_name) 
                 WHEN customer_type_id = 2 THEN group_name
                 WHEN customer_type_id = 3 THEN business_name
                END) AS 'Customer name'";
            $rawQuery2 =  "(SELECT name FROM account_types WHERE id=accounts.account_type_id) as Type  ";
            $rawQuery3 = "(SELECT name FROM branches WHERE id=accounts.branch_id) as Branch";

            $rawQuery4 = "(CASE WHEN 
                            (SELECT count(*) FROM document_files inner join documents where documents.account_id = accounts.id AND document_files.document_id=documents.id
                              AND title = 'Customer Signature File' ) > 0 
                            THEN 'Y'
                            ELSE 'N'
                            END) AS 'signature_present'";


            $rawQuery5 = "(CASE WHEN 
                            (SELECT count(*) FROM document_files inner join documents where documents.account_id = accounts.id AND document_files.document_id=documents.id 
                             AND title = 'Account Forms File' ) > 0 
                            THEN 'Y'
                            ELSE 'N'
                            END) AS 'cif_present'";

            $rawQuery6 = "(CASE WHEN 
                            (SELECT count(*) FROM document_files inner join documents where documents.account_id = accounts.id AND document_files.document_id=documents.id 
                                AND title = 'Customer Attachments File') > 0 
                            THEN 'Y'
                            ELSE 'N'
                            END) AS 'attachment_present'";


            
        
            $account = Account::select(
                                \DB::raw($rawQuery ),
                                "customers.customer_number as Customer Number ", 
                                "accounts.account_number as Account Number ", 
                                 \DB::raw($rawQuery2),
                                 \DB::raw($rawQuery3),
                                 "accounts.created_at as Date Created",
                                  \DB::raw($rawQuery4),
                                  \DB::raw($rawQuery5),
                                  \DB::raw($rawQuery6)
                                )
                         ->join('customers', 
                                'accounts.customer_id', 
                                'customers.id')

                         ->join('archiving_boxes', 
                                'accounts.archiving_box_id', 
                                'archiving_boxes.id');

            if($serialNumber) {
                $account->where('archiving_boxes.serial_number', '=', $serialNumber);
            }

            if($boxNumber) {
                $account->where('archiving_boxes.box_number', '=', $boxNumber);
            }


            if ($fromDate) {
                $account->where('accounts.created_at', '>=', $fromDate);
            }

            if ($toDate) {
                $toDate = Request::input('to_date') . ' 23:59';
                $account->where('accounts.created_at', '<=', $toDate);
            }

            if($filterNumber) {
                $account->where(function($query) use($filterNumber){ 
                    $query->orWhere('customers.customer_number', '=', $filterNumber);
                    $query->orWhere('accounts.account_number', '=', $filterNumber);
                });
            }

            if($dateOfBirth) {
                $account->where('customers.date_of_birth', '=', $dateOfBirth);
            }

            if($accountType) {
                $account->where('accounts.account_type_id', '=', $accountType);
            }


            if($branchId) {

                $account->where('accounts.branch_id', '=', $branchId);
            }

            if($name) {

                $account->where(function($query) use($name){

                    $arr = explode(" ", $name);
                    $query->where('customers.first_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customers.last_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customers.maiden_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customers.group_name', 'LIKE', "%%{$name}%%");
                    $query->orWhere('customers.business_name', 'LIKE', "%%{$name}%%");

                    foreach($arr as $item) {
                        $query->orWhere('customers.first_name', '=', $item);
                        $query->orWhere('customers.last_name', '=', $item);
                        $query->orWhere('customers.maiden_name', '=', $item);
                    }
                });
            }

            $account->orderBy('first_name');
            $account->orderBy('last_name');
            $account->orderBy('group_name');
            $account->orderBy('business_name');

            $account->orderBy('accounts.id', 'DESC');

            return $account->get();
        }


        public static function getAccountTypes() {

                $cacheKey = 'account_types';

                if (\Cache::has($cacheKey)) {
                    return \Cache::get($cacheKey);
                }

                $branches = AccountType::orderBy('name')->get();
                \Cache::put($cacheKey,  $branches , 60);

                return $branches;
        }


        public static function totalAccountsCount() {    

            if (\Cache::has(self::CACHE_KEY_TOTAL_ACCOUNTS_COUNT)) {

                return \Cache::get(self::CACHE_KEY_TOTAL_ACCOUNTS_COUNT);
            }
     
            $accounts = Account::select(
                \DB::raw('(SELECT count(*) FROM accounts) as total')
            )->first();

            $total = ( $accounts )?  $accounts->total : 0;
            \Cache::put(self::CACHE_KEY_TOTAL_ACCOUNTS_COUNT,  $total, 60);
            return $total;
        }


        public static function cusTypeAccTypeTotalCount($customerTypeId, $accountTypeId) {

            $cacheKey = self::generateCustomerTypeAccountTypeCountCacheKey(
                $customerTypeId, $accountTypeId);

            if (\Cache::has($cacheKey)) {
                return \Cache::get($cacheKey);
            }

            $rawQuery = "(SELECT count(*) FROM accounts inner join customers ON accounts.customer_id = customers.id WHERE accounts.account_type_id={$accountTypeId} AND customers.customer_type_id = {$customerTypeId} ) as total ";

            $account = Account::select(
                        DB::raw($rawQuery))
                        ->take(1)
                        ->first();


            $total = ( $account )?  $account->total : 0;

             \Cache::put($cacheKey,  $total, 60);

            return $total;

        }





}


?>