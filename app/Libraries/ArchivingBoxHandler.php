<?php namespace App\Libraries;

use App\ArchivingBox;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Libraries\UserActivityHandler;
use App\Libraries\EntityHandler;

use App\Exceptions\ExistingArchivingBoxException;
use Request;
use Auth;


class ArchivingBoxHandler {


	public static function generateUserArchivingBoxCacheKey($userId) {
		return "user_{$userId}_archiving_box";
	}


	public static function archivingBoxExists($serialNumber) {
		$archivingBox = ArchivingBox::select()
									->where('serial_number', '=', $serialNumber)
									->take(1)
									->get();

		if( $archivingBox->count() > 0 ) {
			return true;
		}

		return false;
	}


	public static function createUserArchvingBox(){

		$userId = Auth::user()->id;
		$serialNumber = Request::input('serial_number');

		if (self::archivingBoxExists($serialNumber)) {
			$message = "Archiving Box $serialNumber alreadyExists";
			throw new ExistingArchivingBoxException($message);
		}

    	$archivingBox = new ArchivingBox;
    	$archivingBox->box_number = Request::input('box_number');
    	$archivingBox->serial_number = $serialNumber;
    	$archivingBox->location = Request::input('location');
    	$archivingBox->save();

    	$archivingBox->users()
    				 ->attach([
    				 	$userId
    				 ]);

    	$cacheKey = self::generateUserArchivingBoxCacheKey($userId);

    	\Cache::put($cacheKey,  $archivingBox , 15 );

       UserActivityHandler::pushActivity(
            EntityHandler::ENTITY_ID_ARCHIVING_BOX,
            $archivingBox->id,
            UserActivityHandler::ACTIVITY_TYPE_ID_CREATE,
            "serial_number",
            $archivingBox->serial_number );


	}

}










?>
