<?php namespace App\Libraries;

use App\Customer;
use App\Account;
use App\Document;
use App\DocumentFile;
use App\AccountType;

use App\CustomerModification;
use App\AccountModification;
use App\DocumentModification;
use App\DocumentFileModification;
use App\User;
use App\Branch;
use Request;
use Auth;
use DB;

use App\Libraries\DocumentFileHandler;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Libraries\UserActivityHandler;
use App\Libraries\EntityHandler;
use App\Exceptions\PendingCustomerVerificationException;
use App\Exceptions\NoUserArchivingBoxException;

class CustomerHandler extends DataHandler {

	const CUSTOMER_TYPE_ID_PERSONAL = 1;
	const CUSTOMER_TYPE_ID_GROUP = 2;
	const CUSTOMER_TYPE_ID_BUSINESS = 3;

	const CACHE_KEY_TOTAL_CUSTOMERS_COUNT = 'total_customers_count';

	public static function checkPendingCustomerModification ($customerNumber) {

		$count = CustomerModification::where(
			'customer_number', '=', $customerNumber 
			)->where('authorization_status','=', 0)
			 ->take(1)
			 ->get()
			 ->count();

			if($count >= 1){
				return true;
			}

			return false;
	}

	public static function checkExistingCustomerModification ($customerNumber) {

		$count = Customer::where(
			'customer_number', '=', $customerNumber 
			)->take(1)
			 ->get()
			 ->count();

			if($count >= 1){
				return true;
			}

			return false;
	}

	public static function createCustomerFromCustomerModification (
		\App\CustomerModification $customerModification) {

		$customer = new Customer;

		$customer->customer_modification_id = $customerModification->id;
		$customer->customer_type_id = $customerModification->customer_type_id;
		$customer->customer_number = $customerModification->customer_number;
		$customer->first_name = $customerModification->first_name;
		$customer->last_name = $customerModification->last_name;
		$customer->maiden_name = $customerModification->maiden_name;
		$customer->date_of_birth = $customerModification->date_of_birth;
		$customer->group_name = $customerModification->group_name;
		$customer->business_name = $customerModification->business_name;

		$customer->save();

		$customerModification->authorization_status = 1;
		$customerModification->verifier_id = \Auth::user()->id;
		$customerModification->save();

         UserActivityHandler::pushActivity(
         	EntityHandler::ENTITY_ID_CUSTOMER,
         	$customer->id,
         	UserActivityHandler::ACTIVITY_TYPE_ID_AUTHORIZE,
         	"customer_number",
         	$customer->customer_number );

         return $customer;

	}

	public static function createCustomerModification ($authorizationStatus = 0) {

		$customerTypeId   = Request::input('customer_type_id');
		$customerNumber = Request::input('customer_number');

 		$userId =  \Auth::user()->id;
		$currentUserArchivingBox =  UserHandler::currentUserArchivingBox($userId);


		$pendingModifications = self::checkPendingCustomerModification($customerNumber);

		if (!$currentUserArchivingBox) {
            $message = "No archiving box specified";
            throw new NoUserArchivingBoxException($message);
        }

		if ( $pendingModifications ) {
			$message = "Customer {$customerNumber} has a pending verification";
			throw new PendingCustomerVerificationException($message);
		}


		 $customerModification = new CustomerModification;

		 switch ($customerTypeId) {

		 	case 1:
		        $customerModification->first_name =  self::correctNameCasing(Request::input('first_name')); ;
		        $customerModification->maiden_name =  self::correctNameCasing(Request::input('maiden_name'));
		        $customerModification->last_name =  self::correctNameCasing(Request::input('last_name'));
		        $customerModification->date_of_birth =  Request::input('date_of_birth');
		 		break;
		 	case 2 :

		 		$customerModification->group_name =  self::correctNameCasing(Request::input('group_name')); 
		 		break;
		 	case 3 : 

		 		$customerModification->business_name =  self::correctNameCasing(Request::input('business_name'));
		 		break;
		 	
		 	default:
		 		throw new UndefinedCustomerTypeException("Unknown customer id {$customerTypeId}");
		 		
		 		break;
		 }

		$customerModification->customer_number =  $customerNumber;
        $customerModification->customer_type_id = $customerTypeId;

        $customerModification->authorization_status =  $authorizationStatus;
        $customerModification->initiator_id =  \Auth::user()->id;

        $customerModification->save();

         UserActivityHandler::pushActivity(
         	EntityHandler::ENTITY_ID_CUSTOMER_MODIFICATION,
         	$customerModification->id,
         	UserActivityHandler::ACTIVITY_TYPE_ID_CREATE,
         	"customer_number",
         	$customerModification->customer_number  );


        return $customerModification;
	}

    public static function getCustomerTypes() {

            $cacheKey = 'customer_types';

            if (\Cache::has($cacheKey)) {
                return \Cache::get($cacheKey);
            }

            $branches = CustomerType::orderBy('name')->get();


            \Cache::put($cacheKey,  $branches , 60);

            return $branches;
    }



    public static function totalCustomersCount() {    

        if (\Cache::has(self::CACHE_KEY_TOTAL_CUSTOMERS_COUNT)) {
            return \Cache::get(self::CACHE_KEY_TOTAL_CUSTOMERS_COUNT);
        }
 
        $customer = Customer::select(
        	\DB::raw('(SELECT count(*) FROM customers ) as total')
        )->first();

        $total = ($customer)? $customer->total : 0;

        \Cache::put(self::CACHE_KEY_TOTAL_CUSTOMERS_COUNT,  $total, 60);

        return $total;
    }






}


?>