<?php namespace App\Libraries;

use App\User;
use App\AccessLevel;
use Request;
use Auth;
use DB;

use App\AccessPermission;

class AccessLevelHandler {


	const ACCESS_PERMISSION_ID_CREATE_FMB_ADMIN_USER  = 1;
	const ACCESS_PERMISSION_ID_UPDATE_FMB_ADMIN_USER  = 2;
	const ACCESS_PERMISSION_ID_VIEW_USERS  = 3;
	const ACCESS_PERMISSION_ID_CREATE_FMB_STAFF  = 4;
	const ACCESS_PERMISSION_ID_UPDATE_FMB_STAFF  = 5;


	const ACCESS_PERMISSION_ID_CREATE_BEEHIVE_ADMIN_USER  = 11;
	const ACCESS_PERMISSION_ID_UPDATE_BEEHIVE_ADMIN_USER  = 12;

	
	const ACCESS_PERMISSION_ID_VIEW_ACCOUNTS  = 6;
	const ACCESS_PERMISSION_ID_UPDATE_ACCOUNTS  = 7;


	const ACCESS_PERMISSION_ID_AUDIT_TRAIL  = 8;
	const ACCESS_PERMISSION_ID_PULL_REPORTS = 9;


	const ACCESS_LEVEL_ID_SUPER_USER = 1;
	const ACCESS_LEVEL_ID_FMB_ADMIN = 2;
	const ACCESS_LEVEL_ID_FMB_STAFF = 3;
	const ACCESS_LEVEL_ID_FMB_TECH = 4;
	const ACCESS_LEVEL_ID_BEEHIVE_ADMIN = 5;
	const ACCESS_LEVEL_ID_BEEHIVE_CLERK = 7;

	const ACCESS_PERMISSION_ID_AUTHORIZE_USER_MODIFICATION = 10;


	public static function checkAccessExec(Array $permissions, $callback) {

		$user = Auth::user();

		$result = User::join('user_access_permission', 'users.id', 'user_access_permission.user_id')
						->where('users.id', '=', $user->id)
						->whereIn('user_access_permission.access_permission_id', $permissions)
						->get();

		if($result->count()) {
			return is_callable($callback)? $callback() : '';
		}

		$errorMessage = "You do not have enough permissions to access the section you were trying to.";

		switch ($user->access_level_id) {
			case self::ACCESS_LEVEL_ID_FMB_ADMIN :
				return \Redirect::to('/admin/dashboard')->with("feedback-error", $errorMessage);
				break;
			
			case self::ACCESS_LEVEL_ID_FMB_STAFF :
				return \Redirect::to('/user/dashboard')->with("feedback-error", $errorMessage);
				break;
			
			default:
				return \Redirect::to('login')->with("feedback-error", $errorMessage);

				break;
		}
	}


   public static function getAccessLevelsByIds (array $ids) {

        return AccessLevel::select()
                            ->whereIn("id", $ids)
                            ->with('permissions')
                            ->get();
    }











}










?>