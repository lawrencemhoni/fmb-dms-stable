<?php namespace App\Libraries;

use Request;


class LinksHelper {


	const URI_FMB_ADMIN_ACCOUNTS = '/admin/accounts';
	const URI_FMB_ADMIN_ACCOUNTS_REPORT_DOWNLOAD = '/admin/accounts-report/download';
	const URI_EDIT_OWN_ACCOUNT = '/my-account';


	public static function  returnToAccountsFilter () {

		$link = Request::server('HTTP_REFERER');
		if ( $link ) {
			return $link;
		}

		return self::URI_FMB_ADMIN_ACCOUNTS;

	}

	public static function  exportAccountsReportUsingFilter () {
		$queryString = Request::getQueryString();
		$link = self::URI_FMB_ADMIN_ACCOUNTS_REPORT_DOWNLOAD .'?'. $queryString; 
		return $link;

	}



}



